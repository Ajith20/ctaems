#Parsing log files to make a csv
import os
import csv
import matplotlib.pyplot as plt

a = 1
quality_list = []
schedule_list = []
jensen_learned_enables_list = []
jensen_learned_disables_list = []
my_learned_enables_list = []
my_learned_disables_list = []
actual_enable_list = []
actual_disable_list = []
my_guarenteed_not_disable_list = []
my_could_be_disable_list = []
my_guarenteed_not_enable_list = []
my_could_be_enable_list = []
log_schedule_string = ""
x = []
y =[]
my_y = []
y_percent = []
my_y_percent = []

directory_names = ["/home/ajith/workspace/AIsimv3/logs/TestOuputssucc/Test1"]
for directory_name in directory_names:
    directory = os.fsencode(directory_name)
    with open("data.csv", "a") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            filename.strip()
            if(filename.split("_")[0] == "output"):
                log_output_content = open(directory_name + "/" + filename)
                log_output_lines = log_output_content.readlines()
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            filename.strip()
            if(filename.split("_")[1] == "Ordered"):
                #print(filename)
                file_content = open(directory_name + "/" + filename)
                lines = file_content.readlines()
                quality = [0] * 30
                schedule = []
                #print(directory_name + "/" + filename)
                for i in range(len(lines)):
                    line = lines[i].strip()
                    if(line == "####### Ordered Logger #######"):
                        seed_value = int(lines[i+5].split(":")[1].strip())
                        #print(seed_value)
                        for j in range(len(log_output_lines)):
                            log_line = log_output_lines[j].strip()
                            if(log_line.split(" ")[0] == "Random"):
                                log_seed_value = int(log_line.split(" ")[4])
                                if(log_seed_value == seed_value):
                                    #print(log_seed_value)
                                    #print(log_output_lines[j-2])
                                    log_schedule_string = log_output_lines[j-2]
                                    print(log_schedule_string)
                    if(line == "## Method ##"):
                        method_name = lines[i+1].strip()
                        method_name = method_name.split(":")[1]
                        method_name = method_name.strip()
                        method_name = method_name.split("d")[1]
                        method_quality = lines[i+3].strip()
                        method_quality = method_quality.split(":")[1]
                        method_quality = method_quality.strip()
                        quality[int(method_name) - 1] = float(method_quality)

                    schedule_string = log_schedule_string.strip()
                    schedule_string = schedule_string[1:-1]
                    #print(schedule_string)
                    schedule = [int(s) for s in schedule_string.split(',')]
                    #print(schedule)

                #print(quality)
                quality_list.append(quality)
                schedule_list.append(schedule)
                writer.writerow(quality)
#print(quality_list)
#print(schedule_list)


def NLEs():
    temp_list = []
    temp_list3 = []
    jensen_misclassified_count = 0
    hard_nles_count = 870
    jensen_error = 0
    my_error = 0
    my_misclassified_count = 0
    jensen_no_of_hnles = 0
    my_no_of_hnles = 0
    """ actual enable and disable lists are relationships that exist and these lists are used for generating jensen_error"""
    for i in range(1,31):
        actual_enable_list.append(temp_list3)
        actual_disable_list.append(temp_list3)
        my_guarenteed_not_disable_list.append(temp_list3)
        my_could_be_disable_list.append(temp_list3)
        my_guarenteed_not_enable_list.append(temp_list3)
        my_could_be_enable_list.append(temp_list3)
    #Manually encoding enable and disables
    temp_list2 = [1]
    actual_enable_list[3] = temp_list2
    temp_list2 = [9]
    actual_enable_list[25] = temp_list2
    temp_list2 = [24]
    actual_enable_list[12] = temp_list2
    temp_list2 = [9,26]
    actual_enable_list[27] = temp_list2
    temp_list2 = [6,7,8,9,10]
    actual_enable_list[10] = temp_list2
    actual_enable_list[11] = temp_list2
    actual_enable_list[12] = temp_list2
    actual_enable_list[13] = temp_list2
    actual_enable_list[14] = temp_list2
    temp_list2 = [4]
    actual_disable_list[5] = temp_list2

    temp_list2 = [18]
    actual_disable_list[13] = temp_list2
    temp_list2 = [14]
    actual_disable_list[15] = temp_list2
    temp_list2 = [16,17,18,19,20]
    actual_disable_list[25] = temp_list2
    actual_disable_list[26] = temp_list2
    actual_disable_list[27] = temp_list2
    actual_disable_list[28] = temp_list2
    actual_disable_list[29] = temp_list2
    #print(actual_enable_list)
    #print(actual_disable_list)


    for i in range(1,31):
        temp_list.append(i)
    #print(temp_list)
    for i in range(1,31):
        method_list = list(temp_list)
        method_list.pop(i-1)
        #print(method_list)
        jensen_learned_enables_list.append(method_list)
        jensen_learned_disables_list.append(method_list)
        my_learned_enables_list.append(method_list)
        my_learned_disables_list.append(method_list)
    #print(my_learned_enables_list)
    #print(actual_enable_list)

    #Iterating through quality and schedule lists to apply enables and diables exclusion rules of Jensesn and my rules

    for i in range(len(quality_list)):
        jensen_misclassified_count = 0
        my_misclassified_count = 0
        jensen_no_of_hnles = 0
        my_no_of_hnles = 0
        quality_instance = quality_list[i]
        schedule_instance = schedule_list[i]
        #print(quality_list)
        for j in range(len(jensen_learned_enables_list)):
            enables_method = list(jensen_learned_enables_list[j])
            disables_method = list(jensen_learned_disables_list[j])
            my_enables_method = list(my_learned_enables_list[j])
            my_disables_method = list(my_learned_disables_list[j])
            my_guarenteed_not_disable_method = list(my_guarenteed_not_disable_list[j])
            my_could_disable_method = list(my_could_be_disable_list[j])
            my_guarenteed_not_enable_method = list(my_guarenteed_not_enable_list[j])
            my_could_enable_method = list(my_could_be_enable_list[j])


            if(quality_instance[j] != 0):
                index = schedule_instance.index(j+1)
                #print(index)
                #Exclusion rule for enables
                for k in range(index+1, 30):
                    if schedule_instance[k] in enables_method:
                        enables_method.remove(schedule_instance[k])
                        #my_enables_method.remove(schedule_instance[k])
                    #My enables rule
                    if schedule_instance[k] not in my_guarenteed_not_enable_method:
                        my_guarenteed_not_enable_method.append(schedule_instance[k])

                jensen_learned_enables_list[j] = enables_method
                #my_learned_enables_list[j] = my_enables_method
                #print(enables_method)

                #Exclusion principle for disables
                for k in range(0,index):
                    if schedule_instance[k] in disables_method:
                        disables_method.remove(schedule_instance[k])
                    #My rule for disables maintain a list of guarenteed methods which don't disable the method i.e all the methods which have been
                    #sucessfully executed before the current method
                    if schedule_instance[k] not in my_guarenteed_not_disable_method and quality_instance[schedule_instance[k] - 1] != 0:
                        my_guarenteed_not_disable_method.append(schedule_instance[k])

                    #My rule for enables
                    if schedule_instance[k] not in my_could_enable_method and quality_instance[schedule_instance[k] - 1] == 0:
                        my_guarenteed_not_enable_list.append(schedule_instance[k])

                jensen_learned_disables_list[j] = disables_method
                #my_learned_disables_list[j] = my_disables_method
                my_guarenteed_not_disable_list[j] = my_guarenteed_not_disable_method
                my_guarenteed_not_enable_list[j] = my_guarenteed_not_enable_method
                #print(my_guarenteed_not_disable_method)


            else:
                #print(j)
                index = schedule_instance.index(j+1)

                #My rule for disables - removes all the methods which are sucessfully executed after the current method and all
                #the methods which are unsuccesfully executed befor the method
                for k in range(0, index):
                    if schedule_instance[k] not in my_could_disable_method and quality_instance[schedule_instance[k] - 1] != 0:
                        my_could_disable_method.append(schedule_instance[k])

                    #My rule for enables appends all unsuccesfull methods before the current method in schedule in could_enable list
                    if schedule_instance[k] not in my_could_enable_method and quality_instance[schedule_instance[k] - 1] == 0:
                        my_could_enable_method.append(schedule_instance[k])
                #My rule for enables appends all methods after the current method in could_enable list
                for k in range(index+1, 30):
                    if schedule_instance[k] not in my_could_enable_method:
                        my_could_enable_method.append(schedule_instance[k])

                    if schedule_instance[k] not in my_guarenteed_not_disable_method:
                        my_guarenteed_not_disable_method.append(schedule_instance[k])
                #print(my_could_disable_method)
                my_could_be_disable_list[j] = my_could_disable_method
                my_could_be_enable_list[j] = my_could_enable_method

            for z in my_guarenteed_not_disable_method:
                if z in my_could_disable_method:
                    my_could_disable_method.remove(z)
            for z in my_guarenteed_not_enable_method:
                if z in my_could_enable_method:
                    my_could_enable_method.remove(z)
            my_learned_disables_list[j] = my_could_disable_method
            my_learned_enables_list[j] = my_could_enable_method

        #Generating jensen_error and ploting, this could have been done in the for loop too
        for j in range(len(jensen_learned_enables_list)):
            enables_method = list(jensen_learned_enables_list[j])
            disables_method = list(jensen_learned_disables_list[j])
            my_enables_method = list(my_learned_enables_list[j])
            my_disables_method = list(my_learned_disables_list[j])
            #print(my_disables_method)
            actual_enables_method = list(actual_enable_list[j])
            #print(actual_enables_method)
            actual_disables_method = list(actual_disable_list[j])
            for z in actual_enables_method:
                #print(z)
                if(z in enables_method):
                    jensen_misclassified_count = jensen_misclassified_count - 1
                    jensen_no_of_hnles = jensen_no_of_hnles + 1
                else:
                    jensen_misclassified_count = jensen_misclassified_count + 1

                if(z in my_enables_method):
                    my_misclassified_count = my_misclassified_count - 1
                    my_no_of_hnles = my_no_of_hnles + 1
                else:
                    my_misclassified_count = my_misclassified_count + 1
            #print(jensen_misclassified_count)
            jensen_misclassified_count = jensen_misclassified_count + len(enables_method)
            my_misclassified_count = my_misclassified_count + len(my_enables_method)
            for z in actual_disables_method:
                if(z in disables_method):
                    jensen_misclassified_count = jensen_misclassified_count - 1
                    jensen_no_of_hnles = jensen_no_of_hnles + 1
                else:
                    jensen_misclassified_count = jensen_misclassified_count + 1

                if(z in my_disables_method):
                    my_misclassified_count = my_misclassified_count - 1
                    my_no_of_hnles = my_no_of_hnles + 1
                else:
                    my_misclassified_count = my_misclassified_count + 1

            jensen_misclassified_count = jensen_misclassified_count + len(disables_method)
            my_misclassified_count = my_misclassified_count + len(my_disables_method)
        print(jensen_misclassified_count)
        jensen_error = jensen_misclassified_count/hard_nles_count
        my_error = my_misclassified_count/hard_nles_count
        #print(my_error)
        #print(jensen_error)
        x.append(i+1)
        y.append(jensen_error)
        my_y.append(my_error)
        y_percent.append((jensen_no_of_hnles/11) * 100)
        my_y_percent.append((my_no_of_hnles/11) * 100)
    #print(actual_enable_list)
    #print(jensen_learned_enables_list)
    print(my_learned_enables_list)
    #print(actual_disable_list)
    #print(jensen_learned_disables_list)
    print(my_learned_disables_list)
    #for i in schedule_list:
        #print(i)
    plt.xlabel('Schedules')
    #plt.ylabel('jensen_error')
    #plt.ylabel('Percentage of ')
    plt.plot(x,y)
    plt.plot(x,my_y)
    #plt.plot(x,y_percent)
    #plt.plot(x,my_y_percent)
    plt.show()








NLEs()
