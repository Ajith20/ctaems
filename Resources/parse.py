#Parsing log files to make a csv
import os
import csv
import math
directory_name = "/home/ajith/workspace/AIsimv3/logs/Outputs/NLEsBatch4"
log_output_lines = ""
directory = os.fsencode(directory_name)
a = 1
names_list = []
number_of_hard_nles = 7
quality_list = []
schedule_list = []
log_schedule_string = ""
number_of_methods = 30
for i in range(1,number_of_methods + 1):
    Name = "Method" + str(i)
    names_list.append(Name)
#print(names_list)
with open("data.csv", "a") as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    writer.writerow(names_list)
    directory_names = ["/home/ajith/workspace/AIsimv3/logs/Outputs/NLEsBatch1","/home/ajith/workspace/AIsimv3/logs/Outputs/NLEsBatch2","/home/ajith/workspace/AIsimv3/logs/Outputs/NLEsBatch3","/home/ajith/workspace/AIsimv3/logs/Outputs/NLEsBatch4"]
    for directory_name in directory_names:
        directory = os.fsencode(directory_name)
        for file in os.listdir(directory):
            count = 0
            filename = os.fsdecode(file)
            filename.strip()
            if(filename.split("_")[0] == "output"):
                #print(filename)
                log_output_content = open(directory_name + "/" + filename)
                log_output_lines = log_output_content.readlines()
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            filename.strip()
            if(filename.split("_")[1] == "Ordered"):
                #print(filename)
                file_content = open(directory_name + "/" + filename)
                lines = file_content.readlines()
                quality = [0] * 30
                #print(directory_name + "/" + filename)
                for i in range(len(lines)):
                    line = lines[i].strip()
                    if(line == "####### Ordered Logger #######"):
                        seed_value = int(lines[i+5].split(":")[1].strip())
                        #print(seed_value)
                        for j in range(len(log_output_lines)):
                            log_line = log_output_lines[j].strip()
                            if(log_line.split(" ")[0] == "Random"):
                                log_seed_value = int(log_line.split(" ")[4])
                                if(log_seed_value == seed_value):
                                    log_schedule_string = log_output_lines[j-2]
                                    #print(log_schedule_string)
                    if(line == "## Method ##"):
                        method_name = lines[i+1].strip()
                        method_name = method_name.split(":")[1]
                        method_name = method_name.strip()
                        method_name = method_name.split("d")[1]
                        method_quality = lines[i+3].strip()
                        method_quality = method_quality.split(":")[1]
                        method_quality = method_quality.strip()
                        quality[int(method_name) - 1] = math.log(float(method_quality))
                quality_list.append(quality)
                schedule_string = log_schedule_string.strip()
                schedule_string = schedule_string[1:-1]
                #print(schedule_string)
                schedule = [int(s) for s in schedule_string.split(',')]
                schedule_list.append(schedule)
            #print(quality)
"""for i in range(len(quality_list)):
    print(quality_list[i])
    print(schedule_list[i])
    print(" ")"""
#print(schedule_list)
index_list = []
new_quality_list =[]
new_schedule_list = []
for i in range(len(quality_list)):
    #print(quality_list[95])
    quality = []
    count = 0
    quality = list(quality_list[i])
    for j in quality:
        if j  == 0:
            count = count +1
    if count > number_of_hard_nles:
        index_list.append(i)
for i in range(len(quality_list)):
    if i not in index_list:
        new_quality_list.append(quality_list[i])
        new_schedule_list.append(schedule_list[i])
    #schedule_list.pop(i)
#print(new_quality_list)
#print(new_schedule_list)
for i in range(0,number_of_methods):
    with open("Method" + str(i+1) + "data.csv", "a") as csv_file:
        writer = csv.writer(csv_file, delimiter = ',')
        writer.writerow(names_list)
        quality = [0] * 30
        for j in range(len(new_quality_list)):
            schedule = list(new_schedule_list[j])
            index = schedule.index(i+1)
            for k in range(0,index):
                quality[schedule[k]-1] = 1
            for k in range(index+1, number_of_methods):
                quality[schedule[k]-1] = 0
            quality[i] = new_quality_list[j][i]
            print(quality)
            print(schedule)
            print(" ")
            writer.writerow(quality)

with open("data_mean.csv", "a") as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    for j in range(len(log_output_lines)):
        log_line = log_output_lines[j].strip()
        if(log_line.split(" ")[0] == "Mean"):
            #print(log_output_lines[j])
            log_mean_string = log_output_lines[j+1].strip()
            mean_string = log_mean_string.strip()
            mean_string = mean_string[1:-1]
            #print(schedule_string)
            mean = [float(s) for s in mean_string.split(',')]
            print(mean)
            writer.writerow(mean)
            break
