from collections import deque
import copy
from Vertex import *
import heapq

class MCS(object):
    def triangulate(self, vertices):

        untagged = []
        filledEdges = []
        orderedVertices = []
        for i in range(len(vertices) -1, -1, -1):
            heapq.heappush(untagged, ((vertices[i].count) * -1, i, vertices[i]) )

        for i in range(len(vertices) - 1, -1, -1):
            #print("untagged: ", untagged)
            vertexTuple = heapq.heappop(untagged)
            vertex = vertexTuple[2]
            vertex.order = i
            vertex.tagged = True
            orderedVertices.append(vertex)
            untagged = list(untagged)
            countIncrease = []
            for j in range(len(untagged)):
                if(self.findPath(untagged[j][2], vertex)):
                    countIncrease.append(j)
                    filledEdges.append((untagged[j][2].name, vertex.name))
            for k in countIncrease:
                vertTuple = untagged[k]
                modified = (vertTuple[0] - 1, vertTuple[1], vertTuple[2])
                untagged[k] = modified

            untag = list(untagged)
            heapq.heapify(untagged)
        return (filledEdges, orderedVertices)


    def findPath(self, source, destination):
        for i in destination.neighbors:

            if source.name == i.name:
                source.count += 1
                return True
        q = deque([source])
        visited = deque([source.name])
        while q:
            node = q.popleft()
            if node.name == destination.name:
                source.count += 1
                return True
            for n in node.neighbors:
                if (n.name not in visited and n.count < source.count and n.tagged == False) or (n.name == destination.name):
                    visited.appendleft(n.name)
                    q.appendleft(n)
        return False





