

class PPC:

    def shortestPath(self, network) -> bool:
        numTimePoints = network.numTimePoints()
        for k in range(0, numTimePoints):
            for i in range(0, k):
                for j in range(0, k):
                    if k in network.successorEdges[i] and k in network.successorEdges[j] and j in network.successorEdges[i] and i != j:
                            network.successorEdges[i][k] = min(network.successorEdges[i][k], network.successorEdges[i][j]
                                                               + network.successorEdges[j][k])
                    if j in network.successorEdges[k] and i in network.successorEdges[k] and j in network.successorEdges[i]:
                            network.successorEdges[k][j] = min(network.successorEdges[k][j],
                                                               network.successorEdges[k][i]
                                                               + network.successorEdges[i][j])
        #network.visualize()
        return network




