import networkx as nx
import matplotlib.pyplot as plt
import graphviz
import tempfile
class AgentLocalView(object):
    def __init__(self, root, taskStructureNames, taskStructureObjects, agentEnvironment, sourceRelationshipTable,
                    destinationRelationshipTable, environmentVariables, constraints):
        self.root = root
        self.taskStructureNames = taskStructureNames
        self.taskStructureObjects = taskStructureObjects
        self.agentEnvironment = agentEnvironment
        self.sourceRelationshipTable = sourceRelationshipTable
        self.destinationRelationshipTable = destinationRelationshipTable
        self.environmentVariables = environmentVariables
        self.constraints = constraints

    def visualize(self):
        G = graphviz.Digraph('G',
                             filename="/Users/ajithvemuri/git/ctaems/Resources/Scheduling/RandomTaskStructures/"
                                      + self.root +'.gv')
        for key, value in self.taskStructureObjects.items():
            if value.type == "Task":
                if value.parent:
                    parentObject = self.taskStructureObjects[value.parent]
                    G.edge(value.parent + "-" + parentObject.qaf + "-" + str(parentObject.qnum) + "-" + parentObject.paf,
                       key + "-" + value.qaf + "-" + str(value.qnum) + "-" + value.paf)
            else:
                parentObject = self.taskStructureObjects[value.parent]
                G.edge(value.parent + "-" + parentObject.qaf + "-" + str(parentObject.qnum) + "-" + parentObject.paf, key)
        for key, value in self.sourceRelationshipTable.items():
            for i in value:
                print("key", key)
                print("value", i)
                src = str(key)
                dest = str(i[0])
                label = str(i[1])
                if i[1] == "Enables" or i[1] == "Disables":
                    G.edge(src, dest, label=label, rank = "same")
                else:
                    G.edge(src, dest, label=label+str(i[5]), rank = "same")
        for i in self.constraints:
            G.edge(i.getSource(), i.getDestination(), label=str(i.getWeight()))

        #G.view()
        G.render(format='pdf', view=False)



