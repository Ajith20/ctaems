import random
from typing import List
from Task import Task
import copy
from EnvironmentVariable import *
from copy import deepcopy


class Method(object):
    def __init__(self, name, agentName, quality, duration, preferences, parentsPreferences, prefParents,
                 parent, status, executed, earliestStartTime, deadline, type):
        self.methodName : str = name
        self.agentName : str = agentName
        self.quality : List[tuple] = quality
        self.duration : List[tuple] = duration
        self.preferences : List[tuple] = preferences
        self.parentsPreferences : List[List[tuple]] = parentsPreferences
        self.prefParents : List[str] = prefParents
        self.parent : Task = parent
        self.enableStatus : bool = status
        self.executed : bool = executed
        self.earliestStartTime : int = earliestStartTime
        self.deadline : int = deadline
        self.type : str = type
        self.prefContexts : list = self.buildPreferenceContexts()
        self.maxPreferenceValue = 0
    def setEnableStatus(self, status):
        self.enableStatus = status

    def expectedQuality(self) -> float:
        quality = 0
        for i in quality:
            quality += i[0] * [1]
        return quality

    def expectedDuration(self) -> float:
        duration = 0
        for i in duration:
            duration += i[0] * i[1]
        return duration

    def minAndMaxDuration(self) -> float:
        maxDur = 0
        minDur = float('inf')
        for i in self.duration:
            if i[0] > maxDur:
                maxDur = i[0]
            if i[0] < minDur:
                minDur = i[0]
        return (minDur, maxDur)

    # Returns a tuple (methodName, minDuration, maxDuration, earliestStartTime, Deadline)
    # If (methodName, 0, 0) is returned then the method cant be executed
    def feasiblePreferenceContext(self, agentEnvironment : dict, smartAgent, type : str) -> tuple:
        #print("in feasible pref context")
        contexts = copy.deepcopy(self.prefContexts)
        #print("contexts", contexts)
        minMaxDur = self.minAndMaxDuration()
        currentTick = smartAgent.currentTick
        while contexts:

            if type == "MAX" or "OPTIMAL":
                context = contexts.pop(0)
            elif type == "RANDOM":
                idx = random.randint(0, len(contexts) - 1)
                context = contexts.pop(idx)
            temp = list(context)
            #print("context: ", context)
            self.maxPreferenceValue = int(temp.pop(0))

            context = tuple(temp)
            ticks = agentEnvironment[context]
            #print("metho name, context and ticks", self.methodName,context, ticks)
            for i in ticks:
                if (currentTick < i[0] and int(i[1] - i[0]) >= minMaxDur[1] and i[0] >= self.earliestStartTime
                        and i[1] <= self.deadline):
                    print("method name and context returned: ", self.methodName,context)
                    return (self.methodName, minMaxDur[0], minMaxDur[1], i[0], i[1])
                elif (currentTick >= i[0] and (i[1] - currentTick) >= minMaxDur[1] and i[0] >= self.earliestStartTime
                        and i[1] <= self.deadline):
                    print("method name and context returned: ", self.methodName, context)
                    return (self.methodName, minMaxDur[0], minMaxDur[1], currentTick, i[1])
        return (self.methodName, minMaxDur[0], minMaxDur[1], 0, 0)

    def buildPreferenceContexts(self) -> list:
        contexts = self.preferences
        #print("method name and contexts", self.methodName, contexts)
        return contexts


    """def buildPreferenceContexts(self) -> list:
        contexts = []
        for i in self.preferences:
            methodPref = i[0]
            conTuple = () + (methodPref,)
            for j in range(1, len(i)):
                for k in self.parentsPreferences:
                    for l in k:
                        if l[1] == i[j]:
                            methodPref = methodPref + l[0]
                            conTuple = conTuple + (i[j],)
                            conList = list(conTuple)
                            conList[0] = methodPref
                            conTuple = tuple(conList)
            contexts.append(conTuple)
        sortedContexts = sorted(contexts, key=lambda x: -x[0])
        return sortedContexts"""

    def getMaxPreferenceValue(self) -> int:
        return self.maxPreferenceValue








    

        




