from typing import List


class Task(object):
    def __init__(self, name, agentName, qaf, paf, qnum, parent, children, enableStatus, executed,  earliestStartTime, deadline, type):
        self.taskName : str = name
        self.agentName : List[str] = agentName
        self.qaf : str = qaf
        self.paf : str = paf
        self.qnum : int = qnum
        self.parent : Task = parent
        self.children : List[str] = children
        self.enableStatus : bool = enableStatus
        self.executed : bool = executed
        self.earliestStartTime : int = earliestStartTime
        self.deadline : int = deadline
        self.accumulatedPreference : int = 0
        self.type = type

    def setAccumulatedPreference(self, accumulatedPreference):
        self.accumulatedPreference = accumulatedPreference
    
    def getAccumulatedPreference(self) -> int:
        return self.accumulatedPreference
    
    def getQAF(self) -> str:
        return self.qaf
    
    def getPAF(self) -> str:
        return self.paf

    def getChildren(self) -> List[str]:
        return self.children
    
    def getParent(self) -> str:
        return self.parent

    def getEnableStatus(self) -> bool:
        return self.enableStatus

    def setEnableStatus(self, enableStatus):
        self.enableStatus = enableStatus
    
    def getExecutedStatus(self) -> bool:
        return self.executedStatus

    def getEarliestStartTime(self) -> int:
        return self.earliestStartTime

    def getDeadline(self) -> int:
        return self.deadline

    def getQnum(self) -> int:
        return self.qnum


    