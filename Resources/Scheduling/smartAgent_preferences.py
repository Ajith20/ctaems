import agent
import time
import json
import sys
import random
from collections import defaultdict
from Method import Method
from Task import Task
from Constraint import Constraint
from EnvironmentVariable import EnvironmentVariable
from AgentLocalView import AgentLocalView
from GenerateSchedule import *

class smartAgent(agent.Agent):

    def __init__(self, agentName, simAddr, port):
        root = None
        random_schedule = []
        self.root = ""
        self.currentTick = 0
        self.agentEnvironment = defaultdict(list)
        self.sourceRelationshipTable = defaultdict(list)
        self.destinationRelationshipTable = defaultdict(list)
        self.environmentVariables = []
        self.constraints = []
        self.agentLocalView = None
        #taskStrucuturesNames is a mapping from method or subtask names to their respective children names
        self.taskStructureNames = {}
        ##taskStructureObjects is a mapping from method or subtaks names to their objects 
        self.taskStructureObjects = {}
        # self.random_schedule = random.sample(range(1, 31), 30)
        self.random_schedule = [1, 2, 3]
        print(self.random_schedule)
        print("hello agent1")
        self.counter = 0
        self.simulator_address = simAddr
        self.simulator_port = port
        self.name = agentName
        self.start()
        self.loop()

    # Returns the name of the method with highest quality

    def runMethod(self):
        print("\n Methods avaiable")
        print(str(self.available_methods))
        print("")
        method_number = self.random_schedule.pop(0)
        method_name = "Method" + str(method_number)
        print(method_name)
        for m in range(len(self.available_methods)):
            print("preferences of each method with method name: ",
                  self.available_methods[m][0], self.available_methods[m][3])
        return method_name
        for m in range(len(self.available_methods)):
            print("preferences of each method with method name: ",
                  self.available_methods[m][0], self.available_methods[m][3])


    # Adds all the nodes to the agent tree and puts them in unavailable methods
    def initial_tree_message(self, message):
        self.root = message["Root"]
        print("Root name: ", self.root)
        self.buildTaskStructure(message)
        self.buildConstraints(message)
        self.buildAgentEnvironment(message)
        self.buildRelationshipTables(message)
        self.buildEnvironmentalVariables(message)
        #Disable required methods
        for keys, values in self.sourceRelationshipTable.items():
            print("Key and values: ", keys, values)
            for i in values:
                if i[1] == "Enables":
                    methodName = i[0]
                    methodObject = self.taskStructureObjects[methodName]
                    methodObject.setEnableStatus(False)


        self.agentLocalView = AgentLocalView(root = self.root, taskStructureNames=self.taskStructureNames,
                                             taskStructureObjects=self.taskStructureObjects,
                                            agentEnvironment=self.agentEnvironment, sourceRelationshipTable=self.sourceRelationshipTable,
                                        destinationRelationshipTable=self.destinationRelationshipTable, 
                                        environmentVariables=self.environmentVariables, constraints=self.constraints)
        generateSchedule = GenerateSchedule(self.agentLocalView, self, self.root)
        generateSchedule.buildSTN("OPTIMAL", self.root)
        generateSchedule.buildSTN("MAX", self.root)
        #print("Now generating random schedule")
        generateSchedule.buildSTN("RANDOM", self.root)
        #generateSchedule.buildSTN("OPTIMAL", self.root)
        print("exiting")
        self.runFlag = False
        self.s.socket.close()
        self.agentLocalView.visualize()



    def buildTaskStructure(self, message):

        for node in message["Message"]["Nodes"]:
            parent = ""
            if "QAF" in node:
                for key, values in self.taskStructureNames.items():
                    if values and node['NodeName'] in values:
                        parent = key

                task = Task(name=node["NodeName"], agentName=node["VisibleToAgents"], qaf=node["QAF"],
                            paf=node["PAF"],
                            qnum=node["qnum"], parent=parent, children=node["SubTasks"], enableStatus=False, executed=False,
                            earliestStartTime=node["EarliestStartTime"], deadline=node["Deadline"], type = "Task")
                self.taskStructureNames[node['NodeName']] = node["SubTasks"]
                #print("task name and children", node["NodeName"], self.taskStructureNames[node['NodeName']])
                self.taskStructureObjects[node['NodeName']] = task
        for node in message["Message"]["Nodes"]:
            if "Quality" in node:
                if len(self.taskStructureNames) != 0:
                    for key, values in self.taskStructureNames.items():
                        if values and node['NodeName'] in values:
                            parent = key
                            break
                quality = node["Quality"].split('),')
                duration = node["Duration"].split('),')
                preferences = node["Preferences"].split(')')
                prefParents = node["Parents"].split(',')
                parentsPreferencesTuple = [[]]
                for i in node["ParentsPreferences"]:
                    if i:
                        parentsPref = i.split(')')
                        preferencesTuple = []
                        for j in parentsPref:
                            if j:
                                pTuple = ()
                                k = j[1:].split(' ')
                                pTuple = pTuple + (int(k[0]),)
                                for l in range(1, len(k)):
                                    pTuple = pTuple + (k[l],)
                                preferencesTuple.append(pTuple)
                        sortedPref = sorted(preferencesTuple, key=lambda x: -x[0])
                        parentsPreferencesTuple.append(sortedPref)
                parentsPreferencesTuple = [x for x in parentsPreferencesTuple if x != []]
                qualityTuple = []
                durationTuple = []
                preferencesTuple = []
                for i in quality:
                    i = i.replace(")", "")
                    j = i[1:].split(',')
                    qualityTuple.append((int(float(j[0])), int(float(j[1]))))
                for i in duration:
                    j = i[1:-1].split(',')
                    durationTuple.append((int(float(j[0])), int(float(j[1]))))
                for i in preferences:
                    if i:
                        pTuple = ()
                        j = i[1:].split(' ')
                        pTuple = pTuple + (int(j[0]),)
                        for k in range(1, len(j)):
                            pTuple = pTuple + (j[k],)
                        preferencesTuple.append(pTuple)
                sortedPreferences = sorted(preferencesTuple, key=lambda x: -x[0])
                method = Method(name=node["NodeName"], agentName=node["AgentName"], quality=qualityTuple,
                                duration=durationTuple, preferences=sortedPreferences, parentsPreferences=parentsPreferencesTuple ,
                                parent=parent,
                                prefParents=prefParents, status = True, executed = False,
                                earliestStartTime=self.taskStructureObjects[parent].getEarliestStartTime(),
                                deadline=self.taskStructureObjects[parent].getDeadline(), type = "Method")
                print("method name and its parent: ", node["NodeName"], self.taskStructureObjects[parent].taskName)
                self.taskStructureNames[node["NodeName"]] = None
                self.taskStructureObjects[node["NodeName"]] = method
        #print("In smartAgent: ",self.taskStructureObjects)

    # self.contraints =[contraints_objects]
    def buildConstraints(self, message):
        for constraint in message["Message"]["Constraints"]:
            if constraint:
                constraint = Constraint(name=constraint["ConstraintName"], source=constraint["Source"],
                                        destination=constraint["Destination"], weight=constraint["Weight"])
                self.constraints.append(constraint)
    #self.agentEnvironments[home, cloudy] = [[1, 100]]
    def buildAgentEnvironment(self, message):
        envi = message["Message"]["AgentEnvironment"][0]
        for context in envi["Contexts"]:
            temp = context.split(":")
            context = temp[0].split(",")
            contextTuple = ()
            for i in context:
                contextTuple = contextTuple + (i,)
            if contextTuple in self.agentEnvironment:
                ticks = [int(temp[1]), int(temp[2])]
                self.agentEnvironment[contextTuple].append(
                    ticks)
            else:
                ticks = [int(temp[1]), int(temp[2])]
                self.agentEnvironment[contextTuple] = [ticks]

    def buildRelationshipTables(self, message):
        for relationship in message["Message"]["Relationships"]:
            source = relationship["Source"]
            destination = relationship["Destination"]
            relationshipType = relationship["RelationshipType"]
            relationshipName = relationship["RelationshipName"]
            if (relationship["RelationshipType"] == "Enables" or relationship["RelationshipType"] == "Disables"):
                # source (methodName) -> [Destination(methodName), relationshipType, relationshipName]
                r = [destination, relationshipType, relationshipName]
                if source not in self.sourceRelationshipTable:
                    self.sourceRelationshipTable[source] = [r]
                else:
                    self.sourceRelationshipTable[source].append(r)
                r = [source, relationshipType, relationshipName]
                if destination not in self.destinationRelationshipTable:
                    self.destinationRelationshipTable[destination] = [r]
                else:
                   self.destinationRelationshipTable[destination].append(r)
            else:
                # source (methodName) -> [Destination(methodName), relationshipType, relationshipName, 
                # QualityFactor, DurationFactor, PreferenceFactor]
                r = [destination, relationshipType, relationshipName, relationship["QualityFactor"],
                        relationship["DurationFactor"], relationship["PreferenceFactor"]]
                
                if source not in self.sourceRelationshipTable:
                    self.sourceRelationshipTable[source] =  [r]
                else:
                    self.sourceRelationshipTable[source].append(r)
                r = [source, relationshipType, relationshipName, relationship["QualityFactor"],
                        relationship["DurationFactor"], relationship["PreferenceFactor"]]
                if destination not in self.destinationRelationshipTable:
                    self.destinationRelationshipTable[destination] = [r]
                else:
                    self.destinationRelationshipTable[destination].append(r)
    
    def buildEnvironmentalVariables(self, message):
        
        for variableList in message["Message"]["AgentEnvironmentVariables"]:
            for i in variableList["Preferences"]:
                preferences = []
                pref = i.split(":")
                name = pref[0]
                parents = pref[1].split(" ")
                prefValues = pref[2].split(" ")
                p = ()
                for j in prefValues:
                    if j.isdigit():
                        if p:

                            preferences.append(p)
                            p = ()
                        p = p + (int(j),)
                    else:
                        p = p + (j,)
                preferences.append(p)
                sortedPreferences = sorted(preferences, key=lambda x: -x[0])
                enviVariable = EnvironmentVariable(parents, name, sortedPreferences)
                self.environmentVariables.append(enviVariable)
        
    def next_tick_message(self, message):
        print("in next tick")
        self.currentTick = int(message['Message']['Tick'])
        """if self.currentTick == 1:
            generateSchedule = GenerateSchedule(self.agentLocalView, self)
            generateSchedule.buildSTN("Max")
            print("Now generating random schedule")
            generateSchedule.buildSTN("Random")
            print("exiting")
            self.runFlag = False
            self.s.socket.close()
            #exit()"""

        """count = 1
        for m in self.available_methods:
            if (self.root.findNode(m[0]).compleated):
                self.available_methods.remove(m)
                print("REMOVING")

        if self.available_methods and self.running_method is None and self.requested_method is None and self.random_schedule:
            print("count", count)
            self.ask_to_start_method(self.runMethod())
            count = count + 1

        if len(self.random_schedule) == 0 and self.available_methods:
           for i in range(len(self.available_methods)):
                tmp = self.available_methods.pop(i)
                self.unavailable_methods.append(tmp)
            self.available_methods = []
            self.unavailable_methods = []
        if (len(self.available_methods) == 0 and int(message['Message']['Tick']) % 5 == 0):
            for m in self.unavailable_methods:
                print("m value")
                print(m)
                method = {}
                method["MessageType"] = "AskMethodStatusMessage"
                method["Message"] = {}
                method["Message"]["MsgSender"] = self.name
                method["Message"]["MsgDest"] = "SIMULATOR"
                method["Message"]["MethodName"] = m[0]
                self.s.send(bytes(json.dumps(method)+"\n", encoding='utf8'))"""

    def confirm_method_start_message(self, message):
        if message['Message']['Started']:
            self.running_method = self.requested_method
            for i in range(len(self.sim_agents)):
                if (self.sim_agents[i] != myName):
                    outMesssage = {}
                    outMesssage["MessageType"] = "AgentToAgentMessage"
                    outMesssage["Message"] = {}
                    outMesssage["Message"]["MsgSender"] = self.name
                    outMesssage["Message"]["MsgDest"] = self.sim_agents[i]
                    outMesssage["Message"]["Content"] = {}
                    outMesssage["Message"]["Content"]["Load"] = len(
                        self.available_methods)
                    print("self.running_method")
                    print(self.running_method)
                    outMesssage["Message"]["Content"]["Running"] = self.root.findNode(
                        self.running_method).data
                    outMesssage["Message"]["Content"]["Compleated"] = ""
                    self.s.send(
                        bytes(json.dumps(outMesssage)+"\n", encoding='utf8'))

        else:
            for i in range(len(self.available_methods)):
                if self.available_methods[i][0] == message['Message']['MethodName']:
                    tmp = self.available_methods.pop(i)
                    self.unavailable_methods.append(tmp)
                    print("\n\n ADDING METHOD TO unavailable_methods \n\n")
                    break
        self.requested_method = None

    def confirm_method_Abort_message(self, message):
        self.running_method = None
        self.requested_method = None
        for i in range(len(self.available_methods)):
            if self.available_methods[i][0] == message['Message']['MethodName']:
                tmp = self.available_methods.pop(i)
                self.unavailable_methods.append(tmp)
                print("\n\n ADDING METHOD TO unavailable_methods \n\n")
                break

    def agent_to_agent_message(self, message):
        print("\n")
        print(message)
        print(message['Message']['Content']['Running'])
        if (message['Message']['Content']['Compleated'] == ""):
            if (self.root.addNode(message['Message']['Content']['Running'])):
                print("added node to tree")
                if (message['Message']['MsgSender'] > self.name):
                    node = self.root.findNode(
                        message['Message']['Content']['Running']['NodeName'])
                    if (node.parent.data['QAF'] == 'OR'):
                        for n in node.parent.data['SubTasks']:
                            if (n == self.running_method):
                                print("aborting method: ", self.running_method)
                                self.sendAbortMessage(self.running_method)
                                break
        else:
            self.root.setNodeCompleated(
                message['Message']['Content']['Compleated'])
            node = self.root.findNode(
                message['Message']['Content']['Compleated'])
            if (node):
                if (node.parent.data['QAF'] == 'OR'):
                    for n in node.parent.data['SubTasks']:
                        self.root.setNodeCompleated(n)
                        if (n == self.running_method):
                            print("aborting method: ", self.running_method)
                            self.sendAbortMessage(self.running_method)

        print("\n")

    def sendAbortMessage(self, name):
        method = {}
        method["MessageType"] = "AbortMethodMessage"
        method["Message"] = {}
        method["Message"]["MsgSender"] = self.name
        method["Message"]["MsgDest"] = "SIMULATOR"
        method["Message"]["MethodName"] = name
        self.s.send(bytes(json.dumps(method)+"\n", encoding='utf8'))

    def notify_method_completed_message(self, message):
        self.running_method = None
#        print("Removing " + message['Message']['MethodName'] + " from avaiable Methods")
        for i in range(len(self.available_methods)):
            if self.available_methods[i][0] == message['Message']['MethodName']:
                self.available_methods.pop(i)
                break
        self.root.setNodeCompleated(message['Message']['MethodName'])
        for i in range(len(self.sim_agents)):
            if (self.sim_agents[i] != myName):
                outMesssage = {}
                outMesssage["MessageType"] = "AgentToAgentMessage"
                outMesssage["Message"] = {}
                outMesssage["Message"]["MsgSender"] = self.name
                outMesssage["Message"]["MsgDest"] = self.sim_agents[i]
                outMesssage["Message"]["Content"] = {}
                outMesssage["Message"]["Content"]["Load"] = len(
                    self.available_methods)
                outMesssage["Message"]["Content"]["Running"] = ""
                outMesssage["Message"]["Content"]["Compleated"] = message['Message']['MethodName']
                self.s.send(
                    bytes(json.dumps(outMesssage)+"\n", encoding='utf8'))

    def notify_relationship_activation_message(self, message):
        if (message["Message"]["Type"] == "Disables"):
            i = 0
            while i < len(self.available_methods):
                if (self.available_methods[i][0] == message["Message"]["Target"]):
                    tmp = self.available_methods.pop(i)
                    self.unavailable_methods.append(tmp)
#                    print("\n\n Removing disabled method\n\n")
                i += 1

        elif (message["Message"]["Type"] == "Enables"):
            i = 0
            while i < len(self.unavailable_methods):
                if (self.unavailable_methods[i][0] == message["Message"]["Target"]):
                    tmp = self.unavailable_methods.pop(i)
                    self.available_methods.append(tmp)
#                    print("\n\n adding in enabled method\n\n")
                i += 1

    def end_simulation_message(self, message):
        print("End of Simulation")
        exit()


myName = sys.argv[1]
agent1 = smartAgent(myName, 'localhost', 9876)
