class Constraint(object):
    def __init__(self, name, source, destination, weight):
        self.name : str = name
        self.source : str = source
        self.destination : str = destination
        self.weight : float = weight

    def getName(self) -> str:
        return self.name
    
    def getSource(self) -> str:
        return self.source
    
    def getDestination(self) -> str:
        return self.destination
    
    def getWeight(self) -> float:
        return self.weight