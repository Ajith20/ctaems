import random
import math

import numpy as np
from scipy.stats import truncnorm


class RandomTaskStructures(object):
    def __init__(self):
        self.contextList = []
        self.space = ' '
        self.methodPrefContexts = []
    def readFile(self):
        self.contextList = []
        self.space = ' '
        self.noOfTaskStructure = 0
        self.noOfAgents = []
        self.agentNames = []
        self.depth = []
        self.breadth = []
        self.andNodes = []
        self.qNum = []
        self.earliestTime = []
        self.deadLine = []
        self.methods = []
        self.tightness = []
        self.enviVariables = []
        self.preferences = []
        self.prefTightness = []
        self.contexts = []
        self.enables = []
        self.disables = []
        self.facilitates = []
        self.hinders = []
        self.synchronization = []
        self.constraints = []
        self.uncertainQuality = []
        self.numAgents = 0
        self.numMethods = 0
        self.weights = []
        self.numParents = 0
        self.numContexts = 0
        self.contextCounter = 1
        self.frequency = []
        f = open("/Users/ajithvemuri/git/ctaems/Resources/taskStrInput.txt", "r")
        for line in f:
            values = line.split(":")
            if values[0] == "TaskStructures":
                self.noOfTaskStructure = int(values[1])
            elif values[0] == "Agents":
                minMax = values[1].replace(" ", "").split("-")
                self.noOfAgents.append(int(minMax[0].strip()))
                self.noOfAgents.append(int(minMax[1].strip()))
            elif values[0] == "Depth":
                minMax = values[1]. replace(" ", "").split("-")
                self.depth.append(int(minMax[0].strip()))
                self.depth.append(int(minMax[1].strip()))
            elif values[0] == "Breadth":
                minMax = values[1].replace(" ", "").split("-")
                self.breadth.append(int(minMax[0].strip()))
                self.breadth.append(int(minMax[1].strip()))
            elif values[0] == "ANDNodes":
                minMax = values[1].replace(" ", "").split("-")
                self.andNodes.append(int(minMax[0].strip()))
                self.andNodes.append(int(minMax[1].strip()))
            elif values[0] == "QNUM":
                minMax = values[1].replace(" ", "").split("-")
                self.qNum.append(int(minMax[0].strip()))
                self.qNum.append(int(minMax[1].strip()))
            elif values[0] == "EarliestTime":
                minMax = values[1].replace(" ", "").split("-")
                self.earliestTime.append(int(minMax[0].strip()))
                self.earliestTime.append(int(minMax[1].strip()))
            elif values[0] == "Deadline":
                minMax = values[1].replace(" ", "").split("-")
                self.deadLine.append(int(minMax[0].strip()))
                self.deadLine.append(int(minMax[1].strip()))
            elif values[0] == "Parallel":
                minMax = values[1].replace(" ", "")
                if minMax == "Yes":
                    self.parallel = True
                else:
                    self.parallel = False
            elif values[0] == "UncertainQuality":
                minMax = values[1].replace(" ", "").split("-")
                if minMax == "Yes":
                    self.uncertainQuality = True
                else:
                    self.uncertainQuality = False
            elif values[0] == "Methods":
                minMax = values[1].replace(" ", "").split("-")
                self.methods.append(int(minMax[0].strip()))
                self.methods.append(int(minMax[1].strip()))
            elif values[0] == "Tightness":
                minMax = values[1].replace(" ", "").split("-")
                self.tightness.append(minMax[0].strip())
                self.tightness.append(minMax[1].strip())
            elif values[0] == "PrefTightness":
                minMax = values[1].replace(" ", "").split("-")
                self.prefTightness.append(minMax[0].strip())
                self.prefTightness.append(minMax[1].strip())
            elif values[0] == "EnviVariables":
                minMax = values[1].replace(" ", "").split("-")
                self.enviVariables.append(int(minMax[0].strip()))
                self.enviVariables.append(int(minMax[1].strip()))
            elif values[0] == "Preferences":
                minMax = values[1].replace(" ", "").split("-")
                self.preferences.append(int(minMax[0].strip()))
                self.preferences.append(int(minMax[1].strip()))
            elif values[0] == "Contexts":
                minMax = values[1].replace(" ", "").split("-")
                self.contexts.append(int(minMax[0].strip()))
                self.contexts.append(int(minMax[1].strip()))
            elif values[0] == "Enables":
                minMax = values[1].replace(" ", "").split("-")
                self.enables.append(int(minMax[0].strip()))
                self.enables.append(int(minMax[1].strip()))
            elif values[0] == "Disables":
                minMax = values[1].replace(" ", "").split("-")
                self.disables.append(int(minMax[0].strip()))
                self.disables.append(int(minMax[1].strip()))
            elif values[0] == "Facilitates":
                minMax = values[1].replace(" ", "").split("-")
                self.facilitates.append(int(minMax[0].strip()))
                self.facilitates.append(int(minMax[1].strip()))
            elif values[0] == "Hinders":
                minMax = values[1].replace(" ", "").split("-")
                self.hinders.append(int(minMax[0].strip()))
                self.hinders.append(int(minMax[1].strip()))
            elif values[0] == "Synchronization":
                minMax = values[1].replace(" ", "").split("-")
                self.synchronization.append(int(minMax[0].strip()))
                self.synchronization.append(int(minMax[1].strip()))
            elif values[0] == "Constraints":
                minMax = values[1].replace(" ", "").split("-")
                self.constraints.append(int(minMax[0].strip()))
                self.constraints.append(int(minMax[1].strip()))
            elif values[0] == "Weights":
                minMax = values[1].replace(" ", "").split("-")
                self.weights.append(int(minMax[0].strip()))
                self.weights.append(int(minMax[1].strip()))
            elif values[0] == "Frequency":
                minMax = values[1].replace(" ", "").split("-")
                self.frequency.append(int(minMax[0].strip()))
                self.frequency.append(int(minMax[1].strip()))


    def createRTS(self):
        for i in range(1, self.noOfTaskStructure + 1):
            self.readFile()
            file = open("/Users/ajithvemuri/git/ctaems/Resources/Scheduling/RandomTaskStructures/" +
                        "RTS" + str(i) + ".ctaems", 'a')
            obj.writeAgents(file)
            obj.writeTasks(file, i)
    def writeAgents(self, file):
        self.numAgents = random.randint(self.noOfAgents[0], self.noOfAgents[1])
        file.write("(spec_version \"V2_0\")")
        file.write("\n\n")
        for i in range(1, self.numAgents + 1):
           file.write("(spec_agent\n")
           file.write(self.space * 2 + "(label Agent" + str(i) + ")\n")
           self.agentNames.append("Agent" + str(i))
           file.write(")\n\n")
        file.write("\n\n")
    #Randomly sets tree parameters and returns the same
    def setTreeParameters(self):
        breadth = 0
        depth = 0
        totalTasks = 0
        # holds the num of subtasks each task has, split is random
        decompositionList = []
        numMethods = 0
        while (totalTasks <= depth):
            breadth = random.randint(self.breadth[0], self.breadth[1])
            depth = random.randint(self.depth[0], self.depth[1])
            totalTasks = breadth * depth
        print('total tasks', totalTasks)

        andPercent = random.randint(self.andNodes[0], self.andNodes[1])
        numAndNodes = (andPercent * totalTasks) // 100
        numOrNodes = totalTasks - numAndNodes
        qnum = round(random.randint(self.qNum[0], self.qNum[1]), -3)
        earliestTime = random.randint(self.earliestTime[0], 100)
        deadline = random.randint(self.deadLine[0], self.deadLine[1])
        # print("totalTasks: ", totalTasks, depth)
        #holds the number of tasks at each level
        breadthList = self.divideNum(totalTasks, depth)
        while (numMethods <= breadthList[-1] or numMethods < self.numAgents):
            num = self.getNormal(int((self.methods[0] + self.methods[1]) / 2), 2, self.methods[0], self.methods[1])
            numMethods = int(num.rvs())
            #numMethods = random.randint(self.methods[0], self.methods[1])
        self.numMethods = numMethods
        agentList = self.agentListForMethods(numMethods)
        breadthList.append(self.numMethods)
        print("numMethods", numMethods)
        print("breadth list", breadthList)
        for i in range(len(breadthList) - 1):
            #print("breadlist nums", breadthList[i+1], breadthList[i])
            returnList = self.divideNum(breadthList[i + 1], breadthList[i])
            random.shuffle(returnList)
            decompositionList.extend(returnList)

        decompositionList.insert(0, breadthList[0])
        #print("decon list ", decompositionList)
        return [numAndNodes, numOrNodes, qnum, earliestTime, deadline, breadthList, decompositionList, agentList]

    def flip(self) -> int:
        return random.randint(0, 1)
    def writeTasks(self, file, rtsNumber):
        numAndNodes, numOrNodes, qnum, earliestTime, deadline, breadthList, \
            decompositionList, agentList = self.setTreeParameters()
        print("agent list", agentList)

        closedList = []
        openList = []
        taskCounter = 1
        MethodCounter = 1
        node = None
        while(decompositionList):
            #print("decomposition list len", len(decompositionList))
            taskStr = ""
            numNodes = decompositionList.pop(0)
            time = 0
            if openList:
                node = openList.pop(0)
                earliestTime = node[2]
                deadline = node[3]
                qnum = node[1]


            if numOrNodes == 0 or numNodes == 1:
                qaf = "q_and"
                paf = "p_sum"
                numAndNodes -= 1
            elif numAndNodes == 0:
                qaf = "q_or"
                paf = "p_max"
                numOrNodes -= 1
            else:
                flip = self.flip()
                qaf = ""
                paf = ""
                if flip == 0 or numNodes == 1:
                    qaf = "q_and"
                    paf = "p_sum"
                    numAndNodes -= 1
                else:
                    qaf = "q_or"
                    paf = "p_max"
                    numOrNodes -= 1
            overlap = random.randint(1,10) / 10
            qnumDecompose = self.divideNum(qnum, numNodes)
            if self.parallel:
                timeDecompose = self.overlapRange(earliestTime, deadline, numNodes, overlap)
            else:
                timeDecompose = self.divideRange(earliestTime, deadline, numNodes)
            #print("len decom and breadthlist ", len(decompositionList), breadthList[-2])
            if len(decompositionList) + 1 > breadthList[-2]:
                #print("in if", numNodes)
                for i in range(numNodes):
                    time = timeDecompose.pop(0)
                    num = qnumDecompose.pop(0)
                    if qaf == "q_or" or numNodes == 1:
                        print("node name and time: ", "T" + str(taskCounter), time[0], time[1])
                        openList.append(("T" + str(taskCounter), qnum, time[0], time[1]))
                    else:
                        print("node name and time: ", "T" + str(taskCounter), time[0], time[1])
                        openList.append(("T" + str(taskCounter), num, time[0], time[1]))
                    if taskStr:
                        taskStr = taskStr + " " + "T" + str(taskCounter)
                    else:
                        taskStr = "T" + str(taskCounter)
                    taskCounter += 1
            else:
                #print("in else", numNodes)
                for i in range(numNodes):
                    tightness = random.choice(self.tightness)
                    if tightness == "Loose":
                        duration = int(((node[3] - node[2]) * 25 ) / 100)
                        if duration < numNodes:
                            duration = duration + numNodes - duration
                        #print("duration and numNodes", duration, numNodes)
                        durationDecompose = self.durDecomposition(duration, numNodes)
                        #print("duration", duration)
                        #print("durationDecom", durationDecompose)
                    elif tightness == "Normal":
                        duration = int(((node[3] - node[2]) * 60) / 100)
                        if duration < numNodes:
                            duration = duration + numNodes - duration
                        #print("duration and numNodes", duration, numNodes)
                        durationDecompose = self.durDecomposition(duration, numNodes)
                    else:
                        duration = int(((node[3] - node[2]) * 80) / 100)
                        if duration < numNodes:
                            duration = duration + numNodes - duration
                        #print("duration and numNodes", duration, numNodes)
                        durationDecompose = self.durDecomposition(duration, numNodes)
                        #print("durationDecom", durationDecompose)
                    if qaf == "q_or":
                        openList.append(("M" + str(MethodCounter), qnum, durationDecompose.pop(0)))
                    else:
                        openList.append(("M" + str(MethodCounter), qnumDecompose.pop(0),
                                         durationDecompose.pop(0)))
                    if taskStr:
                        taskStr = taskStr + " " + "M" + str(MethodCounter)
                    else:
                        taskStr = "M" + str(MethodCounter)
                    if not self.parallel:
                        earliestTime = time + 1
                    else:
                        earliestTime = random.randint(earliestTime, time)
                    MethodCounter += 1

            if not closedList:
                # the node is root
                print("called 1")
                self.writeRoot(file, taskStr, qaf, paf, qnum, rtsNumber)
                closedList.append("Root")
            elif len(decompositionList) + 1 > breadthList[-2]:
                print("called 2")
                self.writeSubTaskswithSubTasks(file, node, taskStr, qaf, paf)
                closedList.append(node)
            elif len(decompositionList) + 1 <= breadthList[-2]:
                print("called 3")
                self.writeSubtasksWithMethods(file, node, taskStr, qaf, paf)
                closedList.append(node)
        numEnvi = random.randint(self.enviVariables[0], self.enviVariables[1])
        for i in range(1, numEnvi + 1):
            self.writeEnvironmentalVariables(file)
        for i in openList:
            #print(i)
            self.writeMethods(file, i, agentList.pop(0))
        numEnables = random.randint(self.enables[0], self.enables[1])
        if numEnables > 0:
            for i in range(1, numEnables + 1):
                self.writeEnables(file, i)
        numDisables = random.randint(self.disables[0], self.disables[1])
        if numDisables > 0:
            for i in range(1, numDisables + 1):
                self.writeDisables(file, i)
        numFacilitates = random.randint(self.facilitates[0], self.facilitates[1])
        if numFacilitates > 0:
            for i in range(1, numFacilitates + 1):
                self.writeFacilitates(file, i)
        numHinders = random.randint(self.hinders[0], self.hinders[1])
        if numHinders > 0:
            for i in range(1, numHinders + 1):
                self.writeHinders(file, i)
        numSynchronization = random.randint(self.synchronization[0], self.synchronization[1])
        for i in range(1, numSynchronization + 1):
            self.writeSynchronization(file, i)
        numConstraints = random.randint(self.constraints[0], self.constraints[1])
        for i in range(1, numConstraints + 1):
            self.writeConstraints(file, i)



    def writeRoot(self, file, taskStr, qaf, paf, qnum, rtsNumber):
        file.write("(spec_task_group\n")
        file.write(self.space * 3 + "(label " + "RTS" + str(rtsNumber) + ")\n")
        file.write(self.space * 3 + "(subtasks " + taskStr + ")\n")
        file.write(self.space * 3 + "(qaf " + qaf + ")\n")
        file.write(self.space * 3 + "(paf " + paf + ")\n")
        file.write(self.space * 3 + "(qnum " + str(round(qnum, -1)) + ")\n" + ")" + "\n\n")

    def writeSubTaskswithSubTasks(self, file, node, taskStr, qaf, paf):
        file.write("(spec_task\n")
        file.write(self.space * 3 + "(label " + node[0] + ")\n")
        file.write(self.space * 3 + "(earliest_start_time " + str(node[2]) + ")\n")
        file.write(self.space * 3 + "(deadline " + str(node[3]) + ")\n")
        file.write(self.space * 3 + "(subtasks " + taskStr + ")\n")
        file.write(self.space * 3 + "(qaf " + qaf + ")\n")
        file.write(self.space * 3 + "(paf " + paf + ")\n")
        file.write(self.space * 3 + "(qnum " + str(node[1]) + ")\n" + ")" + "\n\n")

    def writeSubtasksWithMethods(self, file, node, taskStr, qaf, paf):
        file.write("(spec_task\n")
        file.write("(label " + node[0] + ")\n")
        file.write(self.space * 3 + "(earliest_start_time " + str(node[2]) + ")\n")
        file.write(self.space * 3 + "(deadline " + str(node[3]) + ")\n")
        file.write(self.space * 3 + "(subtasks " + taskStr + ")\n")
        file.write(self.space * 3 + "(qaf " + qaf + ")\n")
        file.write(self.space * 3 + "(paf " + paf + ")\n")
        file.write(self.space * 3 + "(qnum " + str(node[1]) + ")\n" + ")" + "\n\n")

    def writeMethods(self, file, node, agent):
        quality = self.generateQualityDistribution(node[1])
        duration = self.generateDurationDistribution(node[2])
        file.write("(spec_method\n")
        file.write(self.space * 3 + "(label " + node[0] + ")\n")
        file.write(self.space * 3 + "(agent " + "Agent" + str(agent) + ")\n")
        file.write(self.space * 3 +  "(outcomes\n")
        file.write(self.space * 6 + "("+ (node[0]) + "A_o0\n")
        file.write(self.space * 9 + "(density 1.0)\n")
        file.write(self.space * 9 + "(quality_distribution " + quality + ")\n")
        file.write(self.space * 9 + "(duration_distribution " + duration + ")\n")
        file.write(self.space * 9 + "(parents location weather | 11 home 6 work | 9 sunny 2 rainy)\n")
        file.write(self.space * 9 + "(preferences " + self.generatePreferences(node[0]) + ")\n\t\t)\n\t)\n)\n\n")

    def writeEnables(self, file, i):
        """(spec_disables
   (label disables1)
   (from M1)
   (to M3)

)"""
        fromNode = 0
        toNode = 0
        while (fromNode == toNode):
            fromNode = random.randint(1, self.numMethods)
            toNode = random.randint(1, self.numMethods)
        file.write("(spec_enables\n")
        file.write(self.space * 3 + "(label enables" + str(i) + ")\n")
        file.write(self.space * 3 + "(from " + "M" + str(fromNode) + ")\n")
        file.write(self.space * 3 + "(to " + "M" + str(toNode) + ")\n)\n\n")

    def writeDisables(self, file, i):
        fromNode = 0
        toNode = 0
        while (fromNode == toNode):
            fromNode = random.randint(1, self.numMethods)
            toNode = random.randint(1, self.numMethods)
        file.write("(spec_disables\n")
        file.write(self.space * 3 + "(label disables" + str(i) + ")\n")
        file.write(self.space * 3 + "(from " + "M" + str(fromNode) + ")\n")
        file.write(self.space * 3 + "(to " + "M" + str(toNode) + ")\n)\n\n")

    def writeFacilitates(self, file, label):

        fromNode = 0
        toNode = 0
        fromNodeContext = ""
        toNodeContext = ""
        while (fromNode == toNode):
            fromNode = random.randint(1, self.numMethods)
            toNode = random.randint(1, self.numMethods)
        for i in self.methodPrefContexts:
            if i[0] == "M" + str(fromNode):
                context = random.choice(i[1])
                for j in range(1, len(context)):
                    if fromNodeContext:
                        fromNodeContext = fromNodeContext + " " + context[j]
                    else:
                        fromNodeContext = context[j]
            if i[0] == "M" + str(toNode):
                print("prefContexts", i[1])
                context = random.choice(i[1])
                for j in range(1, len(context)):
                    if toNodeContext:
                        toNodeContext = toNodeContext + " " + context[j]
                    else:
                        toNodeContext = context[j]
        file.write("(spec_facilitates\n")
        file.write(self.space * 3 + "(label facilitates" + str(label) + ")\n")
        file.write(self.space * 3 + "(from " + "M" + str(fromNode) + " " + fromNodeContext + ")\n")
        file.write(self.space * 3 + "(to " + "M" + str(toNode) + " " + toNodeContext+")\n")
        file.write(self.space * 3 + "(quality_power " + str(random.randint(1,10) / 10) + " 1.0)\n")
        file.write(self.space * 3 + "(duration_power " + str(random.randint(1, 10) / 10) + " 1.0)\n")
        file.write(self.space * 3 + "(pref_power " +
                   str(random.randint(1, 10) / 10) + " 1.0)\n)\n\n")

    def writeHinders(self, file, i):
        fromNode = 0
        toNode = 0
        while (fromNode == toNode):
            fromNode = random.randint(1, self.numMethods)
            toNode = random.randint(1, self.numMethods)
        file.write("(spec_hinders\n")
        file.write(self.space * 3 + "(label hinders" + str(i) + ")\n")
        file.write(self.space * 3 + "(from " + "M" + str(fromNode) + ")\n")
        file.write(self.space * 3 + "(to " + "M" + str(toNode) + ")\n")
        file.write(self.space * 3 + "(quality_power " + str(random.randint(1, 10) / 10) + " 1.0)\n")
        file.write(self.space * 3 + "(duration_power " + str(random.randint(1, 10) / 10) + " 1.0)\n")
        file.write(self.space * 3 + "(pref_power " + str(random.randint(1, 10) / 10) + " 1.0)\n)\n\n")

    def writeSynchronization(self, file, i):
        fromNode = 0
        toNode = 0
        while (fromNode == toNode):
            fromNode = random.randint(1, self.numMethods)
            toNode = random.randint(1, self.numMethods)
        file.write("(spec_synchronization\n")
        file.write(self.space * 3 + "(label synchronization" + str(i) + ")\n")
        file.write(self.space * 3 + "(from " + "M" + str(fromNode) + ")\n")
        file.write(self.space * 3 + "(to " + "M" + str(toNode) + ")\n")
        file.write(self.space * 3 + "(quality_power " + str(random.randint(1, 11) / 10) + " 1.0)\n")
        file.write(self.space * 3 + "(duration_power " + str(random.randint(1, 11) / 10) + " 1.0)\n")
        file.write(self.space * 3 + "(pref_power " + str(random.randint(1, 11) / 10) + " 1.0)\n)\n\n")

    def writeConstraints(self, file, i):
        """(spec_constraints
  ;; (label constraint1)
  ;; (from M4)
  ;; (to M3)
  ;; (weight 20)
;;)"""
        weight = random.randint(self.weights[0], self.weights[1])
        fromNode = 0
        toNode = 0
        while (fromNode == toNode):
            fromNode = random.randint(1, self.numMethods)
            toNode = random.randint(1, self.numMethods)
        file.write("(spec_constraints\n")
        file.write(self.space * 3 + "(label constraint" + str(i) + ")\n")
        file.write(self.space * 3 + "(from " + "M" + str(fromNode) + ")\n")
        file.write(self.space * 3 + "(to " + "M" +  str(toNode) + ")\n")
        file.write(self.space * 3 + "(weight " + str(weight) + ")\n)\n\n")

    def writeEnvironmentalVariables(self, file):
        """(spec_envi
    (label location)
    (agent Agent1)
    (parents none)
    (preferences 8 home 3 work)
    (distribution home 0.6 work 0.4)
    (frequency home 3 work 1)
)"""
        numContexts = random.randint(self.contexts[0], self.contexts[1])
        dist = self.divideNum(10, numContexts)
        freq = random.randint(self.frequency[0], self.frequency[1])
        if freq < numContexts: freq = numContexts
        freqList =  self.divideNum(freq, numContexts)
        freqStr = ""
        for i in range(len(dist)):
            dist[i] = dist[i] / 10
        distStr = ""
        for i in range(numContexts):
            self.contextList.append("C" + str(self.contextCounter) + str(i))
            self.contextList.sort()
            if not distStr:
                distStr = "C" + str(self.contextCounter) + str(i) + " " + str(dist.pop(0))
            else:
                distStr =  distStr + " " + "C" + str(self.contextCounter)+ str(i) + " " + str(dist.pop(0))
            if not freqStr:
                freqStr = "C" + str(self.contextCounter) + str(i) + " " + str(freqList.pop(0))
            else:
                freqStr = freqStr + " " + "C" + str(self.contextCounter) + str(i) + " " + str(freqList.pop(0))


        file.write("(spec_envi\n")
        file.write(self.space * 4 + "(label " + "C" + str(self.contextCounter) + ")\n")
        file.write(self.space * 4 + "(agent Agent1)\n")
        file.write(self.space * 4 + "(parents none)\n")
        file.write(self.space * 4 + "(preferences 8 home 3 work)\n")
        file.write(self.space * 4 + "(distribution " + distStr + ")\n")
        file.write(self.space * 4 + "(frequency " + freqStr + ")\n)\n\n")
        self.contextCounter += 1



    def generatePreferences(self, methodName):
        if len(self.contextList) == 1:
            pref = random.randint(self.preferences[0], self.preferences[1])
            #print("in generate preds", [(str(pref), self.contextList[0])])
            self.methodPrefContexts.append((methodName, [(pref, self.contextList[0])]))
            return str(pref) + " " + self.contextList[0]
        else:
            numVariables = random.randint(1, len(self.contextList))
        variables = random.sample(self.contextList, numVariables)
        variables.sort()

        prefList = []
        prefString = ""
        for i in range(len(variables)):
            name1 = variables[i]
            for j in range(i+1, len(variables)):
                name2 = variables[j]
                if name1[1] != name2[1]:
                    pref = random.randint(self.preferences[0], self.preferences[1])
                    if prefString:
                        #print("name1 and name2", name1, name2)
                        prefList.append((pref, name1, name2))
                        #prefString = prefString + " " + str(pref) + " " + name1 + " " + name2
                    else:
                        prefList.append((pref, name1, name2))
                        #prefString = str(pref) + " " + name1 + " " + name2

        if not prefList:
            pref = random.randint(self.preferences[0], self.preferences[1])
            prefString = str(pref) + " " + random.choice(self.contextList)
            #print("here ", (methodName, [(str(pref), random.choice(self.contextList))]))
            self.methodPrefContexts.append((methodName, [(pref, random.choice(self.contextList))]))
        else:
            prefList.sort(reverse=True)
            for i in prefList:
                if prefString:
                    prefString = prefString + " " + str(i[0]) + " " + i[1] + " " + i[2]
                else:
                    prefString = str(i[0]) + " " + i[1] + " " + i[2]
            self.methodPrefContexts.append((methodName, prefList))
        prefString.strip()
        return prefString


    def agentListForMethods(self, numMethods):
        returnList = []
        if self.numAgents == 1:
            returnList = [1] * numMethods
            return returnList
        decompose = self.divideNum(numMethods,self.numAgents)
        agent = 1
        for i in decompose:
            for j in range(i):
                returnList.append(agent)
            agent += 1
        return returnList

    def generateQualityDistribution(self, n):
        nums = [0] + [random.randint(1, n - 1) for _ in range(2)] + [n]
        probs = self.divideNum(100, 4)
        for i in range(len(probs)):
            probs[i] = round(probs[i] / 100, 2)
        result = ''
        for i in range(len(probs)):
            result += f'{nums[i]} {probs[i]} '
        return result.strip()

    def generateDurationDistribution(self, n):
        if n-1 > 1:
            nums = [random.randint(1, n - 1) for _ in range(3)] + [n]
        else:
            nums = [1,1,1] + [n]
        probs = self.divideNum(100, 4)
        for i in range(len(probs)):
            probs[i] = round(probs[i] / 100, 2)
        result = ''
        for i in range(len(probs)):
            result += f'{nums[i]} {probs[i]} '
        return result.strip()
    def durDecomposition(self, num, n):
        #print("in durdecomposition", num, n)
        if n == 1:
            return [num]
        # Generate n-1 random numbers between 1 and num - 1
        randList = sorted(random.sample(range(1, num), n - 1))

        # Calculate the differences between adjacent random numbers
        diffs = [randList[0]] + [randList[i] - randList[i - 1] for i in range(1, n - 1)] + [num - randList[n - 2]]

        # Ensure all integers are positive
        result = [max(1, x) for x in diffs]
        return result
    def randomDecomposition(self, i, n):
        #print("i and n", i, n)
        pieces = []
        if i == n:
            for j in range(n):
                pieces.append(1)
        else:
            for idx in range(n - 1):
                    pieces.append(random.randint(1, i - sum(pieces) - n + idx))
            pieces.append(i - sum(pieces))
        pieces.sort()
        return pieces

    def randomNormalDivision(self, num1, num2):
        if num1 == num2:
            result =[]
            for j in range(num2):
                result.append(1)
            return result
        # Generate n-1 random numbers using a normal distribution
        randomNumbers = np.random.normal(loc=0, scale=1, size=num2 - 1)
        randomNumbers = randomNumbers - np.mean(randomNumbers)
        randomNumbers = randomNumbers / np.std(randomNumbers)

        # Sort the random numbers
        randomNumbers.sort()

        # Calculate the differences between adjacent random numbers
        diffs = np.diff(np.concatenate(([0], randomNumbers, [0])))

        # Scale the differences to sum up to num1
        scaledDiffs = diffs / np.sum(diffs) * num1

        # Calculate the final list of positive integers
        result = np.cumsum(scaledDiffs)[:-1]
        result = np.rint(result).astype(int)
        result = [max(1, x) for x in result]

        return list(result)


    def divideRange(self, start, stop, n):

        size = stop - start
        subRangeSizes = [size // n] * n
        remainder = size % n

        # Distribute the remainder over the sub-ranges randomly
        for i in random.sample(range(n), remainder):
            subRangeSizes[i] += 1

        # Create the sub-ranges
        subRanges = []
        for size in subRangeSizes:
            subRanges.append((start, start + size))
            start += size

        return subRanges

    def overlapRange(start, stop, n, overlap):
        """
        Divide a range of integers randomly into n overlapping sub-ranges.
        Returns a list of n tuples, where each tuple contains the start and stop
        values of a sub-range.
        """
        subRanges = []
        size = stop - start
        subRangeSize = size // n

        # Divide the range into n sub-ranges
        for i in range(n):
            subRangeStart = start + i * subRangeSize
            subRangeStop = subRangeStart + subRangeSize
            subRanges.append((subRangeStart, subRangeStop))

        # Overlap adjacent sub-ranges
        for i in range(1, n):
            subRangeStart, subRangeStop = subRanges[i]
            prevSubRangeStart, prevSubRangeStop = subRanges[i - 1]
            overlapSize = subRangeStart - prevSubRangeStop
            overlapStart = subRangeStart - overlapSize * overlap
            overlapStop = prevSubRangeStop + overlapSize * (1 + overlap)
            subRanges[i] = (overlapStart, subRangeStop)
            subRanges[i - 1] = (prevSubRangeStart, overlapStop)

        return subRanges

    def divideNum(self, number, n):
        decompose = []
        if number == n:
            for j in range(n):
                decompose.append(1)
            if (len(decompose) != n):
                print("something is wrong")
            return decompose
        elif n == 1:
            return [number]
        sample = random.sample(range(1,number), n)
        #print("sample: ", sample)
        sample.sort()

        for i in range(len(sample)):
            if i == 0:
                decompose.append(sample[i])
            elif i == len(sample) - 1:
                decompose.append(number - sample[i-1])
            else:
                decompose.append(sample[i] - sample[i-1])
        decompose.sort()
        #print(sum(decompose))
        return decompose
        """
            Divide a number (in thousands) into n parts, where each part is rounded to
            the nearest thousand. Returns a list of n integers.
            """

    def getNormal(self, mean, sd, low, upp):
        return truncnorm(
            (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)




obj = RandomTaskStructures()
obj.readFile()
obj.createRTS()
#print(obj.divideQnum(8000, 7))
#print(obj.divideNum(20, 1))
#obj.writeTasks()
#obj.randomDecomposition(100,4)




