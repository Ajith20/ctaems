from stn import STN
from time import time



class FileReader:


    def __init__(self, successorEdges=True, predecessorEdges=True):
        self.predecessorEdges = predecessorEdges
        self.successorEdges = successorEdges

    def read_file(self, file_path):
        file = open(file_path, "r")
        state = ""
        for line in file:
            if "network" in line.lower():
                state = "NETWORK_TYPE"
                continue
            if state == "NETWORK_TYPE":
                if "u" not in line.lower():
                    return self.readStn(file)
                else:
                    raise Exception("Invalid Network Type")

    def readStn(self, file):
        network = STN(successorEdges=self.successorEdges,
                      predecessorEdges=self.predecessorEdges)
        state = ""
        for line in file:
            if line.startswith('#'):
                if "points" in line.lower() and "num" in line.lower():
                    state = "NO_POINTS"
                elif "edges" in line.lower() and "num" in line.lower():
                    state = "NO_EDGES"
                elif "links" in line.lower():
                    state = "NO_LINKS"
                elif "names" in line.lower():
                    state = "NAMES"
                elif "edges" in line.lower():
                    state = "EDGES"
                    edgeCounter = 0
                elif "links" in line.lower():
                    state = "LINKS"
                else:
                    pass
            else:
                if state == 'NO_POINTS':
                    numTps = int(line)
                    network.n = numTps
                    if self.successorEdges:
                        network.successorEdges = [
                            {} for i in range(numTps)]
                    if self.predecessorEdges:
                        network.predecessorEdges = [
                            {} for i in range(numTps)]
                    network.namesList = ["0" for i in range(numTps)]
                elif state == 'NO_EDGES':
                    numEdges = int(line)
                elif state == 'NO_LINKS':
                    # for testing, throw an error
                    raise Exception(
                        "Simple Temporal Networks do not have contingent links.")
                elif state == 'NAMES':
                    listOfNodes = line.split()
                    if len(listOfNodes) != numTps:
                        raise Exception(
                            "Number of names does not match the number of nodes provided")
                    for idx, nodeName in enumerate(listOfNodes):
                        network.namesDict[nodeName] = idx
                        network.namesList[idx] = nodeName
                elif state == 'EDGES':
                    if numEdges == 0:
                        state = ''
                        continue
                    weights = line.split()
                    edgeCounter += 1
                    # make a list of list of tuples
                    idxNode = network.namesDict[weights[0]]
                    idxSuccessor = network.namesDict[weights[2]]
                    if self.successorEdges:
                        network.successorEdges[idxNode][idxSuccessor] = int(
                            weights[1])
                    if self.predecessorEdges:
                        network.predecessorEdges[idxSuccessor][idxNode] = int(
                            weights[1])
                elif state == 'LINKS':
                    raise Exception(
                        "Simple Temporal Networks do not have contingent links.")
        if numEdges != edgeCounter:
            raise Exception(
                "Number of edges does not match the number given above")
        return network


