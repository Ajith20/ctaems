

class DPC:

    def checkConsistency(self, network) -> bool:
        numTimePoints = network.numTimePoints()
        for k in range(numTimePoints -1, -1, -1):
            #print("k: ", k)
            for i in range(0, k):
                for j in range(0, k):
                    #print("i and j: ", i, j)

                    if k in network.successorEdges[i] and k in network.predecessorEdges[j] and i != j:
                        if j in network.successorEdges[i]:
                            network.successorEdges[i][j] = min(network.successorEdges[i][j], network.successorEdges[i][k]
                                                               + network.successorEdges[k][j])
                        else:
                            #print("iN else", i, j)
                            network.successorEdges[i][j] = network.successorEdges[i][k] + network.successorEdges[k][j]
                        if i in network.predecessorEdges[j] and network.successorEdges[i][j] + network.predecessorEdges[j][i] < 0:
                            return False
        #network.visualize()
        return True




