import matplotlib.pyplot as plt
import networkx as nx



class STN:
    """
    -------------------------------------------------
    A class to represent a Simple Temporal Network
    -------------------------------------------------
    Attributes
    ----------
    names_dict : dict[name: index]
        A dictionary that maps the name of the node to its index
    namesList : List[Int]
        A list that maps the numerical index of a node to its name
    successorEdges : List[Dict[index:name]]
        A list of dicts of successor edges. The list at index i of this attribute is
        the list of edges of the i-th node. Each edge is represented by a tuple - the
        first element of the tuple is the j-th node that the i-th node is connected to
        and the second element is the weight/distance between the i-th and j-th nodes
    n : int
        Number of nodes in the STN
    distance_matrix : List[List[int]]
        (optional) if used, holds the NxN all-pairs, shortest-paths (APSP) matrix
        for the STN.
    dist_up_to_date : boolean  
        True, if the distance_matrix is up-to-date; False, otherwise.
    ---------------------------------------------------
    """

    def __init__(self, successorEdges=True, predecessorEdges=True):
        """
        -----------------------------------------
        Constructor for a Simple Temporal Network
        -----------------------------------------
        Attributes
        ----------
        names_dict : dict[name: index]
            A dictionary that maps the name of the node to its index
        namesList : List[Int]
            A list that maps the numerical index of a node to its name
        successorEdges : List[Dictionary]
            A list of lists of successor edges. The list at index i of this attribute is
            the list of edges of the i-th node. Each edge is represented by a tuple - the
            first element of the tuple is the j-th node that the i-th node is connected to
            and the second element is the weight/distance between the i-th and j-th nodes
        n : int
            Number of nodes in the STN
        distance_matrix : List[List[int]]
            (optional) if used, holds the NxN all-pairs, shortest-paths (APSP) matrix
            for the STN.
        dist_up_to_date : boolean  
            True, if the distance_matrix is up-to-date; False, otherwise.
        -------
        Returns
        -------
        None
        -----------------------------------------
        """

        self.namesDict = {}
        self.namesList = []
        self.successorEdges = [] if successorEdges else None
        self.predecessorEdges = [] if predecessorEdges else None
        self.n = 0
        self.distanceMatrix = []
        self.distUpToDate = False

    def __str__(self):
        """
        ----------------------------------------
        Print method for an STN object
        ----------------------------------------
        OutimePointut:  String representation of the STN
        ----------------------------------------
        """
        stringy = "STN:\n"
        stringy += f"Number of nodes in network: {self.n}\n"
        stringy += f"Dictionary of names -> index: {self.namesDict}\n"
        if self.successorEdges:
            stringy += f"Successor edges of each node: {self.successorEdges}\n"
        if self.predecessorEdges:
            stringy += f"Predecessor edges of each node: {self.predecessorEdges}\n"
        # Display the distance_matrix if it is being used
        if self.distanceMatrix:
            stringy += f"Distance matrix: {self.distanceMatrix}\n"
            if self.distUpToDate:
                stringy += "Distance matrix might not be up to date"
        return stringy

    def numTimePoints(self):
     
        return self.n

    def insertNewEdge(self, timePoint1, timePoint2, weight):
        """
        ----------------------------------------------
        Inserts a new edge into the STN object
        ----------------------------------------------
        Parameters:
        -----------
        timePoint1, either a numerical index or the name of a time-point
        timePoint2, ditto 
        weight, the numerical weight of the edge to be added
        -----------
        Returns:  none
        -----------
        Side Effect:
        -----------
        Inserts the edge, timePoint1 ----[weight]----> timePoint2 into the STN
        ---------------------------------------------------------
        """
        timePoint1Idx = self.namesDict[timePoint1] if type(timePoint1) == str else timePoint1
        timePoint2Idx = self.namesDict[timePoint2] if type(timePoint2) == str else timePoint2

        if self.successorEdges is not None:
            self.successorEdges[timePoint1Idx][timePoint2Idx] = int(weight)
        if self.predecessorEdges is not None:
            self.predecessorEdges[timePoint2Idx][timePoint1Idx] = int(weight)
        self.distUpToDate = False

    def deleteEdge(self, timePoint1, timePoint2):
        """
        ----------------------------------------------
        Deletes an edge from the STN object
        ----------------------------------------------
        Parameters:
        -----------
        timePoint1, either a numerical index or the name of a time-point
        timePoint2, ditto 
        weight, the numerical weight of the edge to be added
        -----------
        Returns:  none
        -----------
        Side Effect:
        -----------
        Deletes the edge, timePoint1 ----[weight]----> timePoint2 from the STN
        ---------------------------------------------------------
        """
        timePoint1Idx = self.namesDict[timePoint1] if type(timePoint1) == str else timePoint1
        timePoint2Idx = self.namesDict[timePoint2] if type(timePoint2) == str else timePoint2

        if self.successorEdges and timePoint2Idx in self.successorEdges[timePoint1Idx]:
            del self.successorEdges[timePoint1Idx][timePoint2Idx]
        if self.predecessorEdges and timePoint1Idx in self.successorEdges[timePoint2Idx]:
            del self.predecessorEdges[timePoint2Idx][timePoint1Idx]

        self.distUpToDate = False

    def insertNewTimePoint(self, timePoint):
        """
        ----------------------------------------------
        Inserts a new time-point into the STN object
        ----------------------------------------------
        Parameters:
        -----------
        timePoint, the name of a time-point
        -----------
        Returns:  none
        -----------
        Side Effect:
        -----------
        Inserts the time-point, timePoint into the STN
        ---------------------------------------------------------
        """
        self.namesDict[timePoint] = self.n
        self.namesList.append(timePoint)
        self.n += 1
        # if self.successorEdges:
        #     self.successorEdges.append({})
        # if self.predecessorEdges:
        #     self.predecessorEdges.append({})
        self.distanceMatrix = []
        self.distUpToDate = False

    
    def deleteTimePoint(self, timePoint):
        if type(timePoint) == str:
            timePointIdx = self.namesDict[timePoint]
        else:
            timePointIdx = timePoint

        tempNamesDict = {}
        tempNamesList = []
        if self.successorEdges:
            tempSuccessorEdges = [{}
                                    for i in range(len(self.successorEdges))]
        if self.predecessorEdges:
            tempPredecessorEdges = [{}
                                      for i in range(len(self.predecessorEdges))]

        for i in range(self.n):
            if i == timePointIdx:
                continue
            nodeName = self.namesList[i]
            tempNamesDict[nodeName] = i
            tempNamesList.append(nodeName)
            for j, weight in self.successorEdges[i].items():
                if j == timePointIdx:
                    continue
                if self.successorEdges:
                    tempSuccessorEdges[i][j] = weight
                if self.predecessorEdges:
                    tempPredecessorEdges[j][i] = weight

        self.namesList = tempNamesList
        self.namesDict = tempNamesDict
        if self.successorEdges:
            self.successorEdges = tempSuccessorEdges
        if self.predecessorEdges:
            self.predecessorEdges = tempPredecessorEdges
        self.n -= 1

    #Check consistency algorithms here 


    def checkSolution(self, distances):
        """
        -----------------------------------------------------------------
        Method to check whether a solution works on the target STN or not
        ------------------------------------------------------------------
        Input:
            stn, the target STN
            distances, a list of integers representing the time at which each index is executed

        OutimePointut:
            works, a boolean that is true if the solution is valid and false otherwise.

        """
        for nodeIdx, distance in enumerate(distances):
            for successorIdx, edgeWeight in self.successorEdges[nodeIdx].items():
                if distances[successorIdx] - distance > edgeWeight:
                    return False
        return True

    def populatePredecessorEdges(self):
        if self.predecessorEdges is not None:
            return
        self.predecessorEdges = [{} for _ in range(self.n)]
        for nodeIdx, edgeDict in enumerate(self.successorEdges):
            for successorIdx, weight in edgeDict.items():
                self.predecessorEdges[successorIdx][nodeIdx] = weight

    def visualize(self):
        G = nx.DiGraph()
        
        G.add_nodes_from(self.namesList)
        blueEdges, redEdges = [], []
        blueLabels, redLabels = {}, {}
        for nodeIdx, edgeDict in enumerate(self.successorEdges):
            for successor_idx, weight in edgeDict.items():
                if (self.namesList[successor_idx], self.namesList[nodeIdx]) in blueEdges:
                    redEdges.append((self.namesList[nodeIdx], self.namesList[successor_idx]))
                    redLabels[(self.namesList[nodeIdx], self.namesList[successor_idx])] = weight
                else:
                    blueEdges.append((self.namesList[nodeIdx], self.namesList[successor_idx]))
                    blueLabels[(self.namesList[nodeIdx],self.namesList[successor_idx])] = weight
                G.add_edge(self.namesList[nodeIdx], self.namesList[successor_idx], weight=weight)
    
        pos = nx.shell_layout(G)
        nx.draw_networkx_nodes(G, pos, node_size=700)
        nx.draw_networkx_edges(
            G, pos, edgelist=blueEdges, arrowstyle="->", connectionstyle='arc3, rad = 0.1', arrowsize=20, width=3, edge_color='b', alpha=1)
        nx.draw_networkx_edges(
            G, pos, edgelist=redEdges, arrowstyle="->", connectionstyle='arc3, rad = 0.1', arrowsize=20, width=3, edge_color='r', alpha=1)
        labels = nx.get_edge_attributes(G, 'weight')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=blueLabels, label_pos=0.3, font_color="b")
        nx.draw_networkx_edge_labels(G, pos, edge_labels=redLabels, label_pos=0.3, font_color="r")
        nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')

        plt.axis('off')
        plt.show()


