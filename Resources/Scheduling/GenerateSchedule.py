from AgentLocalView import *
from Method import *
from Constraint import *
from Task import *
from EnvironmentVariable import *
from stn import *
from Vertex import *
from MCS import *
from file_reader import FileReader
from DPC import *
from PPC import *
class GenerateSchedule(object):

    def __init__(self, agentLocalView, smartAgent, root):
        self.root = root
        self.agentLocalView = agentLocalView
        self.taskStructureNames = self.agentLocalView.taskStructureNames
        self.taskStructureObjects = self.agentLocalView.taskStructureObjects
        self.environmentVariables = self.agentLocalView.environmentVariables
        self.agentEnvironment = self.agentLocalView.agentEnvironment
        self.destinationRelationshipTable = self.agentLocalView.destinationRelationshipTable
        self.sourceRelationshipTable = self.agentLocalView.sourceRelationshipTable
        self.constraints = self.agentLocalView.constraints
        self.smartAgent = smartAgent
        self.schedulePrefValue = 0

    def generateInitialPrefSchedule(self, type: str):
        methodList = []
        taskTreeParse = {}
        for node in reversed(self.taskStructureObjects.values()):

            if node.type == "Method":
                print("In generate initial pref schedule")
                #methodList.append(node.feasibleMaxPreferenceContext(self.agentEnvironment, self.smartAgent))
                context = node.feasiblePreferenceContext(self.agentEnvironment, self.smartAgent, type)
                taskTreeParse[node.methodName] = ["Method", node.getMaxPreferenceValue(),
                                                   context]
                #print("method name and list", node.methodName, node.getMaxPreferenceValue())
        for node in reversed(self.taskStructureObjects.values()):
            if node.type == "Task" and node.qaf == "AND":
                #print("Task name: ", node.taskName)
                prefValue = 0
                subTasks = []
                for i in node.children:
                    taskObject = self.taskStructureObjects[i]
                    if taskObject.type == "Method":
                        value = taskTreeParse[taskObject.methodName]
                        prefValue += value[1]
                        subTasks.append(taskObject.methodName)
                    else:
                        #print("Prefvalue for children", taskObject.taskName, taskTreeParse[taskObject.taskName][1])
                        value = taskTreeParse[taskObject.taskName]
                        prefValue += value[1]
                        subTasks.append(taskObject.taskName)
                taskTreeParse[node.taskName] = ["Task", prefValue, subTasks]
            elif node.type == "Task" and node.qaf == "OR":
                prefValue = 0
                subTasks = []
                if type == "OPTIMAL":
                    for i in node.children:
                        #print("Task name: ", node.taskName)
                        taskObject = self.taskStructureObjects[i]
                        if taskObject.type == "Method":

                                if taskTreeParse[taskObject.methodName][1] > prefValue:
                                    prefValue = taskTreeParse[taskObject.methodName][1]
                                    if subTasks:
                                        subTasks[0] = taskObject.methodName
                                    else:
                                        subTasks.append(taskObject.methodName)

                        else:
                            if taskTreeParse[taskObject.taskName][1] > prefValue:
                                prefValue = taskTreeParse[taskObject.taskName][1]
                                if subTasks:
                                    subTasks[0] = taskObject.taskName
                                else:
                                    subTasks.append(taskObject.taskName)
                #elif type == "RANDOM":
                elif type == "MAX" or "RANDOM":
                    i = random.choice(node.children)
                    taskObject = self.taskStructureObjects[i]
                    if taskObject.type == "Method":
                        prefValue = taskTreeParse[taskObject.methodName][1]
                        if subTasks:
                            subTasks[0] = taskObject.methodName
                        else:
                            subTasks.append(taskObject.methodName)
                    else:
                        prefValue = taskTreeParse[taskObject.taskName][1]
                        if subTasks:
                            subTasks[0] = taskObject.taskName
                        else:
                            subTasks.append(taskObject.taskName)
                taskTreeParse[node.taskName] = ["Task", prefValue, subTasks]


        key = list(taskTreeParse.keys())[-1]
        schedulePref = [0]
        self.recurseTree(key, methodList, taskTreeParse, schedulePref)
        #print("method list: ", methodList)
        return methodList



    def recurseTree(self,key, methodList, taskTreeParse, pref):
        value = taskTreeParse[key]
        if key == self.root:
            self.schedulePrefValue = value[1]
            print("schedulePrefValue :", self.schedulePrefValue)
        #print("key and value: ", key, value)

        if value[0] == "Method":
            methodList.append(value[2])
            return
        else:
            for i in value[2]:
                #pref[0] += i[1]
                #print("i value: ", i)
                self.recurseTree(i, methodList, taskTreeParse, pref)


    def buildSTN(self, type : str, name : str):
        methodList = self.generateInitialPrefSchedule(type)
        type = "STN"
        numberTimePoints = 0
        numberEdges = 0
        edges = []
        timePointNames = ["Z"]
        for i in methodList:
            start = i[0] + "ST"
            end = i[0] + "ET"
            timePointNames.append(start)
            timePointNames.append(end)
            edges.append(["Z", i[3], start])
            edges.append(["Z", i[4], end])
            edges.append([start, i[2], end])
            edges.append([end, -i[1], start])
        for i in self.constraints:
            source = i.source + "ET"
            destination = i.destination + "ST"
            edges.append([source, i.weight, destination])
        #print("timepointNames: ", timePointNames)
        numberTimePoints = len(timePointNames)
        numberEdges = len(edges)
        with open("/Users/ajithvemuri/git/ctaems/Resources/Scheduling/RandomTaskStructures/"+
                  name + ".stn", "w") as file:
            file.write("# KIND OF NETWORK\n")
            file.write(type+"\n")
            file.write("# Num Time-Points\n")
            file.write(str(numberTimePoints)+"\n")
            file.write("# Num Ordinary Edges\n")
            file.write(str(numberEdges)+"\n")
            file.write("# Time-Point Names\n")
            file.writelines(i + " " for i in timePointNames)
            file.write("\n")
            file.write("# Ordinary Edges\n")
            for i in edges:
                file.write(i[0] + " " + str(i[1]) + " " + i[2] + "\n")
        f = FileReader()
        stnNetwork = f.read_file("/Users/ajithvemuri/git/ctaems/Resources/Scheduling/RandomTaskStructures/"+
                  name + ".stn")
        #stnNetwork.visualize()
        self.triangulateSTN(stnNetwork)


    def triangulateSTN(self, stnNetwork):
        mcs = MCS()
        fillEdgesAndOrder = mcs.triangulate(self.buildVertices(stnNetwork))
        fillEdges = fillEdgesAndOrder[0]
        for i in fillEdges:
            a = stnNetwork.namesDict[i[0]]
            b = stnNetwork.namesDict[i[1]]
            if b not in stnNetwork.successorEdges[a] and a not in stnNetwork.successorEdges[b]:
                for i in stnNetwork.successorEdges[a]:
                    if b in stnNetwork.successorEdges[i]:
                        stnNetwork.successorEdges[a][b] = float('inf')
                        stnNetwork.predecessorEdges[b][a] = float('inf')
                        break
                    else:
                        stnNetwork.successorEdges[b][a] = float('inf')
                        stnNetwork.predecessorEdges[a][b] = float('inf')
        ordering = fillEdgesAndOrder[1]
        self.changeSTNOrdering(ordering, stnNetwork)
        if (self.findConsistency(stnNetwork)):
            with open("output.txt", "a") as file:
                file.write(str(self.schedulePrefValue)+"\n")
        else:
            with open("output.txt", "a") as file:
                file.write(str(0)+"\n")

        #print(stnNetwork.successorEdges)



    def changeSTNOrdering(selfself, ordering, stnNetwork):
        nameOrder = []
        indexOrder = []
        oldNewOrderingMap = {}
        namesDictCopy = copy.deepcopy(stnNetwork.namesDict)
        succCopy = copy.deepcopy(stnNetwork.successorEdges)
        preCopy = copy.deepcopy(stnNetwork.predecessorEdges)
        for idx, vertex in enumerate(ordering):
            nameOrder.append(vertex.name)
            indexOrder.append(stnNetwork.namesDict[vertex.name])
            stnNetwork.namesList[idx] = vertex.name
        for i in range(len(stnNetwork.namesList)):
            stnNetwork.namesDict[stnNetwork.namesList[i]] = i
        # Change order of the STN. The order is returned by MCS-M
        for key in namesDictCopy:
            oldNewOrderingMap[namesDictCopy[key]] = stnNetwork.namesDict[key]
        for key in stnNetwork.namesDict:
            i = stnNetwork.namesDict[key]
            stnNetwork.successorEdges[i] = succCopy[namesDictCopy[key]]
            stnNetwork.predecessorEdges[i] = preCopy[namesDictCopy[key]]
            #print("Key, successor and pred", key, stnNetwork.successorEdges[stnNetwork.namesDict[key]],
                  #stnNetwork.predecessorEdges[stnNetwork.namesDict[key]])
            keys = []
            changes = []
            for vertex in list(stnNetwork.successorEdges[i]):
                #print("key and index", key, vertex)
                if vertex != oldNewOrderingMap[vertex]:
                    keys.append(vertex)
                    changes.append([oldNewOrderingMap[vertex], stnNetwork.successorEdges[i][vertex]])
                else:
                    stnNetwork.successorEdges[i][oldNewOrderingMap[vertex]] = stnNetwork.successorEdges[i][vertex]
            #print("changed values", key, stnNetwork.successorEdges[i])
            for j in keys:
                #print("del key and vertex", key, j)
                del stnNetwork.successorEdges[i][j]
            for j in changes:
                stnNetwork.successorEdges[i][j[0]] = j[1]
            keys = []
            changes = []
            for vertex in list(stnNetwork.predecessorEdges[i]):
                if vertex != oldNewOrderingMap[vertex]:
                    keys.append(vertex)
                    changes.append([oldNewOrderingMap[vertex], stnNetwork.predecessorEdges[i][vertex]])
                else:
                    stnNetwork.predecessorEdges[i][oldNewOrderingMap[vertex]] = stnNetwork.predecessorEdges[i][vertex]
            for j in keys:
                del stnNetwork.predecessorEdges[i][j]
            for j in changes:
                stnNetwork.predecessorEdges[i][j[0]] = j[1]

        #print(stnNetwork)
        #print(stnNetwork.namesList)
        #stnNetwork.visualize()

    def findConsistency(self, stnNetwork):
        dpc = DPC()
        return dpc.checkConsistency(stnNetwork)



    def findPPC(self, stnNetwork):
        ppc = PPC()
        ppc.shortestPath(stnNetwork)
    def buildVertices(self, stnNetwork):
        vertices = []
        for i in stnNetwork.namesList:
            vertices.append(Vertex(i))
        for i in vertices:
            index = stnNetwork.namesDict[i.name]
            successorEdges = stnNetwork.successorEdges[index]
            for j in successorEdges:
                succName = stnNetwork.namesList[j]
                for k in vertices:
                    if succName == k.name:
                        i.neighbors.append(k)
            predecessorEdges = stnNetwork.predecessorEdges[index]
            for j in predecessorEdges:
                succName = stnNetwork.namesList[j]
                for k in vertices:
                    if succName == k.name:
                        kPresent = False
                        for l in i.neighbors:
                            if k.name == l.name:
                                kPresent = True
                        if not kPresent:
                            i.neighbors.append(k)
        return vertices







