import matplotlib.pyplot as plt

def read_numbers_2(filename):
    with open(filename, 'r') as file:
        odd_numbers = []
        even_numbers = []
        for i, line in enumerate(file, 1):
            number = int(line.strip())
            if i % 2 != 0:  # Odd-numbered lines
                odd_numbers.append(number)
            else:  # Even-numbered lines
                even_numbers.append(number)
    return odd_numbers, even_numbers

def read_numbers_3(filename):
    with open(filename, 'r') as file:
        optimal = []
        max = []
        random = []
        for i, line in enumerate(file, 1):
            number = int(line.strip())
            if i % 3 == 1:
                optimal.append(number)
            elif i % 3 == 2:  # Even-numbered lines
                max.append(number)
            else:
                random.append(number)
    return optimal, max, random


def plot_numbers(odd_numbers, even_numbers):
    x_odd = list(range(1, len(odd_numbers) + 1))
    x_even = list(range(1, len(even_numbers) + 1))

    plt.plot(x_odd, odd_numbers, '*', label='PrefMax')
    plt.plot(x_even, even_numbers, 'o', label='Random')

    x_tick_values = [x for x in x_odd if x % 25 == 0]  # Filtered x-axis tick values

    plt.xticks(x_tick_values)  # Set x-axis tick values to the filtered list

    plt.xlabel('Schedules')
    plt.ylabel('Preference Values')

    plt.legend()
    plt.show()

def percentage_change_2(odd_numbers, even_numbers):
    change = []
    for i in range(len(odd_numbers)):
        if odd_numbers[i] >= even_numbers[i] and odd_numbers[i] != 0:
            change.append(100 - ((even_numbers[i] / odd_numbers[i]) * 100))
        elif even_numbers[i] > odd_numbers[i] and even_numbers[i] != 0:
            change.append(-(100 - ((odd_numbers[i] / even_numbers[i]) * 100)))
    print(change)
    return sum(change)/(len(change) - change.count(0))

def percentage_change_3(optimal, max, random):
    change_max = []
    change_random = []
    print("optimal", optimal)
    print("random", random)
    for i in range(len(optimal)):
        if optimal[i] >= max[i] and optimal[i] != 0:
            print("here")
            change_max.append(100 - ((max[i] / optimal[i]) * 100))
        elif optimal[i] < max[i] and max[i] != 0:
            change_max.append(-(100 - ((optimal[i] / max[i]) * 100)))
        if optimal[i] >= random[i] and optimal[i] != 0:
            print("here")
            change_random.append(100 - ((random[i] / optimal[i]) * 100))
        elif optimal[i] < random[i] and random[i] != 0:
            change_random.append(-(100 - ((optimal[i] / random[i]) * 100)))
    print("change random", change_random)
    print("change max", change_max)
    return((sum(change_max)/len(change_max), sum(change_random)/len(change_random)))
# Main program
filename = "/Users/ajithvemuri/git/ctaems/Resources/Scheduling/output.txt"
#odd_numbers, even_numbers = read_numbers_2(filename)
#plot_numbers(odd_numbers, even_numbers)
#print(percentage_change_2(odd_numbers, even_numbers))

optimal, max, random = read_numbers_3(filename)

print(percentage_change_3(optimal, max, random))