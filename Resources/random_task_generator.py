import os

name = "rand-"

filename = name+'.ctaems'

if os.path.exists(filename):
    append_write = 'a' # append if already exists
else:
    append_write = 'w' # make a new file if not

file = open(filename,append_write)
file.write("Filename: " + name + '\n')
file.close()