from agent import *
from copy import *




########################################################################
#                             mdp agent                                #
########################################################################
class mdp_agent(agent):
	running_method = None
	tasks = {}
	methods = {}
	relationships = {}
	interesting_times = []
	waiting = False
	finished = False
	tick = 0
	
	transitions = {}
	policy = {}
	
	results = []
	
	def __init__(self, agent_name, sim_addr, port):
		agent.__init__(self, agent_name, sim_addr, port)
		self.tick = 0



#                       Message Handlers                               #
	def confirm_method_start_message(self, message):
		print(message)
		self.running_method = self.requested_method
		self.requested_method = None

	def initial_tree_message(self, message):
		self.find_nodes(message)
		self.generate_transition_table()
		self.generate_policy()
		
	def update_tree_message(self, message):
		print(message)

	def notify_method_completed_message(self, message):
		print(message)
		self.running_method = None
		self.results.append((message['Message']['MethodName'], self.tick-message['Message']['Duration'], float(message['Message']['Duration']), float(message['Message']['Quality'])))
		m = self.policy[str((self.tick,self.results))][1]
		if m == 'FINISHED':
			self.finished = True
		elif m == 'WAIT':
			self.waiting = True	
		else:
			self.run_method(m)

	def notify_method_status_message(self, message):
		print(message)

	def notify_relationship_activation_message(self, message):
		#check schedule and maybe run method
		print(message)

	def start_simulation_message(self, message):
		self.send_initial_commitments()
		#print(self.policy[str((0,[]))])
		m = self.policy[str((2,[]))][1]
				
		print("Simulation started.")

	def end_simulation_message(self, message):
		print("End of Simulation")
		sys.exit(0)

	def next_tick_message(self, message):
		self.tick = self.tick + 1
		if not self.finished and self.tick > 1:
			print(message)
			if str((self.tick , self.results)) in self.policy:
				m = self.policy[str((self.tick, self.results))][1]
				if m != 'WAIT' and m != 'FINISHED':
					self.run_method(m)

	def agent_to_agent_message(self, message):
		print(message)

	def get_applicable_methods(self, state):
		result = []
		for m in self.methods:
			if self.methods[m]['parent'] != 'NON_LOCAL' and self.methods[m]['AgentName'] == self.name:
				completed = False
				print(m)
				for completed_method in state[1]:
					if m == completed_method[0]:
						completed = True
						break
				print(m)		
				if self.tasks[self.methods[m]['parent']]['EarliestStartTime'] <= state[0] and not completed:
					durations = []
					for d in self.methods[m]['Duration'].split('),('):
						durations.append(float(d.split(',')[0].replace('(','')))
					if self.tasks[self.methods[m]['parent']]['Deadline'] < 0 or self.tasks[self.methods[m]['parent']]['Deadline'] > state[0]+max(durations):
						add = True
						
						#HANDLE RELATIONSHIPS HERE#
						for r in self.relationships:
							if r['Destination'] == m:
								if r['RelationshipType'] == 'Disables':
									for completed_method in state[1]:
										if completed_method[0]==r['Source']:
											add = False
											
						#############################
						
											
						if add:
							result.append(m)
		return result




#                     building and solving MDP                         #
	def find_nodes(self, message):	
		for n in message['Message']['Nodes']:
			if n['NodeType'] == 'Task':
				self.tasks[n['NodeName']]=n
			if n['NodeType'] == 'Method':
				self.methods[n['NodeName']]=n	
		self.relationships = message['Message']['Relationships']
		for m in self.methods:
			for t in self.tasks:
				if m in self.tasks[t]['SubTasks']:
					self.methods[m]['parent'] = t
				if 'parent' not in self.methods[m]:
					self.methods[m]['parent'] = 'NON_LOCAL'	
		for t in self.tasks:
			x = self.tasks[t]['EarliestStartTime']
			if x > 0:
				if x not in self.interesting_times:
					self.interesting_times.append(x)
		print(self.interesting_times)					

	def generate_transition_table(self):
		#########################################			
		initial_state = (2,[])
		openlist = [initial_state]
		count = 0
		while openlist:
			current_state = openlist.pop()
			self.transitions[str(current_state)] = {}
			count = count + 1
			applicable_methods = self.get_applicable_methods(current_state)
			for m in applicable_methods:
				self.transitions[str(current_state)][m] = []
				durations = self.methods[m]['Duration'].replace('),(','|').replace('(','').replace(')','').split('|')
				qualities = self.methods[m]['Quality'].replace('),(','|').replace('(','').replace(')','').split('|')
				for d in durations:
					d_split = d.split(',')
					for q in qualities:
						q_split = q.split(',')
						new_state = (current_state[0]+ int(float((d_split[0])))+1, [] )
						for x in current_state[1]:
							new_state[1].append(copy(x))
						new_state[1].append((m, current_state[0]+1, float(d_split[0]), float(q_split[0])))
						if not str(new_state) in self.transitions:
							openlist.append(new_state)
						else:
							print("state not added")	
						self.transitions[str(current_state)][m].append( (float(d_split[1])*float(q_split[1]), new_state) )
			need_wait = False
			for t in self.interesting_times:
				if t > current_state[0]:
					wait_state = (t, current_state[1])
					self.transitions[str(current_state)]['WAIT'] = [ (1.0, wait_state ) ]
					if not str(wait_state) in self.transitions:
						openlist.append(wait_state)
		print("enumerated " + str(count) + " states.")
		
	def generate_policy(self):
		self.generate_policy_rec((2,[]))
		
	def generate_policy_rec(self, state):
		if str(state) in self.transitions:
			curr_util = 0
			#if not self.transitions[str(state)]:
			curr_util = self.calculate_current_utility(state)
			#else:	
			expected_utils = {}
			for m in self.transitions[str(state)]:
				e = 0
				for t in self.transitions[str(state)][m]:
					if not str(t[1]) in self.policy:
						self.generate_policy_rec(t[1])
					e = e + self.policy[str(t[1])][2]*t[0]	
				expected_utils[m] = e
			if expected_utils:
				best = None
				for m in expected_utils:
						if not best or expected_utils[best] < expected_utils[m]:
							best = m
				if curr_util > expected_utils[best]:
					self.policy[str(state)] = (state, 'FINISHED', curr_util)
				else:
					self.policy[str(state)] = (state, best, expected_utils[best])
			else:
				self.policy[str(state)] = (state, 'FINISHED', curr_util)		
		else:
			print("FLAG " + str(state))
					
	def calculate_current_utility(self,state):
		outcomes = {}
		for o in state[1]:
			p = self.methods[o[0]]['parent']
			if p != 'NON_LOCAL':
				if not p in outcomes:
					outcomes[p]=[o]
				else:
					outcomes[p].append(o)
		total_util = 0		
		for t in outcomes:
			task_util = 0
			if self.tasks[t]['QAF']	== 'AND':
				if len(self.tasks[t]['SubTasks']) == len(outcomes[t]):
					for o in outcomes[t]:
						task_util = task_util + o[3]
			total_util = total_util + task_util	
		return total_util	


#                      negotiating commitments                         #
	def send_initial_commitments(self):
		for r in self.relationships:
			print(r)
		
	#def send_final_commitments(self):
			

########################################################################
#                         end mdp agent                                #
########################################################################	

