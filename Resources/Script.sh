#!/bin/bash

# set the path to the folder containing the files
folder_path="/Users/ajithvemuri/git/ctaems/Resources/Scheduling/RandomTaskStructures"

# loop through each file in the folder
for file_path in "$folder_path"/*
do
  # get the file name without the path
  file_name=$(basename "$file_path")

  # run the Java jar file with the current file name as an argument
  java -jar /Users/ajithvemuri/git/ctaems/Resources/ctaems.jar "$file_name"

  # run the Python program
  python3 smartAgent_preferences.py Agent1

  # sleep for a few seconds to allow the Python program to finish
  sleep 5
done
