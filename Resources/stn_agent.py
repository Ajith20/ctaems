from agent import *
from copy import *
from itertools import *




########################################################################
#                             stn agent                                #
########################################################################
class stn_agent(agent):
    tasks = {}
    methods = {}
    my_methods = {}
    relationships = {}
    task_root = None
    distances = {}
    reschedule_times = []
    idle = False
    non_local_gains = {}
    
    schedule = []
    enabled = []
    e_gain = 0
    
    results = {}
    
    stn = {'zero': {}}
    vertices = []
    
    def __init__(self, agent_name, sim_addr, port):
        agent.__init__(self, agent_name, sim_addr, port)
        self.tick = 0
    
    def find_nodes(self, message):
        for n in message['Message']['Nodes']:
            if n['NodeType'] == 'Task':
                self.tasks[n['NodeName']]=n
            elif n['NodeType'] == 'Method':
                self.methods[n['NodeName']]=n
        self.relationships = message['Message']['Relationships']
        for t in self.tasks:
            if t == self.task_root:
                self.tasks[t]['parent'] = 'ROOT'
            else:
                for tt in self.tasks:
                    if t in self.tasks[tt]['SubTasks']:
                        self.tasks[t]['parent'] = tt
                        break            
        for m in self.methods:
            self.methods[m]['enabled'] = True
            self.methods[m]['hypothetically_enabled'] = True
            for t in self.tasks:
                if m in self.tasks[t]['SubTasks']:
                    self.methods[m]['parent'] = t
            if 'parent' not in self.methods[m]:
                self.methods[m]['parent'] = 'NON_LOCAL'
            else:
                self.methods[m]['EarliestStartTime'] = self.tasks[self.methods[m]['parent']]['EarliestStartTime']
                if self.methods[m]['AgentName'] == self.name:
                    self.my_methods[m] = self.methods[m]
            for r in self.relationships:
                if r['Destination'] == m and r['RelationshipType'] == 'Enables':
                    self.methods[m]['enabled'] = False
                    self.methods[m]['hypothetically_enabled'] = False
                
    def generate_stn(self):
        self.vertices.append('zero')
        self.vertices.append(self.task_root+'_s')
        self.vertices.append(self.task_root+'_f')
        self.stn[self.task_root+'_s'] = {}
        self.stn[self.task_root+'_f'] = {}
        self.stn['zero'] = {}
        
        dead = self.tasks[self.task_root]['Deadline']
        if dead < 0:
            dead = 100000
        self.stn['zero'][self.task_root+'_f'] = dead
        start = self.tasks[self.task_root]['EarliestStartTime']
        if start < 0:
            start = 0
        self.stn[self.task_root+'_s']['zero'] = 0 - start
        
        for t in self.tasks:
            if t != self.task_root:
                # Structural links
                if t+'_s' not in self.stn:
                    self.stn[t+'_s'] = {}
                    self.stn[t+'_f'] = {}
                    self.vertices.append(t+'_s')
                    self.vertices.append(t+'_f')
                if self.tasks[t]['parent']+'_s' not in self.stn:
                    self.stn[self.tasks[t]['parent']+'_s'] = {}
                    self.stn[self.tasks[t]['parent']+'_f'] = {}
                    self.vertices.append(self.tasks[t]['parent']+'_s')
                    self.vertices.append(self.tasks[t]['parent']+'_f')
                dead = self.tasks[t]['Deadline']
                p_dead = self.tasks[self.tasks[t]['parent']]['Deadline']
                if dead < 0:
                    dead = 100000
                if p_dead < 0:
                    p_dead = 100000
                if dead - p_dead < 0:
                    #print(str(dead) + ' ' + str(p_dead))
                    self.stn[self.tasks[t]['parent']+'_f'][t+'_f'] = dead - p_dead
                else:
                    self.stn[self.tasks[t]['parent']+'_f'][t+'_f'] = 0
                start = self.tasks[t]['EarliestStartTime']
                if start < 0:
                    start = 0
                p_start = self.tasks[self.tasks[t]['parent']]['EarliestStartTime']
                if p_start < 0:
                    p_start = 0
                self.stn[t+'_s'][self.tasks[t]['parent']+'_s'] = p_start-start
                #print(self.stn['zero'][t+'_f'])
                #print(self.stn[t+'_s']['zero'])
                #print()
        for m in self.my_methods:
            self.stn[m+'_s'] = {}
            self.stn[m+'_f'] = {}
            max_duration = -1
            min_duration = 100000
            for dur in self.methods[m]['Duration'].replace('),(','|').replace('(','').replace(')','').split('|'):
                dur = dur.split(',')
                #print(dur)
                if float(dur[0]) > max_duration:
                    max_duration = int(float(dur[0]))
                if float(dur[0]) < min_duration:
                    min_duration = int(float(dur[0])) 
            self.vertices.append(m+'_s')
            self.vertices.append(m+'_f')
            
            #Structural links
            self.stn[self.methods[m]['parent']+'_f'][m+'_f'] = 0
            start = self.methods[m]['EarliestStartTime']
            if start < 0:
                start = 0
            p_start = self.tasks[self.methods[m]['parent']]['EarliestStartTime']
            if p_start < 0:
                p_start = 0
            self.stn[m+'_s'][self.methods[m]['parent']+'_s'] = p_start-start
            
            # Duration links
            self.stn[m+'_s'][m+'_f'] = max_duration
            self.stn[m+'_f'][m+'_s'] = 0 - min_duration
            
            
            #print(self.methods[m])
        
        # precedence links    
        for r in self.relationships:
            if r['Source'] in self.my_methods and r['Destination'] in self.my_methods:
                self.stn[r['Destination']+'_s'][r['Source']+'_f']=0
            #print(self.stn)

    def bellman_ford(self):
        self.distances = {}
        for v in self.vertices:
            if v == 'zero':
                self.distances[v] = 0
            else:
                self.distances[v] = 100000
        for i in range(len(self.vertices)):
            unchanged = True
            for v in self.vertices:
                #print(self.stn[v])
                for e in self.stn[v]:
                    #print(v + ': ' + str(self.stn[v]))
                    #print ('v = ' + v + ' e = ' + e)
                    if self.distances[v] + self.stn[v][e] < self.distances[e]:
                        #print(str(self.distances[v]) + ' ' + str(self.stn[v][e]))
                        self.distances[e] = self.distances[v] + self.stn[v][e]
                        unchanged = False
            if unchanged:
                break
        for m in self.distances:
            if self.distances[m] not in self.reschedule_times and self.distances[m] != 0 and self.distances[m] != 100000:
                self.reschedule_times.append(self.distances[m])
        #print('reschedule times: ' + str(self.reschedule_times))
                
    def create_schedule(self):
        hypothetical_time = self.tick
        to_be_scheduled = []
        schedules = []
        new_schedule = []
        for m in self.my_methods:
            if hypothetical_time >= self.methods[m]['EarliestStartTime'] and  hypothetical_time <= self.distances[m+'_s']:
                if self.methods[m]['enabled']:
                    to_be_scheduled.append(m)
                else:
                    for r in self.relationships:
                        if r['Destination'] == m:
                            if r['Source'] in self.my_methods:
                                to_be_scheduled.append(m)
                                                            
                                   
        gains = []
        
        current_schedule = []
        
        for i in range(1, len(to_be_scheduled)+1):
            for p in permutations(to_be_scheduled,i):
                current_schedule = []
                for pp in p:
                    current_schedule.append(pp)
                schedules.append(copy(current_schedule))
        schedules_with_gain = []
        for sched in schedules:
            e_sched_gain = 0
            hypothetical_time = 0
            expected_results = {}
            keeping = True
            for m in sched:
                go = False
                if self.methods[m]['enabled']:
                    go = True
                else:
                    for r in self.relationships:
                        if r['Destination'] == m and r['Source'] in expected_results:
                            go = True
                if hypothetical_time >= self.methods[m]['EarliestStartTime'] and  hypothetical_time <= self.distances[m+'_s'] and go:
                    e_q = 0
                    e_dur = 0
                    e_gain = 0
                    for qual in self.methods[m]['Quality'].replace('),(','|').replace('(','').replace(')','').split('|'):
                        qual = qual.split(',')
                        e_q = e_q + float(qual[0])*float(qual[1])
                    for dur in self.methods[m]['Duration'].replace('),(','|').replace('(','').replace(')','').split('|'):
                        dur = dur.split(',')
                        e_dur = e_dur + float(dur[0])*float(dur[1])
                    hypothetical_time = hypothetical_time + e_dur    
                    expected_results[m] = (e_q, e_dur)    
                    p = self.methods[m]['parent']
                    q = self.tasks[p]['QAF']
                    if q == 'AND':
                        max_so_far = 0
                        incomplete = False
                        for mm in self.tasks[self.methods[m]['parent']]['SubTasks']:
                            if mm != m:
                                if mm not in self.results and mm not in expected_results:
                                    incomplete = True
                                    break
                                elif mm in self.results:
                                    if max_so_far < self.results[mm][0]:
                                        max_so_far = self.results[mm][0]
                                else:
                                    if max_so_far < expected_results[mm][0]:
                                        max_so_far = expected_results[mm][0]
                        if incomplete:
                            e_gain = 0
                        else:
                            if max_so_far > e_q:
                                e_gain = max_so_far
                            else:
                                e_gain = e_q
                                        
                    elif q == 'SUM':
                        e_gain = e_q    
                        
                        
                    elif q == 'OR':
                        for m in self.tasks[self.methods[m]['parent']]['SubTasks']:
                            if m in self.results:
                                if self.results[m][1] < e_q:
                                    if e_q - self.results[m][1] > e_gain:
                                        e_gain = e_q - self.results[m][1]
                                else:
                                    e_gain = 0
                    e_sched_gain = e_sched_gain + e_gain    
                else:
                    if self.tasks[self.methods[m]['parent']]['QAF'] == 'AND':
                        keeping = True
                    else:
                        keeping = False
                        break
            if keeping:
                gains.append((sched,e_sched_gain))
        if gains:
            max_gain = gains[0]
        else:
            max_gain = ([],0)    
        for g in gains:
            if g[1] > max_gain[1]:
                max_gain = g
            elif g[1] == max_gain[1] and len(g[0]) >= len(max_gain[0]):
                max_gain = g
        self.schedule = max_gain[0]
        self.e_gain = max_gain[1]
            
            
    def create_hypothetical_schedule(self):
        hypothetical_time = self.tick
        to_be_scheduled = []
        schedules = []
        new_schedule = []
        for m in self.my_methods:
            if hypothetical_time >= self.methods[m]['EarliestStartTime'] and  hypothetical_time <= self.distances[m+'_s']:
                to_be_scheduled.append(m)
        gains = []
        
        current_schedule = []
        
        for i in range(1, len(to_be_scheduled)+1):
            for p in permutations(to_be_scheduled,i):
                current_schedule = []
                for pp in p:
                    current_schedule.append(pp)
                schedules.append(copy(current_schedule))
        schedules_with_gain = []
        for sched in schedules:
            e_sched_gain = 0
            hypothetical_time = 0
            expected_results = {}
            keeping = True
            for m in sched:
                if hypothetical_time >= self.methods[m]['EarliestStartTime'] and  hypothetical_time <= self.distances[m+'_s']:
                    e_q = 0
                    e_dur = 0
                    e_gain = 0
                    for qual in self.methods[m]['Quality'].replace('),(','|').replace('(','').replace(')','').split('|'):
                        qual = qual.split(',')
                        e_q = e_q + float(qual[0])*float(qual[1])
                    for dur in self.methods[m]['Duration'].replace('),(','|').replace('(','').replace(')','').split('|'):
                        dur = dur.split(',')
                        e_dur = e_dur + float(dur[0])*float(dur[1])
                    hypothetical_time = hypothetical_time + e_dur    
                    expected_results[m] = (e_q, e_dur)    
                    p = self.methods[m]['parent']
                    q = self.tasks[p]['QAF']
                    if q == 'AND':
                        max_so_far = 0
                        incomplete = False
                        for mm in self.tasks[self.methods[m]['parent']]['SubTasks']:
                            if mm != m:
                                if mm not in self.results and mm not in expected_results:
                                    incomplete = True
                                    break
                                elif mm in self.results:
                                    if max_so_far < self.results[mm][0]:
                                        max_so_far = self.results[mm][0]
                                else:
                                    if max_so_far < expected_results[mm][0]:
                                        max_so_far = expected_results[mm][0]
                        if incomplete:
                            e_gain = 0
                        else:
                            if max_so_far > e_q:
                                e_gain = max_so_far
                            else:
                                e_gain = e_q
                                        
                    elif q == 'SUM':
                        e_gain = e_q    
                        
                        
                    elif q == 'OR':
                        for m in self.tasks[self.methods[m]['parent']]['SubTasks']:
                            if m in self.results:
                                if self.results[m][1] < e_q:
                                    if e_q - self.results[m][1] > e_gain:
                                        e_gain = e_q - self.results[m][1]
                                else:
                                    e_gain = 0
                    if m in self.non_local_gains:
                        e_gain = e_gain + self.non_local_gains[m]
                    e_sched_gain = e_sched_gain + e_gain    
                else:
                    if self.tasks[self.methods[m]['parent']]['QAF'] == 'AND':
                        keeping = True
                    else:
                        keeping = False
                        break
            if keeping:
                gains.append((sched,e_sched_gain))
        if gains:
            max_gain = gains[0]
        else:
            max_gain = ([],0)    
        for g in gains:
            if g[1] > max_gain[1]:
                max_gain = g
            elif g[1] == max_gain[1] and len(g[0]) >= len(max_gain[0]):
                max_gain = g
        non_local_options = []
        if self.schedule != max_gain[0]:
            for m in max_gain[0]:
                if not self.methods[m]['enabled']:
                    tmp = []
                    tmp.append(m)
                    tmp.append(max_gain[1])
                    non_local_options.append(tmp)
        for nl in non_local_options:
            nl_message = {}
            nl_message['MessageType'] = 'AgentToAgentMessage'
            nl_msg = {}
            nl_msg['MsgSender'] = self.name
            other_agent = None;
            for r in self.relationships:
                if r['Destination'] == nl[0]:
                    other_agent = self.methods[r['Source']]['AgentName']
            if not other_agent:
                continue        
            nl_msg['MsgDest'] = other_agent
            nl_msg['Content'] = {'Option': nl}
            nl_message['Message'] = nl_msg
            self.s.send(bytes(json.dumps(nl_message)+"\n", 'UTF-8'))
            print("Sending message to " + other_agent)
    ########################################################

    def confirm_method_start_message(self, message):
        self.running_method = self.requested_method
        self.requested_method = None

    def initial_tree_message(self, message):
        self.task_root = message['Root']
        self.find_nodes(message)
        self.generate_stn()
        self.bellman_ford()
        print(self.distances)
        self.create_schedule()
        self.create_hypothetical_schedule()
        #print(message)

    def update_tree_message(self, message):
        print(message)

    def notify_method_completed_message(self, message):
        self.running_method = None
        print(message)
        if self.schedule:
            self.run_method(self.schedule.pop())
        else:
            self.idle = True    

    def notify_method_status_message(self, message):
        print(message)

    def notify_relationship_activation_message(self, message):
        print(message)
        if message['Message']['Type'] == 'Enables':
            self.methods[message['Message']['Target']]['enabled'] = True
        elif message['Message']['Type'] == 'Disnables':
            self.methods[message['Message']['Target']]['enabled'] = False
        self.create_schedule()
        if self.idle and self.schedule:
            self.run_method(self.schedule.pop())
            self.idle = False
            
    def start_simulation_message(self, message):
        if self.schedule:
            self.run_method(self.schedule.pop())
        else:
            self.idle = True    
        print(message)
            

    def end_simulation_message(self, message):
        print("End of Simulation")
        sys.exit(0)

    def next_tick_message(self, message):
        self.tick = self.tick + 1
        if self.tick in self.reschedule_times:
            self.create_schedule()
        if self.idle and self.schedule:
            self.run_method(self.schedule.pop())
        print(message)

    def agent_to_agent_message(self, message):
        print(message)
        self.non_local_gains[message['Message']['Content']['Option'][0]] = message['Message']['Content']['Option'][1]
        self.create_schedule()


agent = stn_agent('Agent_Green','localhost',9876)
