####### Ordered Logger #######


Final Duration: 278.0
Final Quality: 0.0
Seed: 7629086251

##### Method Results #####


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7
        End Time: 18


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 12
        End Time: 32


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 6
        End Time: 40


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 20
        End Time: 62


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 65


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 72


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 80


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 4.972727272727273
        With Duration: 0
        End Time: 82


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 90


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 96


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 15
        End Time: 117


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 122


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4
        End Time: 130


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 145


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.5454545454545454
        With Duration: 8
        End Time: 155


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 161


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9
        End Time: 173


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 7
        End Time: 183


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 189


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 192


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 201


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.4117647058823528
        With Duration: 2
        End Time: 210


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 217


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 224


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 228


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9
        End Time: 239


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 249


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 4
        End Time: 255


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 18
        End Time: 278

##### Relationship Results #####


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 386
        Completed: Method3, Method13, Method5, Method8, Method17, Method22, Method12, Method30, Method16, Method14, Method27, Method1, Method24, Method10, Method23, Method11, Method7, Method29, Method20, Method19, Method2, Method15, Method9, Method26, Method28, Method18, Method25, Method4, Method21