####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6644503090

##### Method Results #####


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 7
        End Time: 8


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12
        End Time: 24


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6
        End Time: 32


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 51


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 43.05555555555556
        With Duration: 5
        End Time: 69


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 100


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 117


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 128


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 144


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 154


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7
        End Time: 210


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 3
        End Time: 238


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17
        End Time: 274


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15
        End Time: 296


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.352941176470589
        With Duration: 8
        End Time: 319


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 338


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 3
        End Time: 369


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 409


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1
        End Time: 430


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 5.555555555555555
        With Duration: 8
        End Time: 450


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 36.0
        With Duration: 2
        End Time: 472


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12
        End Time: 524


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 570


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1
        End Time: 593


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 0.7058823529411764
        With Duration: 2
        End Time: 625


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 12
        End Time: 670

##### Relationship Results #####


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 1114
        Received: 19209
        Completed: Method30, Method17, Method8, Method24, Method6, Method21, Method2, Method29, Method5, Method12, Method18, Method13, Method10, Method26, Method16, Method22, Method15, Method19, Method20, Method7, Method23, Method9, Method28, Method25, Method1, Method27, Method11, Method5, Method2, Method9, Method7, Method10, Method26, Method14, Method12, Method16, Method8, Method21, Method24, Method23, Method15, Method6, Method28, Method27, Method20, Method17, Method30, Method25, Method13, Method22, Method1, Method29, Method18, Method20, Method28, Method12, Method21, Method17, Method6, Method10, Method3, Method15, Method8, Method24, Method5, Method2, Method1, Method16, Method13, Method18, Method14, Method27, Method7, Method30, Method23, Method22, Method26, Method9, Method29, Method15, Method5, Method24, Method6, Method30, Method27, Method1, Method7, Method28, Method10, Method23, Method4, Method11, Method20, Method3, Method21, Method9, Method29, Method8, Method13, Method2, Method22, Method25, Method26, Method18, Method12, Method17, Method16, Method19, Method7, Method29, Method12, Method9, Method18, Method22, Method13, Method2, Method21, Method3, Method5, Method15, Method28, Method24, Method27, Method23, Method30, Method26, Method1, Method17, Method16, Method10, Method8, Method6, Method20, Method25, Method16, Method28, Method5, Method23, Method24, Method22, Method30, Method12, Method7, Method15, Method13, Method8, Method27, Method26, Method20, Method17, Method21, Method9, Method29, Method10, Method6, Method1, Method18, Method25, Method2, Method22, Method20, Method6, Method1, Method8, Method5, Method10, Method4, Method15, Method9, Method23, Method28, Method2, Method24, Method7, Method13, Method27, Method18, Method14, Method12, Method16, Method26, Method17, Method21, Method29, Method30, Method19, Method15, Method29, Method8, Method16, Method26, Method9, Method24, Method23, Method27, Method13, Method1, Method22, Method6, Method17, Method7, Method10, Method21, Method20, Method12, Method19, Method18, Method4, Method30, Method25, Method5, Method2, Method28, Method28, Method6, Method10, Method22, Method14, Method24, Method15, Method19, Method16, Method18, Method21, Method30, Method12, Method26, Method13, Method2, Method27, Method5, Method29, Method17, Method10, Method30, Method21, Method27, Method11, Method1, Method15, Method13, Method18, Method5, Method29, Method2, Method23, Method3, Method6, Method12, Method20, Method5, Method1, Method16, Method18, Method20, Method15, Method12, Method29, Method28, Method23, Method22, Method5, Method18, Method13, Method27, Method20, Method30, Method11, Method21, Method15, Method29, Method8, Method1, Method12, Method16, Method6, Method2, Method7, Method26, Method14, Method3, Method2, Method22, Method23, Method13, Method1, Method4, Method19, Method8, Method6, Method27, Method7, Method28, Method20, Method3, Method8, Method11, Method16, Method26, Method20, Method8, Method12, Method10, Method9, Method28, Method22, Method21, Method1, Method7, Method21, Method12, Method18, Method6, Method22, Method3, Method30, Method10, Method29, Method28, Method15, Method23, Method20, Method19, Method4, Method5, Method9, Method16, Method2, Method26, Method24, Method27, Method13, Method25, Method8, Method17, Method29, Method16, Method9, Method7, Method13, Method20, Method6, Method1, Method2, Method17, Method23, Method15, Method28, Method12, Method27, Method26, Method22, Method25, Method30, Method5, Method18, Method4, Method8, Method21, Method24, Method10, Method29, Method2, Method3, Method22, Method28, Method24, Method1, Method12, Method23, Method9, Method21, Method19, Method6, Method5, Method17, Method8, Method30, Method13, Method15, Method26, Method18, Method7, Method16, Method20, Method17, Method8, Method7, Method9, Method12, Method29, Method30, Method6, Method16, Method23, Method21, Method26, Method18, Method22, Method6, Method24, Method7, Method8, Method29, Method23, Method28, Method26, Method5, Method9, Method30, Method27, Method21, Method20, Method16, Method17, Method18, Method1, Method12, Method10, Method2, Method11, Method19, Method15, Method13