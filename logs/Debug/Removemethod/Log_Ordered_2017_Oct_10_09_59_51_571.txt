####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 8952612577

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 3


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 7
        End Time: 12


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 60


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 5
        End Time: 90


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 107


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 132


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 147


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 9
        End Time: 163


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 4
        End Time: 184


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 36.0
        With Duration: 1
        End Time: 203


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 230


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 271


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 282


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 4
        End Time: 299


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.705882352941177
        With Duration: 8
        End Time: 325


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 355


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 4
        End Time: 390


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 432


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7
        End Time: 462


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 488


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12
        End Time: 551


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 597


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 1
        End Time: 611


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18
        End Time: 631


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18
        End Time: 654


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 674


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 41.666666666666664
        With Duration: 6
        End Time: 710


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 8
        End Time: 760

##### Relationship Results #####


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 888
        Received: 15629
        Completed: Method30, Method17, Method8, Method24, Method6, Method21, Method2, Method29, Method5, Method12, Method18, Method13, Method10, Method26, Method16, Method22, Method15, Method19, Method20, Method7, Method23, Method9, Method28, Method25, Method1, Method27, Method11, Method5, Method2, Method9, Method7, Method10, Method26, Method14, Method12, Method16, Method8, Method21, Method24, Method23, Method15, Method6, Method28, Method27, Method20, Method17, Method30, Method25, Method13, Method22, Method1, Method29, Method18, Method20, Method28, Method12, Method21, Method17, Method6, Method10, Method3, Method15, Method8, Method24, Method5, Method2, Method1, Method16, Method13, Method18, Method14, Method27, Method7, Method30, Method23, Method22, Method26, Method9, Method29, Method15, Method5, Method24, Method6, Method30, Method27, Method1, Method7, Method28, Method10, Method23, Method4, Method11, Method20, Method3, Method21, Method9, Method29, Method8, Method13, Method2, Method22, Method25, Method26, Method18, Method12, Method17, Method16, Method19, Method7, Method29, Method12, Method9, Method18, Method22, Method13, Method2, Method21, Method3, Method5, Method15, Method28, Method24, Method27, Method23, Method30, Method26, Method1, Method17, Method16, Method10, Method8, Method6, Method20, Method25, Method16, Method28, Method5, Method23, Method24, Method22, Method30, Method12, Method7, Method15, Method13, Method8, Method27, Method26, Method20, Method17, Method21, Method9, Method29, Method10, Method6, Method1, Method18, Method25, Method2, Method22, Method20, Method6, Method1, Method8, Method5, Method10, Method4, Method15, Method9, Method23, Method28, Method2, Method24, Method7, Method13, Method27, Method18, Method14, Method12, Method16, Method26, Method17, Method21, Method29, Method30, Method19, Method15, Method29, Method8, Method16, Method26, Method9, Method24, Method23, Method27, Method13, Method1, Method22, Method6, Method17, Method7, Method10, Method21, Method20, Method12, Method19, Method18, Method4, Method30, Method25, Method5, Method2, Method28, Method28, Method6, Method10, Method22, Method14, Method24, Method15, Method19, Method16, Method18, Method21, Method30, Method12, Method26, Method13, Method2, Method27, Method5, Method29, Method17, Method10, Method30, Method21, Method27, Method11, Method1, Method15, Method13, Method18, Method5, Method29, Method2, Method23, Method3, Method6, Method12, Method20, Method5, Method1, Method16, Method18, Method20, Method15, Method12, Method29, Method28, Method23, Method22, Method5, Method18, Method13, Method27, Method20, Method30, Method11, Method21, Method15, Method29, Method8, Method1, Method12, Method16, Method6, Method2, Method7, Method26, Method14, Method3, Method2, Method22, Method23, Method13, Method1, Method4, Method19, Method8, Method6, Method27, Method7, Method28, Method20, Method3, Method8, Method11, Method16, Method26, Method20, Method8, Method12, Method10, Method9, Method28, Method22, Method21, Method1, Method7, Method21, Method12, Method18, Method6, Method22, Method3, Method30, Method10, Method29, Method28, Method15, Method23, Method20, Method19, Method4, Method5, Method9, Method16, Method2, Method26, Method24, Method27, Method13, Method25, Method8, Method17