####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6143484648

##### Method Results #####


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 7
        End Time: 17


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 18
        End Time: 37


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 44


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 5
        End Time: 51


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 53


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 55


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 62


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5
        End Time: 69


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 71


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6
        End Time: 79


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 89


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3
        End Time: 96


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 102


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 123


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 20
        End Time: 175


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 7
        End Time: 189


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 10
        End Time: 209


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 221


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12
        End Time: 235


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 237


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3
        End Time: 242


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6
        End Time: 250


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1
        End Time: 253


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 257


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2
        End Time: 264


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 273


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 285


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 295


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 11
        End Time: 308


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 311

##### Relationship Results #####


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 480
        Received: 6696
        Completed: Method18, Method10, Method16, Method2, Method5, Method29, Method30, Method21, Method12, Method11, Method1, Method25, Method24, Method6, Method8, Method3, Method9, Method23, Method15, Method27, Method22, Method20, Method7, Method9, Method11, Method26, Method27, Method21, Method23, Method6, Method3, Method14, Method1, Method30, Method7, Method2, Method29, Method28, Method8, Method20, Method18, Method4, Method10, Method25, Method22, Method5, Method15, Method24, Method12, Method10, Method27, Method16, Method12, Method25, Method14, Method2, Method29, Method5, Method24, Method21, Method7, Method8, Method22, Method30, Method18, Method3, Method23, Method20, Method9, Method11, Method15, Method6, Method1, Method17, Method23, Method6, Method25, Method8, Method3, Method27, Method9, Method16, Method29, Method26, Method20, Method5, Method12, Method22, Method30, Method7, Method14, Method10, Method28, Method18, Method15, Method19, Method1, Method21, Method2, Method11, Method24, Method6, Method9, Method15, Method26, Method14, Method2, Method17, Method7, Method1, Method28, Method3, Method18, Method21, Method20, Method4, Method22, Method10, Method30, Method11, Method12, Method24, Method19, Method29, Method25, Method27, Method23, Method5, Method13, Method2, Method22, Method16, Method7, Method18, Method8, Method24, Method11, Method3, Method23, Method6, Method5, Method15, Method1, Method13, Method10, Method25, Method9, Method20, Method29, Method12, Method30, Method19, Method26, Method27, Method21, Method4, Method24, Method9, Method1, Method12, Method27, Method30, Method13, Method11, Method14, Method10, Method18, Method7, Method15, Method21, Method6, Method4, Method2, Method25, Method3, Method26, Method5, Method23, Method22, Method29, Method20, Method2, Method27, Method29, Method22, Method14, Method5, Method23, Method12, Method30, Method20, Method21, Method8, Method9, Method18, Method7, Method6, Method11, Method24, Method19, Method15, Method1, Method4, Method3, Method25, Method10, Method26