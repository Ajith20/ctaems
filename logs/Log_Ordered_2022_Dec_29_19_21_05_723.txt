####### Ordered Logger #######


Final Duration: 15.0
Final Quality: 13.0
Seed: -2974619723252690137

##### Method Results #####


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 7
        End Time: 11


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2
        End Time: 15

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates6
        Type: Facilitates
        Source : Method1
        Target : Method2


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method1
        Target : Method3

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 5
        Received: 28
        Completed: Method2, Method1Final Duration: 15.0
Final Quality: 0.0
Seed: -2974619723252690137

##### Method Results #####

##### Relationship Results #####

##### Agent Results #####
