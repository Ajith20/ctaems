####### Ordered Logger #######


Final Duration: 400.0
Final Quality: 0.0
Seed: 2434455791

##### Method Results #####


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 4


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 20


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 6


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.8
        With Duration: 3


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2

##### Relationship Results #####


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 234
        Received: 1123
        Completed: Method6, Method27, Method17, Method23, Method3, Method18, Method21, Method24, Method2, Method30, Method29, Method16, Method5, Method28, Method11, Method12, Method22, Method1, Method4, Method7, Method20, Method13, Method10, Method15, Method26, Method8, Method9, Method28, Method6, Method16, Method21, Method2, Method27, Method8, Method22, Method7, Method10, Method13, Method24, Method11, Method18, Method29, Method23, Method17, Method15, Method26, Method9, Method5, Method30, Method20, Method12, Method1