####### Ordered Logger #######


Final Duration: 400.0
Final Quality: 0.0
Seed: 5596162778

##### Method Results #####


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.5
        With Duration: 4


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 12


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 4


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 6


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 18


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.5
        With Duration: 1


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 11


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 7


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 9


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 8


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 7


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 17


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.8
        With Duration: 4


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 7


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 7

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 1731
        Received: 9582
        Completed: Method14, Method3, Method8, Method20, Method6, Method15, Method30, Method7, Method1, Method18, Method9, Method13, Method12, Method28, Method21, Method22, Method10, Method23, Method2, Method25, Method24, Method17, Method5, Method26, Method16, Method27, Method29, Method28, Method6, Method16, Method21, Method2, Method27, Method8, Method22, Method7, Method10, Method13, Method24, Method11, Method18, Method29, Method23, Method17, Method15, Method26, Method9, Method5, Method30, Method20, Method12, Method1, Method27, Method26, Method22, Method30, Method28, Method11, Method21, Method23, Method2, Method13, Method12, Method14, Method1, Method29, Method18, Method16, Method24, Method7, Method6, Method8, Method9, Method10, Method20, Method15, Method5, Method19, Method17, Method18, Method23, Method26, Method10, Method24, Method29, Method22, Method9, Method7, Method28, Method3, Method19, Method8, Method5, Method6, Method16, Method30, Method25, Method20, Method15, Method2, Method27, Method17, Method12, Method1, Method21, Method13, Method26, Method20, Method22, Method30, Method23, Method9, Method29, Method16, Method28, Method15, Method21, Method7, Method8, Method1, Method24, Method17, Method18, Method2, Method27, Method10, Method19, Method13, Method6, Method25, Method5, Method12, Method2, Method21, Method15, Method14, Method10, Method17, Method6, Method5, Method20, Method18, Method28, Method30, Method3, Method8, Method26, Method27, Method29, Method13, Method1, Method12, Method11, Method24, Method7, Method9, Method23, Method16, Method22, Method13, Method20, Method7, Method22, Method6, Method2, Method12, Method9, Method15, Method14, Method8, Method24, Method23, Method19, Method28, Method25, Method5, Method27, Method3, Method16, Method10, Method30, Method21, Method26, Method1, Method29, Method17, Method18, Method5, Method10, Method3, Method16, Method20, Method26, Method9, Method12, Method8, Method29, Method2, Method18, Method6, Method1, Method22, Method24, Method7, Method25, Method21, Method17, Method19, Method15, Method23, Method13, Method30, Method28, Method4, Method27, Method11, Method6, Method21, Method3, Method2, Method1, Method20, Method13, Method26, Method4, Method28, Method12, Method27, Method10, Method5, Method29, Method30, Method18, Method8, Method16, Method9, Method23, Method15, Method22, Method17, Method24, Method7, Method29, Method26, Method2, Method23, Method7, Method22, Method13, Method12, Method30, Method27, Method17, Method8, Method11, Method28, Method10, Method6, Method9, Method20, Method21, Method3, Method18, Method15, Method24, Method5, Method16, Method19, Method1, Method9, Method20, Method13, Method12, Method25, Method2, Method27, Method11, Method22, Method21, Method16, Method23, Method30, Method8, Method19, Method10, Method18, Method29, Method5, Method17, Method6, Method7, Method1, Method26, Method28, Method24, Method15, Method18, Method3, Method2, Method17, Method7, Method14, Method12, Method23, Method27, Method21, Method28, Method22, Method26, Method1, Method13, Method30, Method15, Method20, Method6, Method24, Method29, Method16, Method5, Method8, Method9, Method4, Method10, Method30, Method7, Method13, Method18, Method24, Method21, Method29, Method28, Method16, Method17, Method20, Method6, Method22, Method26, Method9, Method12, Method27, Method23, Method5, Method8, Method10, Method25, Method11, Method2, Method19, Method15, Method9, Method22, Method3, Method1, Method17, Method30, Method28, Method27, Method18, Method6, Method8, Method21, Method12, Method5, Method25, Method16, Method10, Method2, Method14, Method13, Method7, Method26, Method15, Method24, Method20, Method23, Method19, Method29, Method4, Method11, Method26, Method14, Method3, Method12, Method5, Method1, Method2, Method10, Method8, Method22, Method9, Method27, Method30, Method6, Method17, Method18, Method4, Method15, Method28, Method25, Method24, Method21, Method23, Method29, Method20, Method16, Method7, Method13, Method11, Method23, Method16, Method20, Method10, Method15, Method12, Method17, Method5, Method6, Method2, Method30, Method13, Method18, Method24, Method22, Method9, Method7, Method8, Method1, Method14, Method28, Method29, Method21, Method27, Method26, Method24, Method28, Method22, Method9, Method8, Method15, Method13, Method17, Method16, Method18, Method27, Method23, Method19, Method25, Method12, Method20, Method29, Method6, Method5, Method10, Method21, Method1, Method30, Method26, Method7, Method2, Method26, Method12, Method29, Method24, Method16, Method13, Method22, Method1, Method5, Method27, Method2, Method20, Method15, Method10, Method9, Method6, Method17, Method19, Method28, Method8, Method23, Method21, Method30, Method18, Method7, Method25