####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 2513721403

##### Method Results #####


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 17
        End Time: 51


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 62


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 70


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 78


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 9
        End Time: 91


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 6
        End Time: 104


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 116


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 136


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 145


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 156


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12
        End Time: 172


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 188


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 202


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 6
        End Time: 210


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 215


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 20
        End Time: 238


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5
        End Time: 268


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 289


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 301


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6
        End Time: 315


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 318


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9
        End Time: 333


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 351


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9
        End Time: 372


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 390


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 395


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 399


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1
        End Time: 404


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7
        End Time: 415


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 420

##### Relationship Results #####


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 960
        Received: 13984
        Completed: Method29, Method27, Method23, Method3, Method30, Method2, Method21, Method18, Method24, Method22, Method7, Method8, Method9, Method13, Method10, Method12, Method1, Method11, Method15, Method26, Method19, Method6, Method5, Method20, Method28, Method25, Method16, Method9, Method11, Method26, Method27, Method21, Method23, Method6, Method3, Method14, Method1, Method30, Method7, Method2, Method29, Method28, Method8, Method20, Method18, Method4, Method10, Method25, Method22, Method5, Method15, Method24, Method12, Method10, Method27, Method16, Method12, Method25, Method14, Method2, Method29, Method5, Method24, Method21, Method7, Method8, Method22, Method30, Method18, Method3, Method23, Method20, Method9, Method11, Method15, Method6, Method1, Method17, Method23, Method6, Method25, Method8, Method3, Method27, Method9, Method16, Method29, Method26, Method20, Method5, Method12, Method22, Method30, Method7, Method14, Method10, Method28, Method18, Method15, Method19, Method1, Method21, Method2, Method11, Method24, Method6, Method9, Method15, Method26, Method14, Method2, Method17, Method7, Method1, Method28, Method3, Method18, Method21, Method20, Method4, Method22, Method10, Method30, Method11, Method12, Method24, Method19, Method29, Method25, Method27, Method23, Method5, Method13, Method2, Method22, Method16, Method7, Method18, Method8, Method24, Method11, Method3, Method23, Method6, Method5, Method15, Method1, Method13, Method10, Method25, Method9, Method20, Method29, Method12, Method30, Method19, Method26, Method27, Method21, Method4, Method24, Method9, Method1, Method12, Method27, Method30, Method13, Method11, Method14, Method10, Method18, Method7, Method15, Method21, Method6, Method4, Method2, Method25, Method3, Method26, Method5, Method23, Method22, Method29, Method20, Method2, Method27, Method29, Method22, Method14, Method5, Method23, Method12, Method30, Method20, Method21, Method8, Method9, Method18, Method7, Method6, Method11, Method24, Method19, Method15, Method1, Method4, Method3, Method25, Method10, Method26, Method20, Method10, Method16, Method18, Method9, Method15, Method2, Method24, Method7, Method21, Method17, Method11, Method3, Method5, Method23, Method27, Method6, Method26, Method29, Method22, Method19, Method30, Method12, Method13, Method25, Method1, Method28, Method23, Method10, Method25, Method11, Method9, Method17, Method5, Method18, Method16, Method7, Method15, Method27, Method6, Method2, Method30, Method24, Method1, Method29, Method21, Method12, Method22, Method3, Method26, Method20, Method6, Method29, Method24, Method13, Method23, Method1, Method16, Method4, Method5, Method11, Method3, Method10, Method21, Method14, Method22, Method7, Method27, Method15, Method12, Method19, Method25, Method18, Method9, Method30, Method20, Method2, Method21, Method10, Method27, Method9, Method25, Method1, Method17, Method4, Method30, Method5, Method26, Method14, Method15, Method3, Method11, Method23, Method18, Method22, Method24, Method28, Method29, Method12, Method20, Method7, Method2, Method15, Method23, Method11, Method24, Method12, Method7, Method2, Method10, Method18, Method27, Method13, Method21, Method22, Method1, Method4, Method16, Method3, Method9, Method29, Method20, Method5, Method30, Method26, Method25, Method19, Method21, Method6, Method5, Method24, Method11, Method17, Method13, Method25, Method8, Method22, Method1, Method2, Method19, Method4, Method18, Method20, Method9, Method10, Method30, Method7, Method23, Method29, Method27, Method16, Method15, Method3, Method12, Method26, Method2, Method7, Method29, Method6, Method18, Method22, Method20, Method11, Method9, Method30, Method10, Method23, Method8, Method19, Method3, Method25, Method16, Method12, Method24, Method27, Method1, Method15, Method21, Method5, Method26, Method27, Method25, Method6, Method24, Method10, Method21, Method20, Method29, Method2, Method3, Method5, Method11, Method8, Method9, Method1, Method30, Method23, Method7, Method14, Method18, Method12, Method15, Method22, Method26