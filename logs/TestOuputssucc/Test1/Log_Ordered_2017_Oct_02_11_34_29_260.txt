####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 7926855541

##### Method Results #####


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15
        End Time: 36


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 44


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 10
        End Time: 56


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 18
        End Time: 78


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 88


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 99


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 103


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 106


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3
        End Time: 112


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 116


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 4
        End Time: 123


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5
        End Time: 132


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 135


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 145


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 152


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 8
        End Time: 162


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 169


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 172


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 182


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 194


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 196


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 203


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 211


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 7
        End Time: 225


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 4
        End Time: 232


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 5
        End Time: 242


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 244


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5
        End Time: 251


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 9
        End Time: 266


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12
        End Time: 280

##### Relationship Results #####


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 720
        Received: 10391
        Completed: Method29, Method27, Method23, Method3, Method30, Method2, Method21, Method18, Method24, Method22, Method7, Method8, Method9, Method13, Method10, Method12, Method1, Method11, Method15, Method26, Method19, Method6, Method5, Method20, Method28, Method25, Method16, Method9, Method11, Method26, Method27, Method21, Method23, Method6, Method3, Method14, Method1, Method30, Method7, Method2, Method29, Method28, Method8, Method20, Method18, Method4, Method10, Method25, Method22, Method5, Method15, Method24, Method12, Method10, Method27, Method16, Method12, Method25, Method14, Method2, Method29, Method5, Method24, Method21, Method7, Method8, Method22, Method30, Method18, Method3, Method23, Method20, Method9, Method11, Method15, Method6, Method1, Method17, Method23, Method6, Method25, Method8, Method3, Method27, Method9, Method16, Method29, Method26, Method20, Method5, Method12, Method22, Method30, Method7, Method14, Method10, Method28, Method18, Method15, Method19, Method1, Method21, Method2, Method11, Method24, Method6, Method9, Method15, Method26, Method14, Method2, Method17, Method7, Method1, Method28, Method3, Method18, Method21, Method20, Method4, Method22, Method10, Method30, Method11, Method12, Method24, Method19, Method29, Method25, Method27, Method23, Method5, Method13, Method2, Method22, Method16, Method7, Method18, Method8, Method24, Method11, Method3, Method23, Method6, Method5, Method15, Method1, Method13, Method10, Method25, Method9, Method20, Method29, Method12, Method30, Method19, Method26, Method27, Method21, Method4, Method24, Method9, Method1, Method12, Method27, Method30, Method13, Method11, Method14, Method10, Method18, Method7, Method15, Method21, Method6, Method4, Method2, Method25, Method3, Method26, Method5, Method23, Method22, Method29, Method20, Method2, Method27, Method29, Method22, Method14, Method5, Method23, Method12, Method30, Method20, Method21, Method8, Method9, Method18, Method7, Method6, Method11, Method24, Method19, Method15, Method1, Method4, Method3, Method25, Method10, Method26, Method20, Method10, Method16, Method18, Method9, Method15, Method2, Method24, Method7, Method21, Method17, Method11, Method3, Method5, Method23, Method27, Method6, Method26, Method29, Method22, Method19, Method30, Method12, Method13, Method25, Method1, Method28, Method23, Method10, Method25, Method11, Method9, Method17, Method5, Method18, Method16, Method7, Method15, Method27, Method6, Method2, Method30, Method24, Method1, Method29, Method21, Method12, Method22, Method3, Method26, Method20, Method6, Method29, Method24, Method13, Method23, Method1, Method16, Method4, Method5, Method11, Method3, Method10, Method21, Method14, Method22, Method7, Method27, Method15, Method12, Method19, Method25, Method18, Method9, Method30, Method20, Method2, Method21, Method10, Method27, Method9, Method25, Method1, Method17, Method4, Method30, Method5, Method26, Method14, Method15, Method3, Method11, Method23, Method18, Method22, Method24, Method28, Method29, Method12, Method20, Method7, Method2