####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 2187756656

##### Method Results #####


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 10


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 14


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4
        End Time: 20


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7
        End Time: 29


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 36


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 55


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 81


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 85


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 93


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 96


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 5
        End Time: 105


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 112


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 124


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 130


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 5
        End Time: 137


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 139


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 4
        End Time: 145


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9
        End Time: 158


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 9
        End Time: 170


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 4
        End Time: 197


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 9
        End Time: 237


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 7
        End Time: 274


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 295


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 6
        End Time: 318


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 20
        End Time: 346


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 358


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 9
        End Time: 374


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15
        End Time: 397


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 401


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 405

##### Relationship Results #####


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 1410
        Received: 22252
        Completed: Method27, Method10, Method9, Method22, Method14, Method26, Method7, Method29, Method6, Method24, Method1, Method28, Method18, Method11, Method30, Method21, Method2, Method19, Method5, Method12, Method8, Method15, Method23, Method3, Method25, Method20, Method22, Method6, Method9, Method8, Method27, Method18, Method25, Method2, Method29, Method11, Method10, Method19, Method30, Method12, Method21, Method26, Method5, Method1, Method7, Method21, Method17, Method27, Method10, Method6, Method16, Method1, Method9, Method11, Method14, Method24, Method13, Method30, Method3, Method20, Method22, Method2, Method29, Method5, Method25, Method19, Method8, Method12, Method15, Method23, Method18, Method11, Method21, Method8, Method30, Method22, Method2, Method23, Method1, Method19, Method7, Method15, Method5, Method6, Method29, Method24, Method16, Method12, Method20, Method3, Method13, Method25, Method10, Method18, Method9, Method27, Method4, Method12, Method5, Method29, Method1, Method16, Method25, Method27, Method6, Method8, Method23, Method20, Method7, Method11, Method22, Method9, Method18, Method30, Method10, Method5, Method22, Method1, Method16, Method14, Method23, Method19, Method12, Method27, Method2, Method3, Method7, Method18, Method16, Method17, Method15, Method20, Method12, Method1, Method7, Method24, Method25, Method30, Method10, Method9, Method6, Method4, Method23, Method29, Method6, Method25, Method7, Method22, Method15, Method10, Method3, Method29, Method23, Method9, Method16, Method19, Method18, Method5, Method20, Method1, Method27, Method30, Method8, Method5, Method17, Method14, Method9, Method30, Method18, Method25, Method12, Method15, Method27, Method1, Method22, Method11, Method6, Method24, Method12, Method14, Method10, Method1, Method27, Method8, Method25, Method15, Method3, Method5, Method20, Method2, Method4, Method21, Method22, Method29, Method23, Method11, Method30, Method13, Method9, Method7, Method18, Method17, Method16, Method12, Method24, Method21, Method10, Method27, Method15, Method14, Method5, Method7, Method22, Method25, Method20, Method2, Method30, Method11, Method21, Method23, Method17, Method22, Method2, Method16, Method8, Method18, Method6, Method10, Method25, Method29, Method30, Method5, Method7, Method12, Method3, Method24, Method19, Method15, Method1, Method20, Method9, Method27, Method7, Method6, Method11, Method16, Method17, Method23, Method9, Method15, Method20, Method5, Method30, Method2, Method12, Method21, Method10, Method1, Method14, Method3, Method27, Method29, Method16, Method20, Method9, Method22, Method26, Method10, Method14, Method25, Method18, Method5, Method15, Method11, Method6, Method24, Method7, Method23, Method12, Method30, Method20, Method15, Method27, Method6, Method23, Method25, Method11, Method18, Method24, Method5, Method10, Method2, Method9, Method1, Method13, Method3, Method29, Method7, Method26, Method16, Method21, Method22, Method7, Method17, Method6, Method20, Method27, Method16, Method10, Method12, Method8, Method1, Method22, Method14, Method3, Method29, Method15, Method25, Method18, Method21, Method30, Method5, Method9, Method11, Method2, Method23, Method24, Method8, Method5, Method25, Method3, Method30, Method16, Method17, Method14, Method24, Method27, Method1, Method10, Method21, Method11, Method29, Method22, Method23, Method2, Method6, Method12, Method9, Method18, Method26, Method15, Method20, Method7, Method19, Method13, Method4, Method16, Method5, Method25, Method23, Method12, Method27, Method20, Method29, Method2, Method7, Method11, Method24, Method6, Method9, Method1, Method30, Method22, Method21, Method10, Method15, Method13, Method18, Method3, Method10, Method3, Method6, Method12, Method24, Method11, Method8, Method9, Method17, Method23, Method2, Method18, Method15, Method30, Method13, Method16, Method27, Method1, Method29, Method20, Method5, Method7, Method25, Method22, Method21, Method24, Method18, Method15, Method6, Method12, Method7, Method2, Method1, Method11, Method25, Method27, Method9, Method22, Method17, Method19, Method5, Method20, Method29, Method3, Method26, Method23, Method13, Method30, Method21, Method10, Method4, Method16, Method30, Method10, Method17, Method16, Method23, Method22, Method3, Method2, Method27, Method18, Method12, Method9, Method11, Method8, Method29, Method1, Method5, Method21, Method15, Method20, Method7, Method24, Method25, Method6, Method27, Method10, Method9, Method24, Method12, Method25, Method23, Method18, Method8, Method17, Method11, Method21, Method16, Method5, Method6, Method3, Method26, Method20, Method30, Method2, Method1, Method29, Method22, Method7, Method15, Method13, Method1, Method27, Method25, Method24, Method6, Method5, Method2, Method8, Method22, Method9, Method30, Method18, Method11, Method13, Method19, Method3, Method10, Method21, Method20, Method4, Method15, Method12, Method7, Method16, Method29, Method23, Method27, Method23, Method20, Method8, Method1, Method24, Method12, Method22, Method29, Method11, Method9, Method16, Method3, Method15, Method7, Method5, Method6, Method25, Method14, Method18, Method19, Method13, Method21, Method2, Method10, Method30, Method4, Method25, Method11, Method29, Method22, Method9, Method16, Method2, Method19, Method14, Method12, Method3, Method1, Method23, Method26, Method18, Method6, Method4, Method24, Method7, Method21, Method5, Method8, Method20, Method10, Method27, Method15, Method30