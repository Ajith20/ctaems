####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 8880615240

##### Method Results #####


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 569


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 9
        End Time: 47


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 60


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 75


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 92


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1
        End Time: 111


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18
        End Time: 158


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 14
        End Time: 186


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 201


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 227


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 276


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 297


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 315


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 337


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 353


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12
        End Time: 371


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 377


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18
        End Time: 400


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7
        End Time: 421


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 446


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 485


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 498


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 6
        End Time: 513


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 528


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 3
        End Time: 546


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 554


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 569


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 19
        End Time: 590


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 618


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 656


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9
        End Time: 683

##### Relationship Results #####


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 536
        Received: 8844
        Completed: Method27, Method10, Method9, Method22, Method14, Method26, Method7, Method29, Method6, Method24, Method1, Method28, Method18, Method11, Method30, Method21, Method2, Method19, Method5, Method12, Method8, Method15, Method23, Method3, Method25, Method20, Method22, Method6, Method9, Method8, Method27, Method18, Method25, Method2, Method29, Method11, Method10, Method19, Method30, Method12, Method21, Method26, Method5, Method1, Method7, Method21, Method17, Method27, Method10, Method6, Method16, Method1, Method9, Method11, Method14, Method24, Method13, Method30, Method3, Method20, Method22, Method2, Method29, Method5, Method25, Method19, Method8, Method12, Method15, Method23, Method18, Method11, Method21, Method8, Method30, Method22, Method2, Method23, Method1, Method19, Method7, Method15, Method5, Method6, Method29, Method24, Method16, Method12, Method20, Method3, Method13, Method25, Method10, Method18, Method9, Method27, Method4, Method12, Method5, Method29, Method1, Method16, Method25, Method27, Method6, Method8, Method23, Method20, Method7, Method11, Method22, Method9, Method18, Method30, Method10, Method5, Method22, Method1, Method16, Method14, Method23, Method19, Method12, Method27, Method2, Method3, Method7, Method18, Method16, Method17, Method15, Method20, Method12, Method1, Method7, Method24, Method25, Method30, Method10, Method9, Method6, Method4, Method23, Method29, Method6, Method25, Method7, Method22, Method15, Method10, Method3, Method29, Method23, Method9, Method16, Method19, Method18, Method5, Method20, Method1, Method27, Method30, Method8, Method5, Method17, Method14, Method9, Method30, Method18, Method25, Method12, Method15, Method27, Method1, Method22, Method11, Method6, Method24, Method12, Method14, Method10, Method1, Method27, Method8, Method25, Method15, Method3, Method5, Method20, Method2, Method4, Method21, Method22, Method29, Method23, Method11, Method30, Method13, Method9, Method7, Method18