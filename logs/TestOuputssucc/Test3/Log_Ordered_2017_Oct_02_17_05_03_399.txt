####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 5687031444

##### Method Results #####


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 32


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 10
        End Time: 44


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 47


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 6
        End Time: 57


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 4
        End Time: 64


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 67


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 74


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 76


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 88


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 7
        End Time: 99


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 102


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 17
        End Time: 124


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 126


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 138


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 6
        End Time: 149


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5
        End Time: 158


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3
        End Time: 167


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 17
        End Time: 191


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7
        End Time: 201


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 206


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 6
        End Time: 214


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18
        End Time: 235


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 238


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 240


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 248


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 6
        End Time: 256


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 262


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6
        End Time: 273


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 281


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 305

##### Relationship Results #####


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 1260
        Received: 18468
        Completed: Method7, Method20, Method21, Method22, Method16, Method8, Method2, Method3, Method23, Method24, Method19, Method11, Method25, Method13, Method30, Method27, Method9, Method17, Method29, Method15, Method26, Method14, Method5, Method10, Method1, Method12, Method6, Method18, Method22, Method6, Method9, Method8, Method27, Method18, Method25, Method2, Method29, Method11, Method10, Method19, Method30, Method12, Method21, Method26, Method5, Method1, Method7, Method15, Method20, Method4, Method16, Method3, Method24, Method28, Method23, Method13, Method21, Method17, Method27, Method10, Method6, Method16, Method1, Method9, Method11, Method7, Method14, Method24, Method13, Method30, Method3, Method20, Method22, Method2, Method29, Method5, Method25, Method19, Method8, Method12, Method15, Method23, Method18, Method11, Method21, Method8, Method30, Method22, Method2, Method23, Method1, Method19, Method7, Method15, Method5, Method6, Method29, Method24, Method16, Method12, Method20, Method3, Method13, Method25, Method10, Method18, Method9, Method27, Method4, Method12, Method5, Method29, Method1, Method16, Method25, Method27, Method6, Method8, Method23, Method20, Method7, Method11, Method22, Method9, Method18, Method30, Method21, Method15, Method2, Method26, Method3, Method10, Method24, Method19, Method10, Method5, Method22, Method30, Method1, Method16, Method14, Method23, Method19, Method12, Method27, Method2, Method3, Method7, Method18, Method29, Method9, Method8, Method21, Method11, Method20, Method4, Method15, Method25, Method24, Method16, Method17, Method15, Method20, Method12, Method1, Method7, Method24, Method25, Method30, Method10, Method9, Method6, Method4, Method23, Method29, Method14, Method27, Method21, Method2, Method5, Method3, Method11, Method18, Method22, Method6, Method25, Method7, Method22, Method15, Method10, Method3, Method29, Method23, Method9, Method16, Method19, Method18, Method5, Method20, Method1, Method27, Method30, Method11, Method2, Method4, Method12, Method21, Method24, Method5, Method17, Method14, Method9, Method30, Method8, Method18, Method25, Method12, Method15, Method27, Method1, Method22, Method11, Method6, Method29, Method2, Method7, Method3, Method23, Method20, Method21, Method24, Method10, Method19, Method4, Method24, Method12, Method14, Method10, Method1, Method27, Method8, Method25, Method15, Method3, Method5, Method20, Method2, Method4, Method21, Method22, Method29, Method23, Method11, Method30, Method13, Method9, Method7, Method18, Method17, Method16, Method12, Method24, Method21, Method10, Method27, Method15, Method14, Method5, Method7, Method22, Method25, Method20, Method2, Method30, Method11, Method3, Method19, Method9, Method1, Method18, Method29, Method6, Method23, Method21, Method23, Method11, Method17, Method22, Method2, Method16, Method8, Method18, Method6, Method10, Method25, Method29, Method30, Method5, Method7, Method12, Method3, Method24, Method19, Method15, Method1, Method20, Method9, Method27, Method7, Method6, Method11, Method16, Method17, Method23, Method9, Method15, Method20, Method5, Method30, Method2, Method12, Method21, Method10, Method1, Method14, Method3, Method27, Method4, Method26, Method24, Method22, Method25, Method18, Method29, Method29, Method16, Method20, Method9, Method22, Method26, Method10, Method14, Method25, Method18, Method5, Method15, Method11, Method6, Method24, Method7, Method23, Method12, Method1, Method28, Method4, Method2, Method19, Method21, Method3, Method30, Method27, Method30, Method20, Method15, Method27, Method12, Method6, Method23, Method25, Method11, Method18, Method24, Method5, Method10, Method2, Method9, Method1, Method13, Method3, Method29, Method7, Method26, Method16, Method21, Method22, Method7, Method17, Method6, Method20, Method27, Method16, Method10, Method12, Method8, Method1, Method22, Method14, Method3, Method29, Method15, Method25, Method18, Method21, Method30, Method5, Method9, Method11, Method2, Method23, Method24, Method8, Method5, Method25, Method3, Method30, Method16, Method17, Method14, Method24, Method27, Method1, Method10, Method21, Method11, Method29, Method22, Method23, Method2, Method6, Method12, Method9, Method18, Method26, Method15, Method20, Method7, Method19, Method13, Method4, Method16, Method5, Method25, Method23, Method12, Method27, Method20, Method29, Method2, Method7, Method11, Method24, Method6, Method9, Method1, Method30, Method22, Method21, Method10, Method15, Method13, Method18, Method3, Method10, Method3, Method6, Method12, Method24, Method11, Method8, Method9, Method17, Method23, Method2, Method18, Method15, Method30, Method13, Method16, Method27, Method1, Method29, Method20, Method5, Method7, Method25, Method22, Method21, Method24, Method18, Method15, Method6, Method12, Method7, Method2, Method1, Method11, Method25, Method27, Method9, Method22, Method17, Method19, Method5, Method20, Method29, Method3, Method26, Method23, Method13, Method30, Method21, Method10, Method4, Method16, Method30, Method10, Method17, Method16, Method23, Method22, Method3, Method2, Method27, Method18, Method12, Method9, Method11, Method8, Method29, Method1, Method5, Method21, Method15, Method20, Method7, Method24, Method25, Method6