####### Sequential Logger #######



    ## Message ##
        Sending a SetRandomSeedMessage to Agent1
        The random seed is -1409473398389181228


    ## Message ##
        Sending a SetRandomSeedMessage to Agent2
        The random seed is -1409473398389181228


    ## Message ##
        Sending a InitialTreeMessage to Agent1
        Tree Head: Problem1


    ## Message ##
        Sending a InitialTreeMessage to Agent2
        Tree Head: Problem1


    ## Message ##
        Sending a StartSimulationMessage to Agent1
        The Simulation has Started


    ## Message ##
        Sending a StartSimulationMessage to Agent2
        The Simulation has Started


    ## Message ##
        Received a AskMethodStatusMessage From Agent2
        Asked Status of Method3


    ## Message ##
        Received a AskMethodStatusMessage From Agent2
        Asked Status of Method4


    ## Message ##
        Received a AskMethodStatusMessage From Agent1
        Asked Status of Method1


    ## Message ##
        Received a AskMethodStatusMessage From Agent1
        Asked Status of Method2


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 1 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 1 has occurred


    ## Message ##
        Sending a NotifyMethodStatusMessage to Agent2
        Method3 is Enabled: false Completed: false In Progress: false


    ## Message ##
        Sending a NotifyMethodStatusMessage to Agent2
        Method4 is Enabled: false Completed: false In Progress: false


    ## Message ##
        Sending a NotifyMethodStatusMessage to Agent1
        Method1 is Enabled: true Completed: false In Progress: false


    ## Message ##
        Sending a NotifyMethodStatusMessage to Agent1
        Method2 is Enabled: true Completed: false In Progress: false


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 2 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 2 has occurred


    ## Message ##
        Received a StartMethodMessage From Agent1
        Agent1 has asked to start Method1


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 3 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 3 has occurred


    ## Message ##
        Sending a ConfirmMethodStartMessage to Agent1
        Method1 has started: true


    ## Message ##
        Received a AgentToAgentMessage From Agent1
        Agent1 has sent a message to Agent2 with content: {"Load":2,"Running":{"VisibleToAgents":["Agent1","Agent2"],"NodeName":"Method1","Quality":"(20.0,0.5),(27.0,0.25),(13.0,0.25)","NodeType":"Method","Duration":"(2.0,0.5),(1.0,0.25),(2.0,0.25)","AgentName":"Agent1","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 4 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 4 has occurred


    ## Message ##
        Received a AgentToAgentMessage From Agent1
        Agent1 has sent a message to Agent2 with content: {"Load":2,"Running":{"VisibleToAgents":["Agent1","Agent2"],"NodeName":"Method1","Quality":"(20.0,0.5),(27.0,0.25),(13.0,0.25)","NodeType":"Method","Duration":"(2.0,0.5),(1.0,0.25),(2.0,0.25)","AgentName":"Agent1","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 5 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 5 has occurred


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : CTask2


    ## Message ##
        Sending a NotifyRelationshipActivationMessage to Agent1
         Relationship enables1 has been activated


    ## Message ##
        Sending a NotifyRelationshipActivationMessage to Agent2
         Relationship enables1 has been activated


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Message ##
        Sending a NotifyMethodCompletedMessage to Agent1
        Agent1 has completed Method1


    ## Message ##
        Received a AskMethodStatusMessage From Agent2
        Asked Status of Method3


    ## Message ##
        Received a AskMethodStatusMessage From Agent2
        Asked Status of Method4


    ## Message ##
        Received a AgentToAgentMessage From Agent1
        Agent1 has sent a message to Agent2 with content: {"Load":1,"Running":"","Compleated":"Method1"}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 6 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 6 has occurred


    ## Message ##
        Sending a NotifyMethodStatusMessage to Agent2
        Method3 is Enabled: true Completed: false In Progress: false


    ## Message ##
        Sending a NotifyMethodStatusMessage to Agent2
        Method4 is Enabled: true Completed: false In Progress: false


    ## Message ##
        Received a AgentToAgentMessage From Agent1
        Agent1 has sent a message to Agent2 with content: {"Load":1,"Running":"","Compleated":"Method1"}


    ## Message ##
        Received a StartMethodMessage From Agent1
        Agent1 has asked to start Method2


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 7 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 7 has occurred


    ## Message ##
        Sending a ConfirmMethodStartMessage to Agent1
        Method2 has started: true


    ## Message ##
        Received a StartMethodMessage From Agent2
        Agent2 has asked to start Method4


    ## Message ##
        Received a AgentToAgentMessage From Agent1
        Agent1 has sent a message to Agent2 with content: {"Load":1,"Running":{"VisibleToAgents":["Agent1"],"NodeName":"Method2","Quality":"(20.0,0.5),(27.0,0.25),(13.0,0.25)","NodeType":"Method","Duration":"(12.0,0.5),(4.0,0.25),(7.0,0.25)","AgentName":"Agent1","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 8 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 8 has occurred


    ## Message ##
        Sending a ConfirmMethodStartMessage to Agent2
        Method4 has started: true


    ## Message ##
        Received a AgentToAgentMessage From Agent1
        Agent1 has sent a message to Agent2 with content: {"Load":1,"Running":{"VisibleToAgents":["Agent1"],"NodeName":"Method2","Quality":"(20.0,0.5),(27.0,0.25),(13.0,0.25)","NodeType":"Method","Duration":"(12.0,0.5),(4.0,0.25),(7.0,0.25)","AgentName":"Agent1","SubTasks":[]},"Compleated":""}


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":2,"Running":{"VisibleToAgents":["Agent2"],"NodeName":"Method4","Quality":"(40.0,0.5),(47.0,0.25),(43.0,0.25)","NodeType":"Method","Duration":"(2.0,0.5),(3.0,0.25),(5.0,0.25)","AgentName":"Agent2","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 9 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 9 has occurred


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":2,"Running":{"VisibleToAgents":["Agent2"],"NodeName":"Method4","Quality":"(40.0,0.5),(47.0,0.25),(43.0,0.25)","NodeType":"Method","Duration":"(2.0,0.5),(3.0,0.25),(5.0,0.25)","AgentName":"Agent2","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 10 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 10 has occurred


    ## Method ##
        Name: Method4
        Completed By : Agent2
        With Quality : 40.0
        With Duration: 2


    ## Message ##
        Sending a NotifyMethodCompletedMessage to Agent2
        Agent2 has completed Method4


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":1,"Running":"","Compleated":"Method4"}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 11 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 11 has occurred


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":1,"Running":"","Compleated":"Method4"}


    ## Message ##
        Received a StartMethodMessage From Agent2
        Agent2 has asked to start Method3


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 12 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 12 has occurred


    ## Message ##
        Sending a ConfirmMethodStartMessage to Agent2
        Method3 has started: true


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":1,"Running":{"VisibleToAgents":["Agent2"],"NodeName":"Method3","Quality":"(20.0,0.5),(27.0,0.25),(13.0,0.25)","NodeType":"Method","Duration":"(2.0,0.5),(3.0,0.25),(5.0,0.25)","AgentName":"Agent2","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 13 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 13 has occurred


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":1,"Running":{"VisibleToAgents":["Agent2"],"NodeName":"Method3","Quality":"(20.0,0.5),(27.0,0.25),(13.0,0.25)","NodeType":"Method","Duration":"(2.0,0.5),(3.0,0.25),(5.0,0.25)","AgentName":"Agent2","SubTasks":[]},"Compleated":""}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 14 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 14 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 15 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 15 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 16 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 16 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 17 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 17 has occurred


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 20.0
        With Duration: 5


    ## Message ##
        Sending a NotifyMethodCompletedMessage to Agent2
        Agent2 has completed Method3


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":0,"Running":"","Compleated":"Method3"}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 18 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 18 has occurred


    ## Message ##
        Received a AgentToAgentMessage From Agent2
        Agent2 has sent a message to Agent1 with content: {"Load":0,"Running":"","Compleated":"Method3"}


    ## Message ##
        Sending a NextTickMessage to Agent1
        Tick 19 has occurred


    ## Message ##
        Sending a NextTickMessage to Agent2
        Tick 19 has occurred


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 12


    ## Message ##
        Sending a NotifyMethodCompletedMessage to Agent1
        Agent1 has completed Method2


    ## Message ##
        Sending a EndSimulationMessage to Agent1
        The Simulation is Over


    ## Message ##
        Sending a EndSimulationMessage to Agent2
        The Simulation is Over


    ## Agent ##
        Name Agent1
        Sent: 7
        Received: 34
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 10
        Received: 35
        Completed: Method4, Method3