####### Ordered Logger #######


Final Duration: 19.0
Final Quality: 47.0
Seed: -6985069397762151892

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 27.0
        With Duration: 3


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 12

##### Relationship Results #####


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method3

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 7
        Received: 32
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 5
        Received: 31
        Completed: Method3