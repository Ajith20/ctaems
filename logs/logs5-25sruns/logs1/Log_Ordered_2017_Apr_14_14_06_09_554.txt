####### Ordered Logger #######


Final Duration: 23.0
Final Quality: 43.0
Seed: 6499656665000076141

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method4
        Completed By : Agent2
        With Quality : 60.0
        With Duration: 4


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 30.0
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method1
        Target : CTask2

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 8
        Received: 37
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 11
        Received: 42
        Completed: Method4, Method3