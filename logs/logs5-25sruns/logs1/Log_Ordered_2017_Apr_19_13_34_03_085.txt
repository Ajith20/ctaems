####### Ordered Logger #######


Final Duration: 11.0
Final Quality: 26.0
Seed: -5200161481709677476

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method3

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 7
        Received: 24
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 5
        Received: 23
        Completed: Method3