####### Ordered Logger #######


Final Duration: 14.0
Final Quality: 40.0
Seed: 5452650735787296894

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 27.0
        With Duration: 5


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 7

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method1
        Target : Method3

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 7
        Received: 27
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 4
        Received: 25
        Completed: Method3