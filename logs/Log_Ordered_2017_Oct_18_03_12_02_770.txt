####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 4827814627

##### Method Results #####


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 12
        End Time: 19


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5
        End Time: 28


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 48


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 100


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 104


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 4
        End Time: 110


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5
        End Time: 117


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.3999999999999999
        With Duration: 7
        End Time: 126


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 138


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6
        End Time: 150


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 155


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 162


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 169


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 6
        End Time: 177


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 4
        End Time: 183


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 204


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 4
        End Time: 210


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 0.5
        With Duration: 3
        End Time: 215


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 218


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 221


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18
        End Time: 241


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3
        End Time: 246


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 18.2
        With Duration: 1
        End Time: 249


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 258


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 265


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 41.4
        With Duration: 1
        End Time: 268


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 41.666666666666664
        With Duration: 7
        End Time: 277


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 283

##### Relationship Results #####


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 906
        Completed: Method6, Method20, Method21, Method12, Method1, Method2, Method5, Method23, Method18, Method7, Method30, Method9, Method25, Method16, Method26, Method27, Method22, Method15, Method19, Method17, Method13, Method11, Method24, Method14, Method29, Method10, Method8, Method28