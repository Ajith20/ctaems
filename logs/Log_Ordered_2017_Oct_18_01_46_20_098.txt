####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 4459863880

##### Method Results #####


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13
        End Time: 24


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5
        End Time: 31


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 39


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 19
        End Time: 60


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5
        End Time: 68


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 9
        End Time: 81


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 4.909090909090908
        With Duration: 0
        End Time: 83


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 4
        End Time: 89


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 17
        End Time: 109


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 112


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.235294117647059
        With Duration: 8
        End Time: 122


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 129


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 135


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6
        End Time: 143


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 148


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 162


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 7
        End Time: 172


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 179


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.3999999999999999
        With Duration: 9
        End Time: 190


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12
        End Time: 204


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 208


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 0.7058823529411764
        With Duration: 2
        End Time: 213


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 4
        End Time: 220


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9
        End Time: 233


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18
        End Time: 253


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 8
        End Time: 263


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 274

##### Relationship Results #####


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 905
        Completed: Method21, Method9, Method12, Method13, Method5, Method3, Method30, Method22, Method8, Method19, Method20, Method16, Method2, Method24, Method1, Method10, Method29, Method25, Method23, Method6, Method26, Method15, Method28, Method18, Method27, Method17, Method7