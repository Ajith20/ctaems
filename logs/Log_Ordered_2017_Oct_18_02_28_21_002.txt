####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6008483412

##### Method Results #####


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 10
        End Time: 16


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3
        End Time: 21


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 28


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 39


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 7
        End Time: 49


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 9
        End Time: 60


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 6
        End Time: 68


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.3999999999999999
        With Duration: 9
        End Time: 79


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 37.86153846153846
        With Duration: 3
        End Time: 84


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4
        End Time: 90


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 97


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 100


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 7
        End Time: 112


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 7
        End Time: 121


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 19
        End Time: 142


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 3
        End Time: 147


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 155


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 1
        End Time: 158


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 165


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 12
        End Time: 179


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 196


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 41.666666666666664
        With Duration: 7
        End Time: 205


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.111111111111112
        With Duration: 10
        End Time: 217


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15
        End Time: 234


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.7
        With Duration: 0
        End Time: 238


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.235294117647059
        With Duration: 11
        End Time: 252

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 904
        Completed: Method18, Method17, Method9, Method7, Method29, Method24, Method5, Method23, Method10, Method16, Method22, Method19, Method2, Method28, Method13, Method15, Method26, Method1, Method25, Method6, Method21, Method8, Method12, Method27, Method30, Method20