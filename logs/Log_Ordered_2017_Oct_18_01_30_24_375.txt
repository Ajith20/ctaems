####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 5387830612

##### Method Results #####


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 8


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 11
        End Time: 21


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 14
        End Time: 37


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 40


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 6
        End Time: 48


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 52


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 15
        End Time: 70


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 15.6
        With Duration: 5
        End Time: 78


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 4
        End Time: 84


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 32.92307692307692
        With Duration: 3
        End Time: 89


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17
        End Time: 109


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5
        End Time: 116


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 7
        End Time: 127


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13
        End Time: 142


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 6
        End Time: 152


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 155


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3
        End Time: 160


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7
        End Time: 170


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.19999999999999996
        With Duration: 9
        End Time: 183


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 187


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.666666666666667
        With Duration: 10
        End Time: 199


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 19
        End Time: 220


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2
        End Time: 224


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12
        End Time: 238


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 251


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 257


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 268

##### Relationship Results #####


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 905
        Completed: Method28, Method18, Method8, Method17, Method16, Method26, Method6, Method24, Method29, Method10, Method27, Method20, Method9, Method21, Method5, Method15, Method30, Method22, Method23, Method19, Method12, Method13, Method1, Method2, Method11, Method25, Method7