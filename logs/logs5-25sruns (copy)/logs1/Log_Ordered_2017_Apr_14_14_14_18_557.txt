####### Ordered Logger #######


Final Duration: 19.0
Final Quality: 33.5
Seed: -8122356124515880809

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 13.5
        With Duration: 9


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 12

##### Relationship Results #####


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method1
        Target : Method3

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 7
        Received: 32
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 6
        Received: 32
        Completed: Method3