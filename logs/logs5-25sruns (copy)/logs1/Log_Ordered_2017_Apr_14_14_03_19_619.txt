####### Ordered Logger #######


Final Duration: 22.0
Final Quality: 33.0
Seed: -2841100379565612314

##### Method Results #####


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent2
        With Quality : 13.0
        With Duration: 5


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 12


    ## Method ##
        Name: Method4
        Completed By : Agent2
        With Quality : 60.0
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method1
        Target : CTask2

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 8
        Received: 36
        Completed: Method1, Method2

    ## Agent ##
        Name Agent2
        Sent: 9
        Received: 39
        Completed: Method3, Method4