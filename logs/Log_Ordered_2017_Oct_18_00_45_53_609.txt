####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6682748867

##### Method Results #####


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5
        End Time: 19


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 25


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 34


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 40


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 51


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 62


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 65


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.666666666666667
        With Duration: 11
        End Time: 78


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 18
        End Time: 98

##### Relationship Results #####


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 41
        Received: 858
        Completed: Method16, Method30, Method20, Method28, Method10, Method5, Method17, Method12, Method21