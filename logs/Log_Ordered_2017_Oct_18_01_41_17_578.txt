####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 4941696390

##### Method Results #####


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 12
        End Time: 23


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 9
        End Time: 37


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 1
        End Time: 40


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 43


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 47


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 54


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 8.5
        With Duration: 0
        End Time: 56


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 63


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9
        End Time: 74


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12
        End Time: 88


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 97


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 104


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 7
        End Time: 120


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 131


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 138


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 17
        End Time: 157


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 163


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 170


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 5
        End Time: 177


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 11
        End Time: 190


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 45.0
        With Duration: 3
        End Time: 195


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.8529411764705883
        With Duration: 1
        End Time: 198


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15
        End Time: 215


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 235


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 4
        End Time: 241


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 10
        End Time: 255


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 10
        End Time: 276


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 281


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 288

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 907
        Completed: Method13, Method24, Method1, Method17, Method28, Method23, Method30, Method20, Method7, Method2, Method9, Method14, Method5, Method3, Method25, Method27, Method11, Method29, Method22, Method10, Method8, Method15, Method6, Method21, Method26, Method18, Method12, Method19, Method16