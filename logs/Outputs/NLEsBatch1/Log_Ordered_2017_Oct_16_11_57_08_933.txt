####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6644503090

##### Method Results #####


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12
        End Time: 62


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6
        End Time: 70


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 84


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 43.05555555555556
        With Duration: 5
        End Time: 94


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 106


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 113


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 119


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 123


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 133


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 7
        End Time: 151


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7
        End Time: 180


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 3
        End Time: 195


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17
        End Time: 219


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15
        End Time: 237


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.352941176470589
        With Duration: 8
        End Time: 249


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 271


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 3
        End Time: 298


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 314


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1
        End Time: 328


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 5.555555555555555
        With Duration: 8
        End Time: 342


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 36.0
        With Duration: 2
        End Time: 350


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12
        End Time: 369


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 381


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1
        End Time: 399


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 0.7058823529411764
        With Duration: 2
        End Time: 411


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 12
        End Time: 428

##### Relationship Results #####


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 904
        Completed: Method6, Method24, Method7, Method8, Method29, Method23, Method28, Method26, Method5, Method22, Method9, Method30, Method27, Method21, Method20, Method16, Method17, Method18, Method1, Method12, Method10, Method2, Method11, Method19, Method15, Method13