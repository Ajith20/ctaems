####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 9253859906

##### Method Results #####


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 7
        End Time: 37


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9
        End Time: 50


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9
        End Time: 65


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 4
        End Time: 73


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 84


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 6
        End Time: 92


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 97


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1
        End Time: 110


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 121


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 10
        End Time: 142


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 13
        End Time: 169


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 175


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 191


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 197


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18
        End Time: 226


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 4
        End Time: 241


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 265


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5
        End Time: 285


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 298


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 324


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.352941176470589
        With Duration: 12
        End Time: 343


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 6
        End Time: 353


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 361


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.47272727272727266
        With Duration: 8
        End Time: 390


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 37.55555555555556
        With Duration: 4
        End Time: 401


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 7
        End Time: 411

##### Relationship Results #####


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 904
        Completed: Method28, Method6, Method10, Method22, Method14, Method24, Method15, Method19, Method16, Method18, Method21, Method30, Method12, Method26, Method13, Method2, Method27, Method5, Method29, Method17, Method20, Method7, Method1, Method23, Method8, Method9