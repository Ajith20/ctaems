####### Ordered Logger #######


Final Duration: 1000.0
Final Quality: 0.0
Seed: 5294934750

##### Method Results #####


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 6


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 5


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.5
        With Duration: 3


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 1


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 65.0
        With Duration: 13


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 18


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.5
        With Duration: 0


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 2091
        Received: 12546
        Completed: Method18, Method24, Method27, Method6, Method15, Method21, Method17, Method20, Method9, Method30, Method29, Method13, Method1, Method2, Method8, Method22, Method11, Method3, Method10, Method28, Method23, Method7, Method16, Method26, Method4, Method12, Method5, Method3, Method12, Method15, Method13, Method24, Method20, Method21, Method10, Method30, Method17, Method23, Method2, Method5, Method27, Method7, Method1, Method4, Method26, Method22, Method14, Method11, Method18, Method9, Method16, Method29, Method8, Method28, Method15, Method10, Method5, Method17, Method22, Method28, Method26, Method29, Method12, Method9, Method7, Method1, Method24, Method18, Method30, Method3, Method21, Method19, Method23, Method20, Method16, Method27, Method2, Method6, Method8, Method13, Method12, Method6, Method23, Method24, Method28, Method18, Method7, Method15, Method1, Method2, Method30, Method4, Method16, Method10, Method26, Method13, Method17, Method21, Method20, Method5, Method27, Method14, Method9, Method8, Method29, Method22, Method20, Method9, Method21, Method1, Method3, Method12, Method28, Method17, Method7, Method10, Method15, Method30, Method29, Method23, Method16, Method26, Method6, Method18, Method24, Method4, Method8, Method22, Method2, Method13, Method25, Method5, Method27, Method12, Method9, Method7, Method15, Method28, Method27, Method14, Method1, Method18, Method29, Method26, Method4, Method20, Method3, Method23, Method11, Method2, Method8, Method5, Method17, Method25, Method10, Method21, Method13, Method24, Method22, Method16, Method30, Method9, Method13, Method22, Method8, Method17, Method6, Method3, Method15, Method21, Method16, Method29, Method1, Method30, Method26, Method7, Method24, Method28, Method20, Method10, Method18, Method25, Method19, Method27, Method12, Method5, Method2, Method11, Method23, Method23, Method21, Method28, Method12, Method18, Method15, Method22, Method8, Method7, Method30, Method20, Method17, Method26, Method29, Method27, Method5, Method13, Method9, Method6, Method24, Method11, Method16, Method2, Method10, Method1, Method3, Method24, Method15, Method12, Method26, Method10, Method1, Method21, Method20, Method27, Method16, Method14, Method6, Method8, Method5, Method2, Method18, Method13, Method22, Method29, Method4, Method17, Method11, Method23, Method28, Method30, Method9, Method7, Method26, Method12, Method10, Method29, Method27, Method22, Method2, Method18, Method28, Method30, Method3, Method6, Method23, Method24, Method7, Method20, Method5, Method1, Method19, Method9, Method11, Method17, Method8, Method13, Method15, Method16, Method21