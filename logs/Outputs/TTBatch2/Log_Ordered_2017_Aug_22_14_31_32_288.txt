####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 9548503422

##### Method Results #####


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 12


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 14


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 4


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 5


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 3


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 7


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 7


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 11


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 7


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 7022
        Received: 21150
        Completed: Method21, Method25, Method7, Method22, Method6, Method17, Method27, Method5, Method19, Method30, Method3, Method20, Method18, Method24, Method29, Method2, Method10, Method9, Method23, Method13, Method1, Method16, Method8, Method10, Method23, Method5, Method9, Method30, Method1, Method29, Method3, Method16, Method17, Method22, Method25, Method2, Method6, Method4, Method20, Method7, Method21, Method26, Method19, Method8, Method18, Method24, Method1, Method3, Method25, Method21, Method20, Method9, Method22, Method23, Method18, Method4, Method19, Method16, Method29, Method8, Method24, Method2, Method30, Method7, Method17, Method10, Method5, Method17, Method2, Method3, Method19, Method7, Method22, Method16, Method9, Method18, Method30, Method21, Method5, Method1, Method23, Method24, Method6, Method8, Method25, Method4, Method20, Method10, Method12, Method9, Method1, Method4, Method3, Method8, Method10, Method24, Method19, Method13, Method26, Method22, Method25, Method21, Method27, Method5, Method30, Method16, Method7, Method29, Method18, Method2, Method20, Method17, Method23, Method27, Method18, Method22, Method6, Method23, Method24, Method25, Method16, Method20, Method17, Method7, Method5, Method8, Method9, Method10, Method13, Method12, Method1, Method30, Method26, Method19, Method3, Method4, Method2, Method21, Method6, Method17, Method8, Method29, Method1, Method4, Method24, Method16, Method9, Method30, Method3, Method23, Method26, Method25, Method22, Method18, Method20, Method19, Method2, Method13, Method21, Method5, Method7, Method10, Method25, Method17, Method6, Method20, Method16, Method18, Method3, Method7, Method19, Method24, Method2, Method9, Method1, Method5, Method8, Method13, Method10, Method22, Method21, Method23, Method3, Method25, Method5, Method27, Method30, Method6, Method18, Method9, Method7, Method2, Method16, Method20, Method21, Method24, Method1, Method22, Method26, Method8, Method19, Method23, Method10, Method14, Method29, Method4, Method17, Method16, Method25, Method6, Method17, Method8, Method23, Method24, Method2, Method1, Method5, Method19, Method21, Method9, Method29, Method27, Method30, Method3, Method7, Method18, Method22, Method10, Method20