####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 4179017410

##### Method Results #####


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 4


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 7


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 6


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 14


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 10


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 8


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 13


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 6


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 9


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 6


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 4


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 12


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 12307
        Received: 35928
        Completed: Method5, Method2, Method17, Method1, Method25, Method3, Method10, Method6, Method20, Method19, Method24, Method22, Method9, Method27, Method30, Method23, Method26, Method4, Method8, Method18, Method29, Method21, Method7, Method11, Method16, Method8, Method22, Method23, Method17, Method9, Method7, Method21, Method25, Method5, Method24, Method27, Method19, Method3, Method18, Method1, Method16, Method10, Method4, Method13, Method20, Method2, Method9, Method26, Method29, Method19, Method28, Method3, Method1, Method30, Method21, Method10, Method18, Method22, Method20, Method24, Method8, Method25, Method23, Method4, Method2, Method17, Method5, Method7, Method27, Method16, Method9, Method27, Method18, Method21, Method7, Method5, Method22, Method30, Method2, Method19, Method25, Method3, Method29, Method26, Method10, Method28, Method24, Method6, Method16, Method17, Method20, Method1, Method23, Method8, Method27, Method20, Method18, Method29, Method2, Method21, Method16, Method17, Method23, Method25, Method30, Method1, Method7, Method4, Method10, Method3, Method5, Method22, Method24, Method8, Method9, Method19, Method7, Method23, Method19, Method9, Method8, Method10, Method5, Method21, Method18, Method20, Method25, Method2, Method27, Method16, Method26, Method6, Method3, Method14, Method28, Method11, Method1, Method30, Method29, Method24, Method15, Method22, Method17, Method27, Method21, Method30, Method5, Method20, Method6, Method9, Method10, Method7, Method16, Method29, Method17, Method2, Method3, Method8, Method15, Method18, Method12, Method22, Method24, Method23, Method19, Method25, Method1, Method8, Method16, Method5, Method30, Method6, Method22, Method10, Method2, Method9, Method23, Method29, Method7, Method25, Method15, Method3, Method12, Method19, Method11, Method17, Method1, Method27, Method13, Method4, Method18, Method20, Method21, Method24, Method29, Method22, Method3, Method24, Method20, Method2, Method19, Method17, Method25, Method5, Method9, Method6, Method13, Method27, Method18, Method30, Method8, Method23, Method10, Method7, Method1, Method26, Method16, Method21, Method6, Method23, Method22, Method5, Method7, Method18, Method2, Method3, Method17, Method25, Method1, Method29, Method24, Method19, Method9, Method21, Method10, Method8, Method20, Method4, Method14, Method27, Method30, Method9, Method20, Method25, Method24, Method2, Method10, Method1, Method21, Method16, Method30, Method22, Method8, Method13, Method19, Method29, Method6, Method23, Method18, Method27, Method5, Method3, Method26, Method17, Method7, Method4, Method22, Method29, Method5, Method7, Method17, Method8, Method3, Method25, Method23, Method21, Method1, Method4, Method24, Method13, Method30, Method18, Method2, Method19, Method20, Method27, Method16, Method10, Method9, Method25, Method21, Method8, Method2, Method18, Method16, Method17, Method6, Method29, Method3, Method23, Method30, Method24, Method19, Method22, Method1, Method10, Method9, Method5, Method4, Method20, Method7, Method2, Method20, Method6, Method17, Method5, Method7, Method16, Method29, Method1, Method21, Method23, Method4, Method18, Method10, Method27, Method9, Method22, Method19, Method25, Method3, Method24, Method8, Method25, Method3, Method7, Method16, Method19, Method21, Method30, Method6, Method1, Method27, Method8, Method20, Method23, Method22, Method29, Method5, Method18, Method9, Method10, Method26, Method17, Method14, Method24, Method2, Method29, Method20, Method3, Method1, Method24, Method22, Method4, Method27, Method5, Method9, Method26, Method16, Method23, Method8, Method17, Method2, Method10, Method28, Method19, Method30, Method7, Method25, Method18, Method21, Method16, Method22, Method19, Method7, Method5, Method27, Method8, Method30, Method10, Method9, Method18, Method20, Method3, Method21, Method24, Method1, Method6, Method15, Method12, Method23, Method2, Method13, Method17, Method4, Method11, Method25