####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 4461924282

##### Method Results #####


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 11


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 7


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 11


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 7


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 17277
        Received: 55725
        Completed: Method16, Method25, Method30, Method14, Method15, Method11, Method9, Method10, Method24, Method2, Method29, Method3, Method6, Method26, Method28, Method1, Method18, Method20, Method5, Method12, Method7, Method23, Method22, Method21, Method13, Method27, Method7, Method15, Method6, Method1, Method14, Method9, Method25, Method18, Method30, Method11, Method22, Method4, Method17, Method24, Method23, Method12, Method2, Method21, Method19, Method27, Method20, Method3, Method26, Method10, Method29, Method5, Method8, Method7, Method18, Method12, Method10, Method11, Method17, Method22, Method2, Method27, Method24, Method9, Method21, Method19, Method29, Method23, Method30, Method5, Method3, Method1, Method15, Method26, Method6, Method16, Method13, Method28, Method20, Method25, Method22, Method27, Method3, Method11, Method29, Method1, Method6, Method16, Method14, Method10, Method2, Method8, Method18, Method19, Method5, Method12, Method23, Method30, Method24, Method20, Method25, Method7, Method15, Method13, Method21, Method9, Method4, Method16, Method27, Method10, Method30, Method8, Method23, Method17, Method24, Method21, Method5, Method29, Method9, Method25, Method20, Method11, Method22, Method1, Method3, Method7, Method15, Method19, Method14, Method4, Method18, Method13, Method12, Method2, Method18, Method6, Method29, Method8, Method3, Method12, Method25, Method11, Method20, Method7, Method5, Method21, Method23, Method1, Method16, Method10, Method24, Method30, Method22, Method2, Method9, Method27, Method15, Method16, Method23, Method10, Method30, Method7, Method15, Method6, Method20, Method27, Method1, Method21, Method12, Method17, Method24, Method14, Method11, Method29, Method18, Method25, Method3, Method22, Method5, Method2, Method13, Method9, Method24, Method3, Method25, Method15, Method22, Method30, Method5, Method1, Method14, Method20, Method9, Method21, Method23, Method26, Method17, Method12, Method7, Method11, Method6, Method2, Method4, Method27, Method29, Method18, Method13, Method10, Method20, Method24, Method1, Method5, Method29, Method12, Method21, Method25, Method9, Method11, Method23, Method27, Method6, Method30, Method8, Method13, Method15, Method3, Method16, Method22, Method14, Method10, Method7, Method4, Method18, Method2, Method9, Method2, Method25, Method30, Method15, Method22, Method27, Method7, Method5, Method21, Method11, Method14, Method10, Method3, Method23, Method19, Method24, Method12, Method20, Method29, Method6, Method26, Method1, Method18, Method24, Method27, Method10, Method11, Method23, Method18, Method15, Method25, Method13, Method16, Method30, Method22, Method7, Method12, Method17, Method9, Method1, Method5, Method3, Method2, Method21, Method20, Method6, Method29, Method23, Method21, Method20, Method12, Method24, Method13, Method14, Method25, Method29, Method15, Method3, Method27, Method11, Method10, Method22, Method6, Method19, Method5, Method7, Method18, Method2, Method1, Method30, Method9, Method12, Method29, Method2, Method11, Method5, Method23, Method27, Method21, Method16, Method6, Method18, Method10, Method30, Method9, Method7, Method1, Method3, Method25, Method22, Method24, Method8, Method20, Method15, Method19, Method1, Method15, Method7, Method9, Method29, Method11, Method16, Method12, Method5, Method14, Method3, Method6, Method30, Method10, Method20, Method24, Method22, Method4, Method18, Method27, Method21, Method19, Method26, Method23, Method25, Method2, Method21, Method22, Method9, Method15, Method3, Method7, Method17, Method18, Method25, Method5, Method23, Method6, Method16, Method19, Method10, Method1, Method24, Method2, Method12, Method11, Method29, Method27, Method30, Method20, Method18, Method27, Method20, Method11, Method1, Method9, Method29, Method4, Method5, Method30, Method24, Method10, Method15, Method13, Method12, Method2, Method22, Method3, Method23, Method7, Method16, Method25, Method21, Method26, Method24, Method22, Method14, Method29, Method19, Method6, Method23, Method8, Method1, Method4, Method21, Method18, Method15, Method27, Method7, Method13, Method5, Method3, Method10, Method20, Method12, Method2, Method11, Method30, Method25, Method9, Method9, Method5, Method21, Method23, Method11, Method30, Method16, Method15, Method10, Method12, Method14, Method6, Method18, Method3, Method25, Method20, Method22, Method27, Method26, Method29, Method1, Method24, Method2, Method7, Method18, Method9, Method22, Method30, Method20, Method2, Method24, Method3, Method16, Method15, Method13, Method7, Method12, Method26, Method10, Method29, Method1, Method6, Method11, Method25, Method5, Method4, Method27, Method23, Method21, Method2, Method20, Method6, Method10, Method27, Method16, Method24, Method17, Method3, Method8, Method29, Method22, Method12, Method1, Method25, Method5, Method4, Method11, Method30, Method18, Method13, Method19, Method15, Method7, Method21, Method9, Method23, Method14, Method2, Method8, Method6, Method21, Method15, Method27, Method1, Method11, Method22, Method30, Method5, Method12, Method18, Method4, Method17, Method20, Method29, Method7, Method3, Method25, Method24, Method9, Method23, Method10, Method22, Method23, Method15, Method1, Method24, Method17, Method5, Method14, Method20, Method13, Method27, Method11, Method2, Method12, Method19, Method21, Method9, Method29, Method10, Method26, Method25, Method30, Method18, Method7, Method3, Method6, Method1, Method23, Method22, Method9, Method11, Method26, Method15, Method29, Method14, Method25, Method20, Method18, Method24, Method19, Method12, Method30, Method10, Method27, Method3, Method21, Method6, Method7, Method2, Method5, Method4, Method5, Method8, Method14, Method2, Method22, Method12, Method1, Method7, Method23, Method11, Method15, Method21, Method29, Method25, Method24, Method19, Method20, Method9, Method30, Method4, Method13, Method3, Method18, Method27, Method10, Method18, Method1, Method3, Method9, Method10, Method12, Method30, Method7, Method27, Method6, Method16, Method4, Method2, Method23, Method26, Method15, Method5, Method20, Method25, Method29, Method21, Method22, Method11, Method24