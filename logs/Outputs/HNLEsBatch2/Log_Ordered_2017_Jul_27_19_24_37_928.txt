####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 4706445887

##### Method Results #####


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 11


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 7


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 9


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 18


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 7


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 9


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 7


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 18


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 3889
        Received: 14659
        Completed: Method16, Method25, Method30, Method14, Method15, Method11, Method9, Method10, Method24, Method2, Method29, Method3, Method6, Method26, Method28, Method1, Method18, Method20, Method5, Method12, Method7, Method23, Method22, Method21, Method13, Method27, Method7, Method15, Method6, Method1, Method14, Method9, Method25, Method18, Method30, Method11, Method22, Method4, Method17, Method24, Method23, Method12, Method2, Method21, Method19, Method27, Method20, Method3, Method26, Method10, Method29, Method5, Method8, Method7, Method18, Method12, Method10, Method11, Method17, Method22, Method2, Method27, Method24, Method9, Method21, Method19, Method29, Method23, Method30, Method5, Method3, Method1, Method15, Method26, Method6, Method16, Method13, Method28, Method20, Method25, Method22, Method27, Method3, Method11, Method29, Method1, Method6, Method16, Method14, Method10, Method2, Method8, Method18, Method19, Method5, Method12, Method23, Method30, Method24, Method20, Method25, Method7, Method15, Method13, Method21, Method9, Method4, Method16, Method27, Method10, Method30, Method8, Method23, Method17, Method24, Method21, Method5, Method29, Method9, Method25, Method20, Method11, Method22, Method1, Method3, Method7, Method15, Method19, Method14, Method4, Method18, Method13, Method12, Method2, Method18, Method6, Method29, Method8, Method3, Method12, Method25, Method11, Method20, Method7, Method5, Method21, Method23, Method1, Method16, Method10, Method24, Method30, Method22, Method2, Method9, Method27, Method15, Method16, Method23, Method10, Method30, Method7, Method15, Method6, Method20, Method27, Method1, Method21, Method12, Method17, Method24, Method14, Method11, Method29, Method18, Method25, Method3, Method22, Method5, Method2, Method13, Method9