####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 9323751109

##### Method Results #####


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 7
        End Time: 19


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 26


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 10
        End Time: 38


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4
        End Time: 44


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 53


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 8
        End Time: 63


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 67


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 71


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.5999999999999999
        With Duration: 10
        End Time: 83


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 15
        End Time: 100


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 103


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 17
        End Time: 122


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4
        End Time: 131


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 138


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 1
        End Time: 141


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 19
        End Time: 162


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13
        End Time: 177


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 10
        End Time: 189


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 201


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 9
        End Time: 213


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5
        End Time: 220


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 227


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 3
        End Time: 232


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18
        End Time: 252


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 263


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 7
        End Time: 272


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 278


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 285

##### Relationship Results #####


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 906
        Completed: Method22, Method9, Method10, Method30, Method5, Method17, Method28, Method15, Method23, Method6, Method26, Method8, Method20, Method19, Method1, Method13, Method21, Method12, Method18, Method7, Method14, Method25, Method24, Method27, Method3, Method2, Method29, Method16