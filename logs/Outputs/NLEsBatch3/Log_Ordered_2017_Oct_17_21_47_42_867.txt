####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 9862669080

##### Method Results #####


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12
        End Time: 26


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 32


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 4
        End Time: 38


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 60


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 15
        End Time: 77


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 5
        End Time: 90


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 97


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 2
        End Time: 105


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 116


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 129


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 18
        End Time: 163


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 183


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 9
        End Time: 209


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 260


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.4117647058823528
        With Duration: 2
        End Time: 329


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9
        End Time: 452


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1
        End Time: 589

##### Relationship Results #####


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 49
        Received: 879
        Completed: Method2, Method14, Method22, Method27, Method6, Method5, Method12, Method28, Method10, Method30, Method13, Method18, Method3, Method24, Method15, Method7, Method1