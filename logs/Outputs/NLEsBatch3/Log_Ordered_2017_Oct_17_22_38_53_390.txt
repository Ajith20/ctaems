####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 7683642665

##### Method Results #####


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 16


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 27


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 35


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 7
        End Time: 45


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 56


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 66


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 41.666666666666664
        With Duration: 5
        End Time: 76


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 41.4
        With Duration: 1
        End Time: 79


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 6
        End Time: 87


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.23636363636363633
        With Duration: 10
        End Time: 100


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 8
        End Time: 110


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 114


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 15
        End Time: 133


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 141


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 15
        End Time: 158


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 3.2
        With Duration: 9
        End Time: 169


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 12
        End Time: 183


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.7
        With Duration: 0
        End Time: 188


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 2
        End Time: 194


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 5
        End Time: 203


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12
        End Time: 220


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5
        End Time: 227


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1
        End Time: 230


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9
        End Time: 241


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 244


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 248

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 905
        Completed: Method18, Method3, Method24, Method5, Method7, Method29, Method8, Method10, Method12, Method23, Method17, Method15, Method27, Method16, Method21, Method20, Method13, Method30, Method28, Method22, Method2, Method9, Method1, Method6, Method26, Method4