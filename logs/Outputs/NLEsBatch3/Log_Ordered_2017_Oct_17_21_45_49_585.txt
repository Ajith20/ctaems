####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 1123719295

##### Method Results #####


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 1
        End Time: 7


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 27


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 33


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 44


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 48


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 15
        End Time: 65


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 72


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 36.11111111111111
        With Duration: 7
        End Time: 81


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 88


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 5
        End Time: 95


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 102


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 2
        End Time: 106


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5
        End Time: 113


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5
        End Time: 120


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9
        End Time: 132


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 145


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2
        End Time: 174


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 209


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 16.900000000000002
        With Duration: 3
        End Time: 240


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15
        End Time: 259


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.19999999999999996
        With Duration: 7
        End Time: 268


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 277


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 19
        End Time: 300


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 37.800000000000004
        With Duration: 2
        End Time: 304


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.111111111111112
        With Duration: 11
        End Time: 317


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2
        End Time: 323


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3
        End Time: 328

##### Relationship Results #####


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 905
        Completed: Method17, Method27, Method20, Method7, Method26, Method6, Method16, Method8, Method9, Method22, Method14, Method28, Method5, Method29, Method18, Method15, Method30, Method19, Method24, Method21, Method23, Method2, Method13, Method10, Method12, Method1, Method11