####### Ordered Logger #######


Final Duration: 1000.0
Final Quality: 0.0
Seed: 3416409773

##### Method Results #####


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 8


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 10


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 12


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 7


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 11


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 2


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 7


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.5
        With Duration: 0


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 6


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.4000000000000004
        With Duration: 4


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 75.0
        With Duration: 16


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2

##### Relationship Results #####


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 1399
        Received: 5581
        Completed: Method27, Method28, Method6, Method2, Method11, Method13, Method26, Method20, Method15, Method9, Method17, Method21, Method3, Method16, Method18, Method10, Method24, Method12, Method30, Method8, Method7, Method23, Method14, Method1, Method25, Method22, Method29, Method5, Method21, Method20, Method9, Method26, Method25, Method8, Method18, Method15, Method29, Method3, Method24, Method2, Method5, Method10, Method7, Method22, Method12, Method28, Method17, Method30, Method23, Method13, Method27, Method6, Method16, Method11, Method1, Method6, Method15, Method10, Method30, Method20, Method9, Method27, Method16, Method8, Method1, Method22, Method2, Method23, Method21, Method28, Method5, Method17, Method13, Method24, Method29, Method25, Method11, Method7, Method18, Method12, Method19, Method26, Method3, Method22, Method2, Method12, Method10, Method13, Method19, Method20, Method21, Method30, Method29, Method18, Method6, Method17, Method28, Method7, Method24, Method15, Method5, Method9, Method23, Method26, Method8, Method27, Method1, Method16, Method4