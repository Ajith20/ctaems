####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 7830004884

##### Method Results #####


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 3
        End Time: 15


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12
        End Time: 29


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 31


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17
        End Time: 50


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9
        End Time: 61


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 7
        End Time: 70


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 72


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4
        End Time: 78


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 17
        End Time: 97


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 99


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 107


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 116


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 127


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4
        End Time: 133


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 135


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5
        End Time: 142


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 10
        End Time: 154


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13
        End Time: 169


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 7
        End Time: 179


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 188


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 4
        End Time: 194


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9
        End Time: 205


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 207


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 212


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 3
        End Time: 217


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 219


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1
        End Time: 222


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 226


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 233


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12
        End Time: 247

##### Relationship Results #####


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 240
        Received: 3594
        Completed: Method16, Method18, Method24, Method5, Method10, Method30, Method9, Method1, Method20, Method3, Method11, Method26, Method23, Method25, Method22, Method8, Method21, Method19, Method29, Method7, Method2, Method6, Method27, Method12, Method15, Method13, Method4, Method29, Method24, Method6, Method25, Method30, Method8, Method9, Method23, Method7, Method12, Method22, Method21, Method5, Method27, Method26, Method20, Method10, Method2, Method14, Method18, Method11, Method13, Method1, Method3, Method19, Method28, Method15, Method27, Method16, Method1, Method5, Method7, Method30, Method12, Method14, Method22, Method4, Method3, Method25, Method9, Method20, Method18, Method2, Method11, Method23, Method8, Method15, Method10, Method21, Method17, Method24, Method29, Method11, Method2, Method27, Method3, Method5, Method29, Method8, Method14, Method25, Method7, Method24, Method20, Method10, Method21, Method9, Method12, Method22, Method18, Method1, Method30, Method19, Method15, Method23, Method6