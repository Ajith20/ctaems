####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 2010603911

##### Method Results #####


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 4
        End Time: 34


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4
        End Time: 40


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 8
        End Time: 50


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9
        End Time: 61


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 63


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 72


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 9
        End Time: 83


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7
        End Time: 92


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 94


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 20
        End Time: 119


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 128


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 7
        End Time: 156


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 163


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 167


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 169


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 171


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6
        End Time: 179


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 186


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 9
        End Time: 197


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 199


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 11
        End Time: 212


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 9
        End Time: 223


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 12
        End Time: 237


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 247


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 251


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 4
        End Time: 260


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 15
        End Time: 277


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 297


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5
        End Time: 305


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3
        End Time: 310

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 780
        Received: 11678
        Completed: Method16, Method18, Method24, Method5, Method10, Method30, Method9, Method1, Method20, Method3, Method11, Method26, Method23, Method25, Method22, Method8, Method21, Method19, Method29, Method7, Method2, Method6, Method27, Method12, Method15, Method13, Method4, Method29, Method24, Method6, Method25, Method30, Method8, Method9, Method23, Method7, Method12, Method22, Method21, Method5, Method27, Method26, Method20, Method10, Method2, Method14, Method18, Method11, Method13, Method1, Method3, Method19, Method28, Method15, Method27, Method16, Method1, Method5, Method7, Method30, Method12, Method14, Method22, Method4, Method3, Method25, Method9, Method20, Method18, Method2, Method11, Method23, Method8, Method15, Method10, Method21, Method17, Method24, Method29, Method11, Method2, Method27, Method3, Method5, Method29, Method8, Method14, Method25, Method7, Method24, Method20, Method10, Method21, Method9, Method12, Method22, Method18, Method1, Method30, Method19, Method15, Method23, Method6, Method29, Method12, Method11, Method3, Method1, Method15, Method14, Method25, Method5, Method10, Method24, Method21, Method2, Method22, Method7, Method30, Method23, Method18, Method27, Method9, Method20, Method6, Method1, Method2, Method27, Method8, Method4, Method5, Method21, Method12, Method16, Method24, Method15, Method29, Method23, Method7, Method14, Method20, Method22, Method18, Method9, Method3, Method30, Method11, Method26, Method13, Method10, Method25, Method28, Method9, Method3, Method21, Method11, Method18, Method20, Method27, Method23, Method17, Method2, Method26, Method15, Method6, Method7, Method25, Method22, Method1, Method10, Method30, Method29, Method5, Method28, Method12, Method16, Method24, Method4, Method17, Method2, Method21, Method9, Method22, Method6, Method27, Method10, Method29, Method25, Method19, Method16, Method18, Method5, Method12, Method7, Method3, Method8, Method30, Method20, Method15, Method11, Method23, Method1, Method24, Method20, Method14, Method10, Method3, Method18, Method2, Method12, Method21, Method9, Method30, Method7, Method29, Method23, Method11, Method25, Method6, Method22, Method5, Method27, Method24, Method8, Method15, Method1, Method3, Method23, Method12, Method30, Method10, Method20, Method27, Method17, Method8, Method29, Method18, Method22, Method25, Method24, Method9, Method15, Method26, Method11, Method2, Method16, Method7, Method1, Method21, Method6, Method19, Method5, Method25, Method6, Method24, Method16, Method7, Method11, Method23, Method3, Method14, Method22, Method8, Method13, Method20, Method18, Method17, Method1, Method21, Method15, Method29, Method27, Method5, Method19, Method30, Method9, Method2, Method26, Method10, Method4, Method12, Method6, Method10, Method12, Method20, Method2, Method16, Method3, Method21, Method24, Method1, Method4, Method11, Method23, Method8, Method25, Method13, Method29, Method5, Method9, Method27, Method14, Method7, Method22, Method15, Method18, Method30, Method26, Method23, Method16, Method3, Method10, Method12, Method24, Method5, Method8, Method14, Method29, Method20, Method15, Method9, Method22, Method6, Method18, Method7, Method13, Method2, Method1, Method30, Method27, Method21, Method25, Method11