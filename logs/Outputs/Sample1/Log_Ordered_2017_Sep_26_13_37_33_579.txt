####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 4301156419

##### Method Results #####


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18
        End Time: 29


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 4
        End Time: 35


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5
        End Time: 42


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 44


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 53


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 9
        End Time: 70


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6
        End Time: 84


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 92


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4
        End Time: 98


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 10
        End Time: 110


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 8
        End Time: 120


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 3
        End Time: 125


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 20
        End Time: 149


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 5
        End Time: 156


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 158


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 7
        End Time: 170


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 11
        End Time: 187


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1
        End Time: 197


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 12
        End Time: 221


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 225


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 7
        End Time: 237


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 249


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 7
        End Time: 258


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 267


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 287


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 291


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 0.0
        With Duration: 0
        End Time: 293


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 6
        End Time: 301


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 9
        End Time: 312


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 316

##### Relationship Results #####


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 660
        Received: 9882
        Completed: Method23, Method15, Method18, Method17, Method22, Method1, Method6, Method9, Method4, Method2, Method7, Method12, Method21, Method25, Method20, Method24, Method10, Method5, Method30, Method27, Method19, Method3, Method29, Method11, Method16, Method26, Method25, Method5, Method22, Method16, Method10, Method27, Method3, Method2, Method17, Method15, Method29, Method23, Method7, Method30, Method21, Method9, Method14, Method18, Method12, Method26, Method24, Method6, Method19, Method1, Method11, Method20, Method2, Method22, Method3, Method19, Method24, Method8, Method18, Method13, Method6, Method7, Method5, Method27, Method10, Method25, Method15, Method17, Method1, Method20, Method29, Method21, Method23, Method16, Method11, Method12, Method30, Method9, Method4, Method25, Method9, Method29, Method3, Method20, Method7, Method2, Method14, Method27, Method12, Method30, Method10, Method1, Method5, Method11, Method6, Method24, Method15, Method18, Method21, Method22, Method26, Method23, Method3, Method12, Method16, Method23, Method18, Method5, Method17, Method22, Method8, Method25, Method7, Method2, Method15, Method11, Method21, Method19, Method9, Method20, Method1, Method29, Method10, Method24, Method27, Method6, Method30, Method3, Method25, Method20, Method24, Method2, Method16, Method21, Method9, Method7, Method18, Method15, Method6, Method11, Method30, Method12, Method22, Method17, Method10, Method13, Method26, Method23, Method29, Method5, Method27, Method1, Method23, Method1, Method16, Method11, Method15, Method7, Method22, Method5, Method19, Method24, Method25, Method14, Method9, Method21, Method26, Method10, Method17, Method6, Method30, Method29, Method18, Method3, Method12, Method2, Method27, Method20, Method28, Method8, Method9, Method2, Method20, Method3, Method27, Method29, Method11, Method6, Method1, Method30, Method15, Method25, Method26, Method16, Method10, Method4, Method24, Method12, Method18, Method5, Method22, Method21, Method28, Method23, Method7, Method11, Method21, Method9, Method30, Method16, Method24, Method15, Method10, Method7, Method23, Method27, Method1, Method6, Method12, Method29, Method14, Method20, Method5, Method2, Method18, Method26, Method3, Method28, Method22, Method25, Method14, Method11, Method22, Method21, Method17, Method23, Method2, Method7, Method12, Method30, Method18, Method25, Method19, Method9, Method10, Method8, Method6, Method15, Method24, Method27, Method3, Method20, Method5, Method1, Method13, Method29, Method21, Method30, Method9, Method25, Method24, Method23, Method16, Method14, Method18, Method17, Method11, Method8, Method22, Method20, Method10, Method1, Method13, Method15, Method29, Method2, Method7, Method12, Method27, Method4, Method5, Method3, Method26