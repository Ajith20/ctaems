####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 2369216060

##### Method Results #####


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 8


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 11


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 9


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 14


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 7


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 7


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 15


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 8


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 4


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 7


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5

##### Relationship Results #####


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 6176
        Received: 26125
        Completed: Method19, Method3, Method30, Method2, Method5, Method7, Method23, Method24, Method17, Method10, Method6, Method1, Method27, Method21, Method22, Method9, Method20, Method16, Method29, Method18, Method8, Method25, Method17, Method3, Method6, Method8, Method22, Method18, Method30, Method20, Method19, Method21, Method9, Method23, Method26, Method2, Method7, Method24, Method10, Method27, Method1, Method25, Method16, Method5, Method2, Method18, Method24, Method25, Method7, Method29, Method1, Method17, Method27, Method5, Method21, Method22, Method23, Method30, Method10, Method19, Method13, Method9, Method6, Method4, Method16, Method3, Method8, Method20, Method15, Method8, Method29, Method17, Method7, Method22, Method27, Method6, Method21, Method2, Method3, Method23, Method5, Method9, Method19, Method25, Method16, Method1, Method10, Method30, Method20, Method24, Method18, Method15, Method11, Method29, Method30, Method19, Method8, Method9, Method23, Method21, Method1, Method27, Method20, Method24, Method6, Method25, Method17, Method10, Method4, Method16, Method26, Method7, Method18, Method5, Method3, Method22, Method2, Method11, Method3, Method9, Method5, Method24, Method20, Method16, Method25, Method26, Method27, Method7, Method30, Method28, Method6, Method1, Method23, Method21, Method17, Method19, Method29, Method22, Method10, Method8, Method2, Method12, Method18, Method1, Method8, Method16, Method17, Method19, Method30, Method21, Method23, Method6, Method29, Method24, Method3, Method25, Method7, Method22, Method18, Method9, Method20, Method5, Method4, Method10, Method2, Method9, Method2, Method19, Method3, Method26, Method20, Method16, Method22, Method29, Method18, Method28, Method23, Method21, Method7, Method5, Method17, Method6, Method10, Method1, Method8, Method24, Method25, Method2, Method22, Method8, Method17, Method3, Method20, Method21, Method7, Method6, Method30, Method25, Method19, Method29, Method10, Method1, Method23, Method27, Method24, Method18, Method9, Method15, Method16, Method5, Method9, Method17, Method7, Method21, Method23, Method25, Method10, Method16, Method1, Method24, Method27, Method18, Method3, Method5, Method20, Method26, Method30, Method13, Method28, Method19, Method8, Method2, Method6, Method22, Method30, Method27, Method19, Method21, Method7, Method3, Method24, Method29, Method13, Method6, Method23, Method9, Method17, Method16, Method2, Method10, Method18, Method1, Method8, Method20, Method22, Method5, Method25, Method19, Method27, Method22, Method21, Method20, Method25, Method9, Method1, Method6, Method10, Method23, Method7, Method26, Method16, Method17, Method3, Method29, Method2, Method28, Method30, Method8, Method18, Method4, Method24, Method5, Method3, Method10, Method18, Method6, Method16, Method2, Method1, Method23, Method9, Method8, Method25, Method22, Method21, Method27, Method17, Method4, Method5, Method30, Method19, Method24, Method29, Method26, Method28, Method7, Method20