####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 1630703728

##### Method Results #####


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 1


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 14


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 9


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 7


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 9


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 16481
        Received: 52039
        Completed: Method21, Method16, Method7, Method27, Method20, Method9, Method18, Method3, Method17, Method26, Method29, Method1, Method22, Method25, Method19, Method6, Method24, Method2, Method10, Method8, Method23, Method5, Method12, Method29, Method30, Method10, Method22, Method23, Method24, Method25, Method2, Method7, Method6, Method20, Method3, Method16, Method21, Method9, Method18, Method17, Method8, Method5, Method27, Method1, Method19, Method15, Method23, Method30, Method22, Method10, Method20, Method29, Method6, Method19, Method16, Method5, Method8, Method24, Method25, Method2, Method17, Method27, Method21, Method1, Method18, Method7, Method3, Method9, Method13, Method11, Method23, Method5, Method22, Method27, Method16, Method18, Method2, Method1, Method3, Method6, Method20, Method25, Method24, Method19, Method8, Method9, Method29, Method4, Method10, Method7, Method17, Method11, Method21, Method19, Method8, Method24, Method5, Method3, Method1, Method16, Method30, Method21, Method10, Method6, Method2, Method29, Method20, Method13, Method25, Method27, Method4, Method17, Method9, Method7, Method23, Method22, Method15, Method18, Method16, Method22, Method6, Method29, Method17, Method5, Method25, Method24, Method27, Method7, Method21, Method10, Method3, Method18, Method9, Method19, Method23, Method20, Method2, Method8, Method1, Method14, Method13, Method8, Method23, Method19, Method5, Method27, Method21, Method10, Method25, Method9, Method6, Method22, Method24, Method17, Method30, Method26, Method16, Method20, Method7, Method3, Method14, Method18, Method1, Method2, Method13, Method6, Method22, Method29, Method18, Method21, Method17, Method5, Method3, Method10, Method7, Method9, Method27, Method2, Method16, Method20, Method23, Method8, Method24, Method25, Method19, Method1, Method22, Method23, Method21, Method27, Method18, Method30, Method25, Method3, Method8, Method19, Method2, Method7, Method5, Method16, Method29, Method17, Method6, Method9, Method1, Method20, Method10, Method15, Method4, Method12, Method24, Method16, Method20, Method21, Method9, Method23, Method6, Method1, Method19, Method5, Method18, Method22, Method10, Method7, Method2, Method29, Method25, Method27, Method30, Method8, Method24, Method3, Method17, Method27, Method30, Method5, Method19, Method18, Method22, Method23, Method8, Method10, Method9, Method16, Method3, Method2, Method29, Method21, Method20, Method1, Method6, Method7, Method25, Method24, Method17, Method27, Method3, Method1, Method6, Method4, Method25, Method16, Method23, Method21, Method10, Method18, Method30, Method5, Method29, Method9, Method24, Method2, Method19, Method8, Method20, Method13, Method22, Method7, Method17, Method27, Method18, Method16, Method8, Method1, Method2, Method5, Method6, Method7, Method10, Method17, Method3, Method29, Method19, Method9, Method21, Method24, Method25, Method4, Method30, Method23, Method22, Method20, Method16, Method8, Method9, Method19, Method18, Method21, Method3, Method25, Method5, Method2, Method22, Method1, Method23, Method24, Method17, Method29, Method7, Method6, Method10, Method20, Method11, Method18, Method19, Method2, Method22, Method20, Method8, Method5, Method1, Method9, Method27, Method23, Method17, Method25, Method30, Method4, Method7, Method16, Method10, Method3, Method21, Method24, Method27, Method29, Method24, Method20, Method30, Method21, Method6, Method7, Method9, Method5, Method10, Method8, Method16, Method22, Method12, Method2, Method18, Method3, Method19, Method23, Method25, Method11, Method1, Method17, Method30, Method17, Method16, Method9, Method25, Method5, Method2, Method3, Method1, Method20, Method24, Method29, Method22, Method10, Method7, Method21, Method26, Method18, Method23, Method27, Method19, Method13, Method6, Method4, Method8, Method12, Method19, Method1, Method16, Method7, Method29, Method21, Method30, Method18, Method23, Method17, Method20, Method2, Method4, Method24, Method3, Method8, Method22, Method10, Method5, Method9, Method25, Method3, Method1, Method7, Method25, Method16, Method22, Method5, Method21, Method20, Method9, Method19, Method6, Method30, Method8, Method23, Method27, Method18, Method26, Method29, Method2, Method17, Method10, Method24, Method3, Method25, Method24, Method20, Method29, Method9, Method16, Method1, Method10, Method17, Method27, Method2, Method7, Method8, Method22, Method5, Method4, Method30, Method26, Method13, Method21, Method23, Method18, Method19, Method21, Method20, Method24, Method2, Method5, Method8, Method30, Method22, Method23, Method19, Method25, Method17, Method7, Method9, Method3, Method16, Method6, Method29, Method27, Method1, Method18, Method10, Method29, Method9, Method5, Method2, Method8, Method7, Method16, Method3, Method1, Method6, Method22, Method24, Method19, Method18, Method27, Method10, Method26, Method21, Method20, Method25, Method30, Method11, Method28, Method17, Method13, Method23, Method30, Method1, Method21, Method16, Method7, Method19, Method25, Method27, Method29, Method8, Method22, Method6, Method17, Method20, Method10, Method3, Method5, Method9, Method18, Method2, Method24, Method23, Method15, Method4, Method17, Method2, Method7, Method19, Method8, Method23, Method16, Method20, Method5, Method6, Method22, Method29, Method25, Method30, Method24, Method1, Method9, Method10, Method27, Method3, Method18, Method21