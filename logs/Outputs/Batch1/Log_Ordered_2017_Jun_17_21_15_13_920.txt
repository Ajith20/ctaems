####### Ordered Logger #######


Final Duration: 277.0
Final Quality: 0.0
Seed: 9079417344

##### Method Results #####


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 6


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 8


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 77.5
        With Duration: 11


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 15


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 5


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.5
        With Duration: 4


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 18


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 6


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 46.0
        With Duration: 7


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 5.3999999999999995
        With Duration: 3


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 0.5
        With Duration: 1

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 4366
        Received: 24558
        Completed: Method27, Method1, Method13, Method22, Method20, Method9, Method2, Method18, Method17, Method29, Method19, Method7, Method23, Method12, Method25, Method16, Method21, Method11, Method5, Method26, Method4, Method10, Method28, Method15, Method8, Method30, Method24, Method12, Method6, Method20, Method23, Method16, Method13, Method26, Method29, Method21, Method8, Method22, Method27, Method28, Method7, Method17, Method9, Method30, Method10, Method25, Method2, Method24, Method5, Method15, Method1, Method11, Method18, Method10, Method21, Method18, Method13, Method9, Method5, Method28, Method2, Method30, Method23, Method8, Method26, Method22, Method7, Method14, Method24, Method12, Method1, Method29, Method3, Method15, Method25, Method6, Method17, Method19, Method27, Method16, Method20, Method28, Method5, Method16, Method13, Method14, Method12, Method1, Method20, Method6, Method9, Method17, Method8, Method7, Method26, Method23, Method18, Method10, Method22, Method24, Method29, Method30, Method4, Method2, Method15, Method27, Method21, Method25, Method7, Method17, Method10, Method9, Method1, Method15, Method3, Method26, Method22, Method14, Method27, Method18, Method4, Method28, Method29, Method24, Method16, Method2, Method25, Method5, Method12, Method20, Method30, Method21, Method23, Method13, Method19, Method8, Method12, Method30, Method10, Method20, Method13, Method17, Method1, Method27, Method11, Method26, Method9, Method2, Method5, Method15, Method23, Method18, Method29, Method7, Method3, Method16, Method6, Method24, Method21, Method28, Method8, Method25, Method22, Method29, Method6, Method24, Method15, Method17, Method22, Method30, Method7, Method5, Method21, Method1, Method8, Method9, Method20, Method16, Method19, Method26, Method12, Method23, Method13, Method10, Method2, Method27, Method4, Method18, Method28, Method27, Method3, Method2, Method16, Method18, Method20, Method22, Method17, Method8, Method23, Method10, Method5, Method24, Method12, Method14, Method6, Method21, Method11, Method7, Method13, Method29, Method30, Method15, Method26, Method9, Method28, Method1, Method10, Method2, Method27, Method5, Method30, Method8, Method24, Method6, Method20, Method18, Method16, Method7, Method13, Method28, Method23, Method22, Method12, Method15, Method9, Method26, Method14, Method29, Method19, Method1, Method21, Method17, Method15, Method1, Method10, Method21, Method4, Method18, Method20, Method14, Method16, Method24, Method17, Method13, Method2, Method26, Method9, Method12, Method23, Method22, Method27, Method5, Method28, Method8, Method29, Method7, Method11, Method30, Method25, Method16, Method9, Method14, Method12, Method30, Method27, Method22, Method18, Method24, Method20, Method2, Method21, Method15, Method8, Method28, Method1, Method29, Method19, Method6, Method23, Method17, Method11, Method10, Method26, Method7, Method13, Method4, Method25, Method5, Method13, Method18, Method2, Method20, Method29, Method16, Method12, Method21, Method15, Method9, Method5, Method23, Method26, Method30, Method28, Method17, Method7, Method27, Method8, Method6, Method1, Method24, Method22, Method25, Method10, Method6, Method15, Method13, Method2, Method17, Method23, Method8, Method5, Method1, Method18, Method29, Method30, Method16, Method24, Method4, Method21, Method10, Method22, Method26, Method20, Method27, Method9, Method7, Method28, Method12, Method13, Method14, Method20, Method24, Method2, Method26, Method29, Method30, Method7, Method3, Method21, Method15, Method17, Method16, Method23, Method5, Method10, Method28, Method1, Method4, Method18, Method9, Method22, Method8, Method27, Method12, Method5, Method14, Method15, Method9, Method20, Method22, Method2, Method3, Method10, Method12, Method6, Method30, Method13, Method18, Method29, Method23, Method16, Method26, Method28, Method21, Method1, Method7, Method24, Method17, Method27, Method8, Method19, Method25, Method27, Method3, Method10, Method24, Method11, Method1, Method23, Method30, Method7, Method8, Method28, Method21, Method20, Method5, Method17, Method14, Method16, Method9, Method29, Method6, Method18, Method2, Method22, Method4, Method12, Method13, Method15, Method26, Method6, Method14, Method28, Method13, Method17, Method8, Method20, Method5, Method18, Method7, Method27, Method24, Method26, Method10, Method29, Method1, Method15, Method22, Method9, Method12, Method2, Method21, Method3, Method30, Method16, Method25, Method23, Method15, Method24, Method22, Method6, Method30, Method13, Method26, Method10, Method23, Method12, Method2, Method27, Method3, Method20, Method1, Method7, Method11, Method19, Method8, Method5, Method21, Method18, Method9, Method25, Method29, Method16, Method17, Method28, Method13, Method5, Method10, Method16, Method23, Method27, Method22, Method9, Method30, Method15, Method17, Method11, Method20, Method12, Method14, Method2, Method29, Method18, Method1, Method19, Method24, Method21, Method7, Method25, Method6, Method8, Method28, Method26, Method22, Method28, Method20, Method2, Method29, Method3, Method13, Method26, Method19, Method7, Method27, Method9, Method30, Method12, Method17, Method21, Method5, Method1, Method10, Method18, Method4, Method16, Method24, Method15, Method25, Method8, Method11, Method23, Method22, Method27, Method9, Method18, Method21, Method13, Method28, Method15, Method1, Method16, Method5, Method26, Method19, Method30, Method23, Method12, Method25, Method6, Method2, Method7, Method24, Method11, Method17, Method29, Method8, Method20, Method10, Method7, Method26, Method30, Method14, Method9, Method17, Method16, Method21, Method22, Method8, Method25, Method6, Method23, Method27, Method18, Method29, Method5, Method24, Method1, Method13, Method12, Method10, Method2, Method19, Method28, Method20, Method11, Method4, Method15