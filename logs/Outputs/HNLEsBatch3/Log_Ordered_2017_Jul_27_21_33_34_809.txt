####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 8481905629

##### Method Results #####


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 1


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 9


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 1


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 20


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 17


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 6


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 2


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 5


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 7


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1

##### Relationship Results #####


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 11507
        Received: 39191
        Completed: Method8, Method16, Method11, Method9, Method5, Method27, Method7, Method6, Method15, Method24, Method26, Method20, Method13, Method3, Method17, Method29, Method12, Method2, Method21, Method1, Method30, Method22, Method18, Method10, Method23, Method4, Method25, Method7, Method6, Method18, Method17, Method1, Method10, Method25, Method27, Method22, Method8, Method9, Method29, Method20, Method3, Method24, Method13, Method2, Method30, Method11, Method5, Method21, Method23, Method15, Method26, Method16, Method4, Method12, Method22, Method27, Method25, Method18, Method9, Method12, Method16, Method23, Method5, Method24, Method15, Method26, Method11, Method29, Method3, Method21, Method19, Method10, Method1, Method6, Method30, Method13, Method7, Method20, Method2, Method5, Method16, Method18, Method24, Method6, Method20, Method13, Method30, Method17, Method29, Method15, Method1, Method25, Method10, Method21, Method9, Method3, Method23, Method11, Method22, Method27, Method12, Method2, Method7, Method4, Method1, Method22, Method7, Method30, Method4, Method8, Method14, Method20, Method10, Method3, Method17, Method29, Method2, Method24, Method18, Method27, Method11, Method15, Method19, Method9, Method5, Method12, Method25, Method23, Method21, Method13, Method7, Method16, Method14, Method23, Method18, Method12, Method27, Method30, Method25, Method1, Method3, Method5, Method9, Method10, Method8, Method17, Method24, Method29, Method2, Method22, Method21, Method11, Method15, Method6, Method13, Method4, Method20, Method26, Method10, Method22, Method11, Method12, Method25, Method15, Method9, Method19, Method21, Method24, Method14, Method3, Method1, Method6, Method7, Method2, Method23, Method18, Method5, Method20, Method29, Method30, Method26, Method27, Method21, Method5, Method30, Method9, Method7, Method3, Method29, Method20, Method24, Method1, Method13, Method8, Method25, Method23, Method26, Method2, Method28, Method18, Method11, Method22, Method6, Method10, Method19, Method27, Method12, Method16, Method15, Method2, Method5, Method15, Method16, Method6, Method12, Method30, Method25, Method29, Method14, Method21, Method20, Method22, Method18, Method11, Method10, Method1, Method23, Method9, Method24, Method3, Method7, Method27, Method9, Method5, Method1, Method22, Method2, Method11, Method23, Method12, Method17, Method20, Method15, Method19, Method16, Method6, Method29, Method24, Method10, Method26, Method30, Method14, Method18, Method25, Method3, Method27, Method4, Method7, Method21, Method6, Method9, Method12, Method27, Method2, Method8, Method20, Method7, Method10, Method3, Method23, Method15, Method29, Method18, Method16, Method5, Method11, Method22, Method25, Method24, Method21, Method30, Method1, Method16, Method3, Method29, Method20, Method18, Method10, Method11, Method5, Method12, Method27, Method22, Method30, Method8, Method2, Method15, Method6, Method9, Method21, Method7, Method24, Method1, Method25, Method23, Method24, Method1, Method15, Method14, Method12, Method9, Method10, Method22, Method6, Method30, Method20, Method19, Method3, Method5, Method13, Method18, Method29, Method7, Method21, Method27, Method25, Method23, Method2, Method4, Method11, Method20, Method27, Method12, Method7, Method14, Method15, Method11, Method22, Method6, Method10, Method30, Method21, Method1, Method29, Method5, Method4, Method9, Method25, Method2, Method3, Method18, Method23, Method26, Method24, Method21, Method7, Method20, Method17, Method15, Method14, Method18, Method3, Method30, Method10, Method25, Method23, Method9, Method24, Method27, Method11, Method29, Method6, Method1, Method26, Method2, Method12, Method5, Method22, Method18, Method27, Method22, Method21, Method5, Method2, Method10, Method15, Method29, Method16, Method6, Method1, Method3, Method7, Method9, Method4, Method23, Method11, Method30, Method20, Method19, Method25, Method24, Method12, Method26, Method16, Method15, Method25, Method14, Method10, Method11, Method22, Method6, Method21, Method30, Method20, Method19, Method27, Method17, Method3, Method23, Method2, Method1, Method18, Method24, Method29, Method7, Method5, Method12, Method9, Method5, Method17, Method6, Method7, Method29, Method22, Method11, Method20, Method18, Method24, Method1, Method8, Method27, Method16, Method9, Method21, Method2, Method13, Method10, Method23, Method4, Method15, Method30, Method12, Method3, Method25, Method26, Method19