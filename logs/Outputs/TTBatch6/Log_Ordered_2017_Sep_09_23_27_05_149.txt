####### Ordered Logger #######


Final Duration: 1800.0
Final Quality: 0.0
Seed: 2842038452

##### Method Results #####


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 5


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 15


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 17


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 7


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 4


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 6


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 9


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5

##### Relationship Results #####


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 7257
        Received: 24094
        Completed: Method17, Method21, Method10, Method8, Method9, Method27, Method7, Method29, Method1, Method22, Method3, Method2, Method25, Method18, Method23, Method19, Method30, Method5, Method16, Method24, Method6, Method11, Method4, Method20, Method13, Method29, Method24, Method6, Method25, Method30, Method8, Method9, Method23, Method7, Method22, Method21, Method5, Method27, Method17, Method26, Method20, Method10, Method2, Method14, Method18, Method11, Method13, Method1, Method3, Method19, Method28, Method15, Method27, Method16, Method1, Method5, Method7, Method19, Method30, Method22, Method4, Method3, Method25, Method9, Method20, Method18, Method2, Method23, Method8, Method10, Method21, Method17, Method24, Method2, Method27, Method3, Method5, Method29, Method8, Method25, Method7, Method24, Method16, Method20, Method10, Method21, Method9, Method22, Method18, Method1, Method30, Method17, Method19, Method23, Method6, Method29, Method3, Method1, Method25, Method5, Method10, Method16, Method24, Method19, Method21, Method2, Method22, Method7, Method30, Method23, Method18, Method8, Method27, Method9, Method17, Method20, Method6, Method1, Method2, Method27, Method8, Method4, Method5, Method21, Method16, Method24, Method29, Method23, Method7, Method19, Method20, Method22, Method18, Method17, Method9, Method3, Method26, Method13, Method10, Method25, Method28, Method9, Method3, Method21, Method18, Method20, Method19, Method27, Method23, Method17, Method2, Method26, Method6, Method7, Method25, Method8, Method22, Method1, Method10, Method30, Method29, Method5, Method28, Method12, Method16, Method24, Method4, Method17, Method2, Method21, Method9, Method22, Method6, Method27, Method10, Method29, Method25, Method19, Method16, Method18, Method5, Method7, Method3, Method8, Method30, Method20, Method15, Method11, Method23, Method1, Method13, Method24, Method14, Method20, Method10, Method3, Method18, Method2, Method21, Method9, Method30, Method7, Method19, Method29, Method23, Method25, Method6, Method22, Method5, Method17, Method27, Method24, Method16, Method8, Method15, Method1, Method3, Method23, Method30, Method10, Method20, Method27, Method17, Method8, Method29, Method18, Method22, Method25, Method24, Method9, Method26, Method2, Method16, Method7, Method1, Method21, Method6, Method19, Method5