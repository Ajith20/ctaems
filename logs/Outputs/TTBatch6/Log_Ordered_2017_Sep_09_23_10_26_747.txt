####### Ordered Logger #######


Final Duration: 1800.0
Final Quality: 0.0
Seed: 7830004884

##### Method Results #####


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 12


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 7


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 17


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 10


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 7


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 4


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 3


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 3


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12

##### Relationship Results #####


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 4431
        Received: 11768
        Completed: Method17, Method21, Method10, Method8, Method9, Method27, Method7, Method29, Method1, Method22, Method3, Method2, Method25, Method18, Method23, Method19, Method30, Method5, Method16, Method24, Method6, Method11, Method4, Method20, Method13, Method29, Method24, Method6, Method25, Method30, Method8, Method9, Method23, Method7, Method22, Method21, Method5, Method27, Method17, Method26, Method20, Method10, Method2, Method14, Method18, Method11, Method13, Method1, Method3, Method19, Method28, Method15, Method27, Method16, Method1, Method5, Method7, Method19, Method30, Method22, Method4, Method3, Method25, Method9, Method20, Method18, Method2, Method23, Method8, Method10, Method21, Method17, Method24, Method2, Method27, Method3, Method5, Method29, Method8, Method25, Method7, Method24, Method16, Method20, Method10, Method21, Method9, Method22, Method18, Method1, Method30, Method17, Method19, Method23, Method6