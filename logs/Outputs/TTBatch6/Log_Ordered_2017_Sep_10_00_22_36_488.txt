####### Ordered Logger #######


Final Duration: 1800.0
Final Quality: 0.0
Seed: 9976366158

##### Method Results #####


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 9


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 14


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 11


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 9


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 15


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 7


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 4


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 11

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 23486
        Received: 72075
        Completed: Method17, Method21, Method10, Method8, Method9, Method27, Method7, Method29, Method1, Method22, Method3, Method2, Method25, Method18, Method23, Method19, Method30, Method5, Method16, Method24, Method6, Method11, Method4, Method20, Method13, Method29, Method24, Method6, Method25, Method30, Method8, Method9, Method23, Method7, Method22, Method21, Method5, Method27, Method17, Method26, Method20, Method10, Method2, Method14, Method18, Method11, Method13, Method1, Method3, Method19, Method28, Method15, Method27, Method16, Method1, Method5, Method7, Method19, Method30, Method22, Method4, Method3, Method25, Method9, Method20, Method18, Method2, Method23, Method8, Method10, Method21, Method17, Method24, Method2, Method27, Method3, Method5, Method29, Method8, Method25, Method7, Method24, Method16, Method20, Method10, Method21, Method9, Method22, Method18, Method1, Method30, Method17, Method19, Method23, Method6, Method29, Method3, Method1, Method25, Method5, Method10, Method16, Method24, Method19, Method21, Method2, Method22, Method7, Method30, Method23, Method18, Method8, Method27, Method9, Method17, Method20, Method6, Method1, Method2, Method27, Method8, Method4, Method5, Method21, Method16, Method24, Method29, Method23, Method7, Method19, Method20, Method22, Method18, Method17, Method9, Method3, Method26, Method13, Method10, Method25, Method28, Method9, Method3, Method21, Method18, Method20, Method19, Method27, Method23, Method17, Method2, Method26, Method6, Method7, Method25, Method8, Method22, Method1, Method10, Method30, Method29, Method5, Method28, Method12, Method16, Method24, Method4, Method17, Method2, Method21, Method9, Method22, Method6, Method27, Method10, Method29, Method25, Method19, Method16, Method18, Method5, Method7, Method3, Method8, Method30, Method20, Method15, Method11, Method23, Method1, Method13, Method24, Method14, Method20, Method10, Method3, Method18, Method2, Method21, Method9, Method30, Method7, Method19, Method29, Method23, Method25, Method6, Method22, Method5, Method17, Method27, Method24, Method16, Method8, Method15, Method1, Method3, Method23, Method30, Method10, Method20, Method27, Method17, Method8, Method29, Method18, Method22, Method25, Method24, Method9, Method26, Method2, Method16, Method7, Method1, Method21, Method6, Method19, Method5, Method25, Method6, Method24, Method16, Method7, Method23, Method3, Method22, Method8, Method13, Method20, Method18, Method17, Method1, Method21, Method29, Method27, Method5, Method19, Method9, Method2, Method26, Method10, Method4, Method12, Method6, Method10, Method20, Method2, Method16, Method3, Method21, Method24, Method1, Method4, Method23, Method8, Method25, Method13, Method29, Method5, Method19, Method9, Method27, Method17, Method7, Method22, Method15, Method18, Method23, Method16, Method3, Method10, Method24, Method5, Method19, Method8, Method29, Method20, Method17, Method9, Method22, Method6, Method18, Method7, Method13, Method2, Method1, Method21, Method25, Method11, Method20, Method18, Method19, Method6, Method2, Method24, Method30, Method25, Method3, Method10, Method29, Method1, Method8, Method27, Method4, Method13, Method9, Method5, Method23, Method22, Method21, Method17, Method7, Method16, Method22, Method23, Method3, Method10, Method2, Method30, Method9, Method16, Method6, Method5, Method21, Method8, Method18, Method19, Method26, Method29, Method20, Method24, Method7, Method25, Method17, Method1, Method27, Method3, Method2, Method24, Method30, Method6, Method1, Method17, Method16, Method19, Method29, Method25, Method20, Method7, Method23, Method18, Method22, Method9, Method21, Method8, Method5, Method10, Method2, Method24, Method21, Method7, Method22, Method25, Method1, Method13, Method4, Method17, Method3, Method5, Method8, Method20, Method29, Method30, Method9, Method27, Method10, Method23, Method16, Method19, Method18, Method6, Method18, Method1, Method3, Method24, Method27, Method13, Method29, Method2, Method25, Method23, Method7, Method20, Method30, Method19, Method22, Method5, Method8, Method16, Method17, Method9, Method10, Method12, Method21, Method3, Method18, Method8, Method20, Method7, Method1, Method16, Method30, Method9, Method21, Method25, Method27, Method23, Method6, Method17, Method5, Method24, Method29, Method2, Method4, Method22, Method10, Method26, Method19, Method7, Method1, Method21, Method2, Method23, Method8, Method6, Method3, Method16, Method20, Method10, Method30, Method25, Method9, Method15, Method18, Method5, Method22, Method27, Method24, Method12, Method29, Method11, Method19, Method17, Method4, Method25, Method30, Method5, Method16, Method19, Method7, Method21, Method10, Method2, Method17, Method9, Method29, Method3, Method6, Method20, Method23, Method18, Method8, Method13, Method22, Method24, Method12, Method1, Method14, Method29, Method1, Method8, Method27, Method23, Method2, Method19, Method4, Method21, Method16, Method18, Method25, Method3, Method22, Method30, Method24, Method10, Method7, Method5, Method9, Method26, Method20, Method17, Method13, Method17, Method25, Method24, Method30, Method20, Method29, Method13, Method22, Method21, Method6, Method16, Method10, Method19, Method9, Method5, Method23, Method2, Method1, Method8, Method7, Method27, Method18, Method15, Method3, Method19, Method10, Method18, Method23, Method2, Method9, Method29, Method26, Method5, Method8, Method20, Method7, Method24, Method21, Method22, Method6, Method15, Method27, Method25, Method14, Method11, Method30, Method17, Method1, Method3, Method12, Method2, Method10, Method18, Method17, Method29, Method9, Method24, Method26, Method8, Method23, Method5, Method20, Method3, Method6, Method25, Method28, Method30, Method27, Method1, Method19, Method21, Method16, Method7, Method14, Method22, Method21, Method18, Method16, Method6, Method5, Method27, Method24, Method19, Method30, Method10, Method23, Method17, Method2, Method3, Method9, Method7, Method13, Method25, Method20, Method22, Method8, Method1, Method21, Method29, Method17, Method7, Method30, Method5, Method2, Method22, Method3, Method18, Method16, Method9, Method10, Method20, Method27, Method1, Method6, Method24, Method25, Method8, Method19, Method23, Method18, Method16, Method1, Method25, Method30, Method8, Method22, Method7, Method10, Method20, Method2, Method4, Method23, Method27, Method9, Method24, Method17, Method29, Method21, Method19, Method3, Method5, Method17, Method16, Method6, Method1, Method8, Method18, Method20, Method7, Method2, Method21, Method24, Method29, Method19, Method3, Method22, Method4, Method5, Method23, Method9, Method25, Method10