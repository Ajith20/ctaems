####### Ordered Logger #######


Final Duration: 1000.0
Final Quality: 0.0
Seed: 6557212942

##### Method Results #####


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 9


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 5


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 5


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 11


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 12


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 18


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 9


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 4.8
        With Duration: 4


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 65.0
        With Duration: 11


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 3341
        Received: 18285
        Completed: Method16, Method10, Method6, Method27, Method30, Method26, Method2, Method17, Method21, Method8, Method13, Method22, Method20, Method15, Method24, Method19, Method23, Method1, Method7, Method29, Method18, Method9, Method28, Method5, Method12, Method25, Method24, Method30, Method23, Method26, Method8, Method17, Method14, Method27, Method22, Method9, Method21, Method1, Method4, Method16, Method11, Method7, Method15, Method5, Method20, Method12, Method10, Method25, Method13, Method28, Method18, Method2, Method29, Method24, Method18, Method15, Method26, Method6, Method7, Method30, Method10, Method13, Method9, Method8, Method14, Method12, Method3, Method17, Method23, Method28, Method1, Method16, Method21, Method20, Method29, Method27, Method4, Method2, Method22, Method5, Method7, Method1, Method22, Method3, Method13, Method10, Method27, Method6, Method9, Method23, Method20, Method18, Method4, Method21, Method29, Method16, Method2, Method15, Method17, Method30, Method12, Method28, Method5, Method8, Method26, Method24, Method19, Method11, Method21, Method26, Method13, Method22, Method10, Method28, Method2, Method30, Method18, Method12, Method14, Method23, Method15, Method24, Method1, Method8, Method19, Method6, Method3, Method29, Method16, Method7, Method17, Method9, Method5, Method20, Method25, Method27, Method22, Method26, Method15, Method24, Method27, Method28, Method2, Method20, Method16, Method14, Method10, Method1, Method19, Method12, Method17, Method21, Method6, Method7, Method23, Method11, Method30, Method9, Method13, Method18, Method5, Method29, Method8, Method22, Method10, Method29, Method27, Method21, Method24, Method5, Method16, Method6, Method17, Method9, Method19, Method8, Method26, Method13, Method30, Method2, Method12, Method23, Method28, Method18, Method11, Method1, Method7, Method15, Method20, Method25, Method18, Method20, Method13, Method3, Method24, Method14, Method28, Method7, Method26, Method16, Method5, Method27, Method29, Method1, Method12, Method10, Method17, Method23, Method15, Method30, Method4, Method2, Method9, Method8, Method21, Method25, Method22, Method8, Method3, Method22, Method18, Method21, Method6, Method24, Method29, Method7, Method10, Method15, Method20, Method27, Method2, Method16, Method13, Method26, Method9, Method17, Method23, Method12, Method1, Method19, Method5, Method28, Method30, Method22, Method12, Method29, Method27, Method3, Method7, Method1, Method28, Method5, Method13, Method10, Method23, Method30, Method16, Method2, Method20, Method17, Method26, Method19, Method21, Method9, Method18, Method24, Method6, Method8, Method25, Method15, Method11, Method6, Method16, Method5, Method2, Method30, Method13, Method27, Method11, Method21, Method10, Method23, Method28, Method22, Method18, Method29, Method17, Method7, Method24, Method9, Method20, Method1, Method25, Method4, Method12, Method26, Method8, Method15, Method18, Method28, Method6, Method29, Method30, Method1, Method22, Method5, Method15, Method12, Method24, Method26, Method20, Method17, Method16, Method7, Method2, Method8, Method9, Method10, Method27, Method21, Method23, Method13, Method9, Method23, Method17, Method28, Method1, Method5, Method21, Method22, Method6, Method12, Method4, Method2, Method8, Method14, Method29, Method24, Method20, Method7, Method16, Method30, Method13, Method10, Method19, Method15, Method18, Method26, Method27, Method11, Method25, Method12, Method18, Method14, Method29, Method17, Method23, Method3, Method7, Method5, Method8, Method27, Method10, Method28, Method2, Method24, Method6, Method26, Method11, Method1, Method30, Method9, Method16, Method22, Method20, Method13, Method15, Method21, Method10, Method23, Method12, Method18, Method30, Method13, Method5, Method7, Method14, Method1, Method27, Method17, Method21, Method22, Method6, Method29, Method24, Method15, Method28, Method2, Method3, Method9, Method19, Method20, Method8, Method16, Method26