####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 5394217705

##### Method Results #####


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 6


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 10


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 21


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 28


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9
        End Time: 39


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 5
        End Time: 46


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12
        End Time: 67


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 12
        End Time: 81


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 12
        End Time: 95


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 101


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.2352941176470589
        With Duration: 3
        End Time: 106


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 109


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.6363636363636362
        With Duration: 1
        End Time: 112


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6
        End Time: 124


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 7
        End Time: 133


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 7
        End Time: 144


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 36.50769230769231
        With Duration: 2
        End Time: 148


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 168


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7
        End Time: 177


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 5.555555555555555
        With Duration: 10
        End Time: 189


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 193


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5
        End Time: 200


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 11.700000000000001
        With Duration: 1
        End Time: 205


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.709090909090909
        With Duration: 10
        End Time: 217


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17
        End Time: 236


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 240


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 36.11111111111111
        With Duration: 7
        End Time: 251

##### Relationship Results #####


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 906
        Completed: Method17, Method1, Method7, Method16, Method18, Method14, Method2, Method6, Method13, Method29, Method15, Method26, Method30, Method9, Method20, Method28, Method10, Method21, Method22, Method12, Method4, Method5, Method24, Method23, Method27, Method19, Method8