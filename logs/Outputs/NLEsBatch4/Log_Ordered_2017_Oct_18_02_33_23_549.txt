####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6079312618

##### Method Results #####


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5
        End Time: 12


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 16


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2
        End Time: 20


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4
        End Time: 26


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9
        End Time: 37


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 6
        End Time: 48


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 103


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 107


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12
        End Time: 123


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 20
        End Time: 146


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 155


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18
        End Time: 181


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 7
        End Time: 190


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 5
        End Time: 197


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 15
        End Time: 214


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.8181818181818181
        With Duration: 10
        End Time: 229


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 10
        End Time: 241


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6
        End Time: 249


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 0.6176470588235294
        With Duration: 3
        End Time: 256


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 2
        End Time: 261


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 270


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 2
        End Time: 274


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 277


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 9
        End Time: 288


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4
        End Time: 294


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 13
        End Time: 309


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 41.4
        With Duration: 1
        End Time: 312

##### Relationship Results #####


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 906
        Completed: Method20, Method30, Method1, Method29, Method6, Method5, Method26, Method4, Method2, Method8, Method12, Method13, Method3, Method22, Method27, Method23, Method18, Method9, Method15, Method28, Method19, Method24, Method17, Method7, Method16, Method21, Method10