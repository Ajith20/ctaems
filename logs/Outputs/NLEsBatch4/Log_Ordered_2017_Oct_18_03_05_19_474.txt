####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 6863704584

##### Method Results #####


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 9
        End Time: 23


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 29


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 33


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9
        End Time: 44


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 5.555555555555555
        With Duration: 11
        End Time: 57


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 6
        End Time: 65


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 5
        End Time: 72


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 14.0
        With Duration: 6
        End Time: 82


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 8
        End Time: 100


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 7
        End Time: 109


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 141


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 33.0
        With Duration: 12
        End Time: 156


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 162


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 171


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 34.56923076923077
        With Duration: 3
        End Time: 178


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.117647058823529
        With Duration: 2
        End Time: 182


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.7
        With Duration: 0
        End Time: 184


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 13
        End Time: 199


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 203


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.8181818181818181
        With Duration: 6
        End Time: 211


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5
        End Time: 218


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 3.764705882352941
        With Duration: 8
        End Time: 231


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 41.666666666666664
        With Duration: 7
        End Time: 242


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 8
        End Time: 252


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 18
        End Time: 272


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4
        End Time: 278


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 281

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 906
        Completed: Method18, Method28, Method1, Method6, Method12, Method24, Method5, Method7, Method3, Method22, Method29, Method13, Method4, Method2, Method10, Method15, Method30, Method21, Method26, Method23, Method9, Method20, Method8, Method17, Method27, Method16, Method11