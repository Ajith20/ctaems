####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 8941780395

##### Method Results #####


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7
        End Time: 21


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 2
        End Time: 25


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9
        End Time: 36


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2
        End Time: 44


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 7
        End Time: 53


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 45.0
        With Duration: 3
        End Time: 62


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4
        End Time: 68


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6
        End Time: 76


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9
        End Time: 87


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 19
        End Time: 108


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 114


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4
        End Time: 123


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 130


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6
        End Time: 138


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 148


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 2
        End Time: 155


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 18
        End Time: 175


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 15
        End Time: 192


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.6363636363636362
        With Duration: 0
        End Time: 194


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 10
        End Time: 206


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 10
        End Time: 218


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 5
        End Time: 225


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2
        End Time: 229


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 236


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 239


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1
        End Time: 242

##### Relationship Results #####


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 905
        Completed: Method12, Method1, Method7, Method15, Method9, Method8, Method14, Method16, Method6, Method13, Method20, Method2, Method29, Method24, Method23, Method28, Method21, Method27, Method30, Method10, Method18, Method5, Method4, Method22, Method26, Method17