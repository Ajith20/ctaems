####### Ordered Logger #######


Final Duration: 800.0
Final Quality: 0.0
Seed: 7795311672

##### Method Results #####


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 20
        End Time: 35


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5
        End Time: 48


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 7
        End Time: 57


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4
        End Time: 63


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1
        End Time: 66


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 5
        End Time: 75


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5
        End Time: 83


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2
        End Time: 88


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 6
        End Time: 96


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 99


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 105


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 8
        End Time: 118


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4
        End Time: 124


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18
        End Time: 144


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9
        End Time: 158


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4
        End Time: 164


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 13
        End Time: 179


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 1
        End Time: 182


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 19
        End Time: 203


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5
        End Time: 210


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 4
        End Time: 218


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 15
        End Time: 235


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 6
        End Time: 243


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 4
        End Time: 255


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.47272727272727266
        With Duration: 8
        End Time: 268


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 7
        End Time: 278


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2
        End Time: 282


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 9
        End Time: 293

##### Relationship Results #####


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 60
        Received: 906
        Completed: Method8, Method5, Method7, Method30, Method15, Method22, Method9, Method1, Method12, Method17, Method20, Method3, Method28, Method27, Method10, Method2, Method21, Method11, Method13, Method25, Method16, Method6, Method14, Method24, Method23, Method29, Method26, Method18