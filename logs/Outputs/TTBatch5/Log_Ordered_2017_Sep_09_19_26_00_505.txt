####### Ordered Logger #######


Final Duration: 1800.0
Final Quality: 0.0
Seed: 2036710476

##### Method Results #####


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 9


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 17


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 7


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 2


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 7


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 4


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 7


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 12


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 6


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 20


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 9


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 3


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 1


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 11


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 2


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 9712
        Received: 32104
        Completed: Method10, Method16, Method5, Method1, Method29, Method18, Method19, Method25, Method30, Method22, Method27, Method24, Method13, Method20, Method21, Method7, Method4, Method17, Method23, Method2, Method9, Method8, Method3, Method19, Method6, Method5, Method25, Method3, Method8, Method27, Method23, Method22, Method20, Method10, Method7, Method18, Method16, Method30, Method29, Method24, Method2, Method17, Method1, Method4, Method9, Method21, Method16, Method20, Method6, Method8, Method23, Method24, Method9, Method1, Method19, Method2, Method21, Method10, Method5, Method25, Method17, Method30, Method7, Method4, Method27, Method14, Method29, Method15, Method3, Method26, Method18, Method22, Method5, Method16, Method21, Method25, Method22, Method2, Method1, Method3, Method18, Method20, Method24, Method23, Method27, Method19, Method4, Method10, Method8, Method30, Method17, Method9, Method7, Method27, Method1, Method9, Method5, Method23, Method6, Method30, Method17, Method3, Method18, Method22, Method20, Method21, Method19, Method8, Method26, Method4, Method29, Method7, Method24, Method10, Method2, Method16, Method25, Method10, Method30, Method6, Method21, Method8, Method23, Method3, Method2, Method5, Method24, Method18, Method13, Method20, Method25, Method27, Method7, Method19, Method9, Method16, Method22, Method29, Method1, Method17, Method21, Method30, Method6, Method10, Method18, Method25, Method3, Method9, Method27, Method29, Method16, Method20, Method8, Method2, Method1, Method23, Method19, Method7, Method22, Method24, Method17, Method5, Method17, Method1, Method9, Method21, Method26, Method2, Method5, Method7, Method6, Method30, Method20, Method27, Method22, Method24, Method29, Method10, Method3, Method19, Method8, Method25, Method4, Method23, Method28, Method18, Method16, Method7, Method17, Method24, Method9, Method30, Method13, Method5, Method1, Method25, Method16, Method10, Method26, Method21, Method18, Method20, Method8, Method4, Method23, Method27, Method29, Method2, Method28, Method19, Method22, Method3, Method6, Method24, Method9, Method27, Method8, Method20, Method7, Method5, Method16, Method3, Method10, Method23, Method18, Method19, Method1, Method30, Method22, Method29, Method2, Method25, Method21, Method13, Method17, Method15, Method12, Method24, Method2, Method17, Method21, Method9, Method8, Method23, Method16, Method13, Method7, Method19, Method22, Method20, Method10, Method27, Method3, Method30, Method26, Method6, Method5, Method25, Method1, Method11, Method29, Method18, Method20, Method30, Method8, Method29, Method19, Method6, Method18, Method17, Method23, Method25, Method24, Method22, Method2, Method5, Method3, Method21, Method1, Method9, Method16, Method7, Method13, Method10, Method16, Method10, Method27, Method9, Method26, Method29, Method22, Method2, Method3, Method19, Method25, Method6, Method5, Method8, Method24, Method7, Method23, Method30, Method21, Method17, Method18, Method28, Method1, Method20