####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 7525353730

##### Method Results #####


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 12


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 11.0
        With Duration: 15


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 3


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 23.0
        With Duration: 9


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 4


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 17.0
        With Duration: 18


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 11


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 6


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 2


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 1


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 4


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 5


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 18


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 4


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 7


    ## Method ##
        Name: Method4
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 2


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 6

##### Relationship Results #####


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 14973
        Received: 50335
        Completed: Method25, Method22, Method29, Method3, Method20, Method7, Method1, Method23, Method21, Method24, Method12, Method27, Method2, Method11, Method5, Method15, Method18, Method16, Method10, Method9, Method13, Method6, Method30, Method19, Method29, Method18, Method12, Method15, Method16, Method10, Method22, Method3, Method6, Method23, Method7, Method20, Method2, Method1, Method9, Method27, Method25, Method21, Method19, Method5, Method11, Method30, Method24, Method2, Method14, Method23, Method24, Method22, Method19, Method7, Method20, Method5, Method15, Method25, Method6, Method3, Method30, Method17, Method9, Method1, Method12, Method10, Method29, Method18, Method21, Method27, Method11, Method26, Method21, Method29, Method8, Method25, Method6, Method14, Method22, Method23, Method27, Method5, Method2, Method11, Method9, Method30, Method24, Method15, Method12, Method18, Method1, Method3, Method7, Method20, Method10, Method26, Method17, Method11, Method21, Method9, Method20, Method15, Method1, Method2, Method22, Method6, Method30, Method25, Method7, Method26, Method24, Method13, Method27, Method23, Method10, Method29, Method5, Method28, Method18, Method3, Method4, Method12, Method16, Method30, Method9, Method6, Method26, Method11, Method8, Method7, Method5, Method25, Method29, Method12, Method22, Method10, Method2, Method1, Method3, Method18, Method20, Method28, Method27, Method15, Method23, Method24, Method21, Method16, Method12, Method5, Method18, Method8, Method24, Method2, Method30, Method6, Method22, Method27, Method23, Method15, Method7, Method11, Method16, Method20, Method9, Method3, Method17, Method10, Method1, Method25, Method21, Method29, Method4, Method10, Method7, Method29, Method8, Method1, Method5, Method30, Method16, Method6, Method22, Method24, Method21, Method2, Method25, Method20, Method23, Method12, Method13, Method3, Method11, Method9, Method15, Method18, Method27, Method19, Method4, Method12, Method25, Method10, Method8, Method2, Method9, Method18, Method24, Method30, Method26, Method3, Method15, Method29, Method16, Method7, Method21, Method5, Method11, Method1, Method20, Method27, Method23, Method22, Method6, Method10, Method7, Method27, Method18, Method2, Method9, Method6, Method22, Method17, Method3, Method30, Method26, Method29, Method12, Method1, Method25, Method19, Method24, Method8, Method11, Method20, Method16, Method23, Method15, Method21, Method5, Method11, Method3, Method27, Method16, Method7, Method1, Method24, Method29, Method20, Method18, Method10, Method15, Method23, Method9, Method2, Method22, Method13, Method25, Method21, Method30, Method12, Method5, Method4, Method17, Method12, Method11, Method3, Method16, Method5, Method10, Method27, Method22, Method14, Method25, Method21, Method2, Method19, Method23, Method9, Method1, Method24, Method20, Method7, Method8, Method26, Method13, Method4, Method29, Method18, Method30, Method15, Method1, Method20, Method5, Method9, Method6, Method12, Method18, Method15, Method11, Method17, Method30, Method4, Method24, Method16, Method22, Method25, Method7, Method13, Method3, Method27, Method29, Method10, Method26, Method2, Method21, Method23, Method3, Method12, Method29, Method18, Method7, Method11, Method2, Method5, Method21, Method30, Method20, Method27, Method16, Method22, Method25, Method9, Method23, Method15, Method6, Method1, Method4, Method24, Method10, Method10, Method23, Method14, Method24, Method27, Method13, Method8, Method20, Method17, Method5, Method15, Method12, Method30, Method9, Method2, Method18, Method7, Method3, Method22, Method21, Method6, Method25, Method1, Method11, Method29, Method7, Method24, Method23, Method10, Method3, Method14, Method15, Method30, Method18, Method2, Method1, Method22, Method20, Method21, Method27, Method25, Method13, Method6, Method17, Method29, Method12, Method5, Method11, Method9, Method30, Method21, Method7, Method18, Method11, Method29, Method12, Method10, Method3, Method8, Method25, Method23, Method24, Method16, Method1, Method4, Method5, Method9, Method22, Method2, Method26, Method15, Method20, Method27, Method12, Method8, Method6, Method1, Method20, Method30, Method3, Method16, Method11, Method27, Method15, Method7, Method14, Method9, Method18, Method24, Method17, Method23, Method13, Method10, Method25, Method21, Method2, Method29, Method22, Method5, Method5, Method23, Method9, Method3, Method15, Method18, Method16, Method20, Method11, Method21, Method26, Method24, Method27, Method2, Method22, Method10, Method1, Method7, Method25, Method17, Method4, Method29, Method30, Method12, Method21, Method8, Method6, Method3, Method22, Method30, Method17, Method12, Method10, Method19, Method14, Method24, Method7, Method27, Method29, Method15, Method18, Method1, Method4, Method11, Method2, Method9, Method23, Method25, Method20, Method5, Method9, Method12, Method11, Method14, Method22, Method17, Method27, Method24, Method23, Method10, Method25, Method29, Method30, Method2, Method5, Method18, Method8, Method6, Method1, Method7, Method21, Method15, Method19, Method26, Method3, Method20, Method16, Method3, Method14, Method12, Method24, Method8, Method5, Method23, Method7, Method30, Method15, Method1, Method20, Method29, Method4, Method10, Method2, Method25, Method9, Method21, Method11, Method18, Method22, Method26, Method27, Method25, Method2, Method6, Method5, Method11, Method10, Method17, Method20, Method21, Method14, Method23, Method18, Method9, Method15, Method1, Method30, Method12, Method22, Method3, Method27, Method26, Method29, Method4, Method24, Method7