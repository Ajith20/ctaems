####### Ordered Logger #######


Final Duration: 1500.0
Final Quality: 0.0
Seed: 9680808853

##### Method Results #####


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 10


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 18


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 11


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 4


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 5


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 15


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 32.0
        With Duration: 5


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 8


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 9


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 3


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 4.0
        With Duration: 1


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 5


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 7


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 7


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 10.0
        With Duration: 6


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 14


    ## Method ##
        Name: Method11
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 3


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 8.0
        With Duration: 5


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 6


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 18


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 22.0
        With Duration: 7

##### Relationship Results #####


    ## Relationship ##
        Name: disables4
        Type: Disables
        Source : Method18
        Target : Method14


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method26


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method17


    ## Relationship ##
        Name: enables5
        Type: Enables
        Source : Method26
        Target : Method28


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method24
        Target : Method13


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method15
        Target : Method8


    ## Relationship ##
        Name: disables5
        Type: Disables
        Source : Method14
        Target : Method16

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 6445
        Received: 21815
        Completed: Method25, Method22, Method29, Method3, Method20, Method7, Method1, Method23, Method21, Method24, Method12, Method27, Method2, Method11, Method5, Method15, Method18, Method16, Method10, Method9, Method13, Method6, Method30, Method19, Method29, Method18, Method12, Method15, Method16, Method10, Method22, Method3, Method6, Method23, Method7, Method20, Method2, Method1, Method9, Method27, Method25, Method21, Method19, Method5, Method11, Method30, Method24, Method2, Method14, Method23, Method24, Method22, Method19, Method7, Method20, Method5, Method15, Method25, Method6, Method3, Method30, Method17, Method9, Method1, Method12, Method10, Method29, Method18, Method21, Method27, Method11, Method26, Method21, Method29, Method8, Method25, Method6, Method14, Method22, Method23, Method27, Method5, Method2, Method11, Method9, Method30, Method24, Method15, Method12, Method18, Method1, Method3, Method7, Method20, Method10, Method26, Method17, Method11, Method21, Method9, Method20, Method15, Method1, Method2, Method22, Method6, Method30, Method25, Method7, Method26, Method24, Method13, Method27, Method23, Method10, Method29, Method5, Method28, Method18, Method3, Method4, Method12, Method16, Method30, Method9, Method6, Method26, Method11, Method8, Method7, Method5, Method25, Method29, Method12, Method22, Method10, Method2, Method1, Method3, Method18, Method20, Method28, Method27, Method15, Method23, Method24, Method21, Method16, Method12, Method5, Method18, Method8, Method24, Method2, Method30, Method6, Method22, Method27, Method23, Method15, Method7, Method11, Method16, Method20, Method9, Method3, Method17, Method10, Method1, Method25, Method21, Method29, Method4, Method10, Method7, Method29, Method8, Method1, Method5, Method30, Method16, Method6, Method22, Method24, Method21, Method2, Method25, Method20, Method23, Method12, Method13, Method3, Method11, Method9, Method15, Method18, Method27, Method19, Method4, Method12, Method25, Method10, Method8, Method2, Method9, Method18, Method24, Method30, Method26, Method3, Method15, Method29, Method16, Method7, Method21, Method5, Method11, Method1, Method20, Method27, Method23, Method22, Method6, Method10, Method7, Method27, Method18, Method2, Method9, Method6, Method22, Method17, Method3, Method30, Method26, Method29, Method12, Method1, Method25, Method19, Method24, Method8, Method11, Method20, Method16, Method23, Method15, Method21, Method5