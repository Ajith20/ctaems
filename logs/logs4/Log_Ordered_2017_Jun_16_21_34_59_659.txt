####### Ordered Logger #######


Final Duration: 400.0
Final Quality: 0.0
Seed: 8716279286

##### Method Results #####


    ## Method ##
        Name: Method28
        Completed By : Agent1
        With Quality : 24.0
        With Duration: 4


    ## Method ##
        Name: Method10
        Completed By : Agent1
        With Quality : 20.0
        With Duration: 11


    ## Method ##
        Name: Method21
        Completed By : Agent1
        With Quality : 15.0
        With Duration: 15


    ## Method ##
        Name: Method18
        Completed By : Agent1
        With Quality : 26.0
        With Duration: 9


    ## Method ##
        Name: Method13
        Completed By : Agent1
        With Quality : 30.0
        With Duration: 18


    ## Method ##
        Name: Method9
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 6


    ## Method ##
        Name: Method5
        Completed By : Agent1
        With Quality : 21.0
        With Duration: 5


    ## Method ##
        Name: Method2
        Completed By : Agent1
        With Quality : 13.0
        With Duration: 12


    ## Method ##
        Name: Method30
        Completed By : Agent1
        With Quality : 3.0
        With Duration: 2


    ## Method ##
        Name: Method23
        Completed By : Agent1
        With Quality : 0.8
        With Duration: 3


    ## Method ##
        Name: Method8
        Completed By : Agent1
        With Quality : 31.0
        With Duration: 17


    ## Method ##
        Name: Method26
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 1


    ## Method ##
        Name: Method22
        Completed By : Agent1
        With Quality : 35.0
        With Duration: 5


    ## Method ##
        Name: Method7
        Completed By : Agent1
        With Quality : 18.0
        With Duration: 9


    ## Method ##
        Name: Method14
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 5


    ## Method ##
        Name: Method24
        Completed By : Agent1
        With Quality : 13.5
        With Duration: 4


    ## Method ##
        Name: Method12
        Completed By : Agent1
        With Quality : 6.0
        With Duration: 4


    ## Method ##
        Name: Method1
        Completed By : Agent1
        With Quality : 27.0
        With Duration: 2


    ## Method ##
        Name: Method29
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 5


    ## Method ##
        Name: Method3
        Completed By : Agent1
        With Quality : 2.0
        With Duration: 9


    ## Method ##
        Name: Method15
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method25
        Completed By : Agent1
        With Quality : 9.0
        With Duration: 5


    ## Method ##
        Name: Method6
        Completed By : Agent1
        With Quality : 12.0
        With Duration: 9


    ## Method ##
        Name: Method17
        Completed By : Agent1
        With Quality : 5.0
        With Duration: 1


    ## Method ##
        Name: Method19
        Completed By : Agent1
        With Quality : 1.0
        With Duration: 1


    ## Method ##
        Name: Method27
        Completed By : Agent1
        With Quality : 16.0
        With Duration: 17


    ## Method ##
        Name: Method16
        Completed By : Agent1
        With Quality : 7.0
        With Duration: 6


    ## Method ##
        Name: Method20
        Completed By : Agent1
        With Quality : 5.3999999999999995
        With Duration: 4

##### Relationship Results #####


    ## Relationship ##
        Name: hinders3
        Type: Hinders
        Source : Method28
        Target : Method12


    ## Relationship ##
        Name: facilitates2
        Type: Facilitates
        Source : Method18
        Target : Method10


    ## Relationship ##
        Name: hinders4
        Type: Hinders
        Source : Method21
        Target : Method20


    ## Relationship ##
        Name: facilitates3
        Type: Facilitates
        Source : Method13
        Target : Method30


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: enables2
        Type: Enables
        Source : Method9
        Target : Method25


    ## Relationship ##
        Name: hinders2
        Type: Hinders
        Source : Method5
        Target : Method23


    ## Relationship ##
        Name: hinders1
        Type: Hinders
        Source : Method2
        Target : Method15


    ## Relationship ##
        Name: facilitates1
        Type: Facilitates
        Source : Method7
        Target : Method8


    ## Relationship ##
        Name: facilitates4
        Type: Facilitates
        Source : Method26
        Target : Method24


    ## Relationship ##
        Name: enables4
        Type: Enables
        Source : Method22
        Target : Method19


    ## Relationship ##
        Name: disables3
        Type: Disables
        Source : Method29
        Target : Method14


    ## Relationship ##
        Name: enables1
        Type: Enables
        Source : Method1
        Target : Method4


    ## Relationship ##
        Name: disables2
        Type: Disables
        Source : Method16
        Target : Method3


    ## Relationship ##
        Name: disables1
        Type: Disables
        Source : Method4
        Target : Method6


    ## Relationship ##
        Name: enables3
        Type: Enables
        Source : Method27
        Target : Method11

##### Agent Results #####


    ## Agent ##
        Name Agent1
        Sent: 413
        Received: 1727
        Completed: Method14, Method21, Method20, Method28, Method5, Method30, Method2, Method18, Method29, Method10, Method17, Method8, Method16, Method9, Method6, Method27, Method15, Method1, Method24, Method26, Method23, Method7, Method12, Method13, Method11, Method22, Method25, Method12, Method6, Method20, Method23, Method16, Method13, Method26, Method29, Method21, Method8, Method22, Method27, Method28, Method10, Method21, Method18, Method13, Method9, Method5, Method2, Method30, Method23, Method8, Method26, Method22, Method7, Method14, Method24, Method12, Method1, Method29, Method3, Method15, Method25, Method6, Method17, Method19, Method27, Method16, Method20