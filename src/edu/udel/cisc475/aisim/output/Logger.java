package edu.udel.cisc475.aisim.output;

import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.LinkedList;

import edu.udel.cisc475.aisim.simulation.communication.Message;
import edu.udel.cisc475.aisim.simulation.simulationstate.Agent;
import edu.udel.cisc475.aisim.tasktree.Method;
import edu.udel.cisc475.aisim.tasktree.NodeRelationship;

public class Logger {
	

	/**
	 * Types of loggers being used
	 */
	protected SequentialLogger SequentialLog;
	protected OrderedLogger OrderedLog;
	
	public Logger(String fileDestination){
		
		long time = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MMM_dd_HH_mm_ss_SSS");
		Date resultdate = new Date(time);
		
		
		//moving date tag to the output directory.  removing here.
		SequentialLog = new SequentialLogger(fileDestination,"Log_Sequential_"+ sdf.format(resultdate) + ".txt");
		OrderedLog = new OrderedLogger(fileDestination,"Log_Ordered_"+ sdf.format(resultdate) + ".txt");
		
	}
		
	public void initializeLogger(String fileDestination){
			
		long time = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MMM_dd_HH_mm_ss_SSS");
		Date resultdate = new Date(time);
		
		SequentialLog = new SequentialLogger(fileDestination,"Log_Sequential_"+ sdf.format(resultdate) + ".txt");
		OrderedLog = new OrderedLogger(fileDestination,"Log_Ordered_"+ sdf.format(resultdate) + ".txt");
		
	}
	
	
	//shortcut compensation for earlier spelling error.
	public void initilizeLogger(String f){
		initializeLogger(f);
	}
	

	/**
	 * Log An Agent
	 * @param a The Agent to be logged
	 * @return
	 */
	public boolean logAgent(Agent a) {
		SequentialLog.logAgent(a);
		OrderedLog.logAgent(a);
		return true;

	}

	/**
	 * Log a Relationship
	 * @param nr the relationship to be logged
	 * @return
	 */
	public boolean logRelationship(NodeRelationship nr) {	
		if (!OrderedLog.getNodeRelations().contains(nr)) {
			SequentialLog.logRelationship(nr);
			OrderedLog.logRelationship(nr);
		}
		return true;
	}
	
	/**
	 * Log a Method
	 * @param m the method to be logged
	 * @return
	 */
	public boolean logMethod(Method m) {
		SequentialLog.logMethod(m);
		OrderedLog.logMethod(m);
		return true;
	}
	
	/**
	 * Log a Message
	 * @param m the Message to be logged
	 * @return
	 */
	public boolean logMessage(Message m) {
		SequentialLog.logMessage(m);
		return true;
	}
	
	public boolean logAll(double duration, double quality, long seed) {
		OrderedLog.logAll(duration, quality, seed);
		return true;
	}
	
	public String getPathToFiles(){
		return OrderedLog.getPathToFile() + "\n" +SequentialLog.getPathToFile();
	}
	


}
