package edu.udel.cisc475.aisim.simulation.simulationstate;
import java.util.ArrayList;

import edu.udel.cisc475.aisim.tasktree.Method;
import edu.udel.cisc475.aisim.tasktree.PreferenceOutcome;

public class QueueObject {
    Method method;
    ArrayList<PreferenceOutcome> prefs;
    long est;
    long deadline;

    public QueueObject(Method method, ArrayList<PreferenceOutcome> prefs, long est, long deadline) {
        this.method = method;
        this.prefs = prefs;
        this.est = est;
        this.deadline = deadline;
    }
    public void setMethod(Method method) {
        this.method = method;
    }
    public void setPrefs(ArrayList<PreferenceOutcome> prefs) {
        this.prefs = prefs;
    }
    public Method getMethod() {
        return this.method;
    }
    public ArrayList<PreferenceOutcome> getPrefs() {
        return this.prefs;
    }
    public long getEST() {
        return this.est;
    }
    public long getDeadline() {
        return this.deadline;
    }
}
