package edu.udel.cisc475.aisim.simulation.simulationstate;




/**
 * This class represents the environment and the state transistion of environment variables. 
 * Each agent is associated with a set of environmental variables and has preferences associated
 * with the instantiations.
 * 
 * @author Ajith Vemuri
 *
 */

public class Environment {
    private int tickLength;
    private String state;
    private int tickStart;
    private int tickEnd;

    public Environment(int tickLength, String state, int tickStart, int tickEnd) {
        this.tickLength = tickLength;
        this.state = state;
        this.tickStart = tickStart;
        this.tickEnd = tickEnd;
    }
    public Environment() { 
        tickLength = 0;
        state = null;
        tickStart = 0;
        tickEnd = 0;
    }
    public void setState(String state) {
        this.state = state;
    }
    public void settickLength(int tickLength) {
        this.tickLength = tickLength;
    }
    public void settickStart(int tickStart) {
        this.tickStart = tickStart;
    }
    public void settickEnd(int tickEnd) {
        this.tickEnd = tickEnd;
    }
    public int gettickLength() {
        return this.tickLength;
    }
    public String getstate() { 
        return this.state;
    }
    public int gettickStart() {
        return this.tickStart;
    }
    public int gettickEnd() { 
        return this.tickEnd;
    }
}

