// Generated from Ctaems.g4 by ANTLR 4.6
package edu.udel.cisc475.aisim.input;import java.util.StringTokenizer;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CtaemsParser}.
 */
public interface CtaemsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#labelParse}.
	 * @param ctx the parse tree
	 */
	void enterLabelParse(CtaemsParser.LabelParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#labelParse}.
	 * @param ctx the parse tree
	 */
	void exitLabelParse(CtaemsParser.LabelParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#agentParse}.
	 * @param ctx the parse tree
	 */
	void enterAgentParse(CtaemsParser.AgentParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#agentParse}.
	 * @param ctx the parse tree
	 */
	void exitAgentParse(CtaemsParser.AgentParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#parentsParse}.
	 * @param ctx the parse tree
	 */
	void enterParentsParse(CtaemsParser.ParentsParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#parentsParse}.
	 * @param ctx the parse tree
	 */
	void exitParentsParse(CtaemsParser.ParentsParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#preferencesParse}.
	 * @param ctx the parse tree
	 */
	void enterPreferencesParse(CtaemsParser.PreferencesParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#preferencesParse}.
	 * @param ctx the parse tree
	 */
	void exitPreferencesParse(CtaemsParser.PreferencesParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#distributionParse}.
	 * @param ctx the parse tree
	 */
	void enterDistributionParse(CtaemsParser.DistributionParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#distributionParse}.
	 * @param ctx the parse tree
	 */
	void exitDistributionParse(CtaemsParser.DistributionParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#frequencyParse}.
	 * @param ctx the parse tree
	 */
	void enterFrequencyParse(CtaemsParser.FrequencyParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#frequencyParse}.
	 * @param ctx the parse tree
	 */
	void exitFrequencyParse(CtaemsParser.FrequencyParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#densityParse}.
	 * @param ctx the parse tree
	 */
	void enterDensityParse(CtaemsParser.DensityParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#densityParse}.
	 * @param ctx the parse tree
	 */
	void exitDensityParse(CtaemsParser.DensityParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#qualityDistributionParse}.
	 * @param ctx the parse tree
	 */
	void enterQualityDistributionParse(CtaemsParser.QualityDistributionParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#qualityDistributionParse}.
	 * @param ctx the parse tree
	 */
	void exitQualityDistributionParse(CtaemsParser.QualityDistributionParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#durationDistributionParse}.
	 * @param ctx the parse tree
	 */
	void enterDurationDistributionParse(CtaemsParser.DurationDistributionParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#durationDistributionParse}.
	 * @param ctx the parse tree
	 */
	void exitDurationDistributionParse(CtaemsParser.DurationDistributionParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#versionParse}.
	 * @param ctx the parse tree
	 */
	void enterVersionParse(CtaemsParser.VersionParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#versionParse}.
	 * @param ctx the parse tree
	 */
	void exitVersionParse(CtaemsParser.VersionParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#earliestStartTimeParse}.
	 * @param ctx the parse tree
	 */
	void enterEarliestStartTimeParse(CtaemsParser.EarliestStartTimeParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#earliestStartTimeParse}.
	 * @param ctx the parse tree
	 */
	void exitEarliestStartTimeParse(CtaemsParser.EarliestStartTimeParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#deadlineParse}.
	 * @param ctx the parse tree
	 */
	void enterDeadlineParse(CtaemsParser.DeadlineParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#deadlineParse}.
	 * @param ctx the parse tree
	 */
	void exitDeadlineParse(CtaemsParser.DeadlineParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#subtaskParse}.
	 * @param ctx the parse tree
	 */
	void enterSubtaskParse(CtaemsParser.SubtaskParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#subtaskParse}.
	 * @param ctx the parse tree
	 */
	void exitSubtaskParse(CtaemsParser.SubtaskParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#qafParse}.
	 * @param ctx the parse tree
	 */
	void enterQafParse(CtaemsParser.QafParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#qafParse}.
	 * @param ctx the parse tree
	 */
	void exitQafParse(CtaemsParser.QafParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#pafParse}.
	 * @param ctx the parse tree
	 */
	void enterPafParse(CtaemsParser.PafParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#pafParse}.
	 * @param ctx the parse tree
	 */
	void exitPafParse(CtaemsParser.PafParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#qnumParse}.
	 * @param ctx the parse tree
	 */
	void enterQnumParse(CtaemsParser.QnumParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#qnumParse}.
	 * @param ctx the parse tree
	 */
	void exitQnumParse(CtaemsParser.QnumParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#fromParse}.
	 * @param ctx the parse tree
	 */
	void enterFromParse(CtaemsParser.FromParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#fromParse}.
	 * @param ctx the parse tree
	 */
	void exitFromParse(CtaemsParser.FromParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#toParse}.
	 * @param ctx the parse tree
	 */
	void enterToParse(CtaemsParser.ToParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#toParse}.
	 * @param ctx the parse tree
	 */
	void exitToParse(CtaemsParser.ToParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#weightParse}.
	 * @param ctx the parse tree
	 */
	void enterWeightParse(CtaemsParser.WeightParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#weightParse}.
	 * @param ctx the parse tree
	 */
	void exitWeightParse(CtaemsParser.WeightParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#qualityPowerParse}.
	 * @param ctx the parse tree
	 */
	void enterQualityPowerParse(CtaemsParser.QualityPowerParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#qualityPowerParse}.
	 * @param ctx the parse tree
	 */
	void exitQualityPowerParse(CtaemsParser.QualityPowerParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#preferencePowerParse}.
	 * @param ctx the parse tree
	 */
	void enterPreferencePowerParse(CtaemsParser.PreferencePowerParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#preferencePowerParse}.
	 * @param ctx the parse tree
	 */
	void exitPreferencePowerParse(CtaemsParser.PreferencePowerParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#durationPowerParse}.
	 * @param ctx the parse tree
	 */
	void enterDurationPowerParse(CtaemsParser.DurationPowerParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#durationPowerParse}.
	 * @param ctx the parse tree
	 */
	void exitDurationPowerParse(CtaemsParser.DurationPowerParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#spliceParse}.
	 * @param ctx the parse tree
	 */
	void enterSpliceParse(CtaemsParser.SpliceParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#spliceParse}.
	 * @param ctx the parse tree
	 */
	void exitSpliceParse(CtaemsParser.SpliceParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#timeParse}.
	 * @param ctx the parse tree
	 */
	void enterTimeParse(CtaemsParser.TimeParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#timeParse}.
	 * @param ctx the parse tree
	 */
	void exitTimeParse(CtaemsParser.TimeParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CtaemsParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(CtaemsParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CtaemsParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(CtaemsParser.ParseContext ctx);
}