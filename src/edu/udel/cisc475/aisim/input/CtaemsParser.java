// Generated from Ctaems.g4 by ANTLR 4.6
package edu.udel.cisc475.aisim.input;import java.util.StringTokenizer;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CtaemsParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		AgentToken=1, VersionToken=2, TaskGroupToken=3, TaskToken=4, MethodToken=5, 
		EnablesToken=6, FacilitatesToken=7, DisablesToken=8, HindersToken=9, SynchToken=10, 
		ConstraintsToken=11, Spaces=12, SpliceToken=13, TimeToken=14, EnviToken=15;
	public static final int
		RULE_labelParse = 0, RULE_agentParse = 1, RULE_parentsParse = 2, RULE_preferencesParse = 3, 
		RULE_distributionParse = 4, RULE_frequencyParse = 5, RULE_densityParse = 6, 
		RULE_qualityDistributionParse = 7, RULE_durationDistributionParse = 8, 
		RULE_versionParse = 9, RULE_earliestStartTimeParse = 10, RULE_deadlineParse = 11, 
		RULE_subtaskParse = 12, RULE_qafParse = 13, RULE_pafParse = 14, RULE_qnumParse = 15, 
		RULE_fromParse = 16, RULE_toParse = 17, RULE_weightParse = 18, RULE_qualityPowerParse = 19, 
		RULE_preferencePowerParse = 20, RULE_durationPowerParse = 21, RULE_spliceParse = 22, 
		RULE_timeParse = 23, RULE_parse = 24;
	public static final String[] ruleNames = {
		"labelParse", "agentParse", "parentsParse", "preferencesParse", "distributionParse", 
		"frequencyParse", "densityParse", "qualityDistributionParse", "durationDistributionParse", 
		"versionParse", "earliestStartTimeParse", "deadlineParse", "subtaskParse", 
		"qafParse", "pafParse", "qnumParse", "fromParse", "toParse", "weightParse", 
		"qualityPowerParse", "preferencePowerParse", "durationPowerParse", "spliceParse", 
		"timeParse", "parse"
	};

	private static final String[] _LITERAL_NAMES = {
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "AgentToken", "VersionToken", "TaskGroupToken", "TaskToken", "MethodToken", 
		"EnablesToken", "FacilitatesToken", "DisablesToken", "HindersToken", "SynchToken", 
		"ConstraintsToken", "Spaces", "SpliceToken", "TimeToken", "EnviToken"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Ctaems.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	String token = "notDone"; 
			 String labelParseString = "notDone";
			 String agentParseString = "notDone";
			 String densityParseString = "notDone";
			 String weightString = "notDone";
			 String versionParseString = "notDone";
			 String earliestStartTimeParseString = "notDone";
			 String deadlineParseString = "notDone";
			 String qafParseString = "notDone";
			 String qnumParseString = "notDone";
			 String toParseString = "notDone";
			 String fromParseString = "notDone";
			 String parentsParseString = "notDone";
			 String preferencesParseString = "notDone";
			 String distributionParseString = "notDone";
			 String frequencyParseString = "notDone";
			 String pafParseString = "notDone";
			 String parseType = "notNew";
			 int timeParseInt = 0;
			 String spliceString = "notDone";	 
			 boolean hasEST = false;
			 boolean hasDeadline = false;
			 double[] qualityDistributionArray;
			 double[] durationDistributionArray;
			 double[] enviDistributionArray;
			 double[] enviFrequencyArray;
			 double[] qualityPowerArray;
			 double[] durationPowerArray;
			 double[] prefPowerArray;
			 String[] subtaskArray;
			 String[] preferencesArray;
			 String[] enviStates;
			 String[] parentsArray;

	public CtaemsParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LabelParseContext extends ParserRuleContext {
		public Token AgentToken;
		public Token EnviToken;
		public Token EnablesToken;
		public Token DisablesToken;
		public Token FacilitatesToken;
		public Token HindersToken;
		public Token ConstraintsToken;
		public Token SynchToken;
		public Token TaskToken;
		public Token TaskGroupToken;
		public Token MethodToken;
		public TerminalNode AgentToken() { return getToken(CtaemsParser.AgentToken, 0); }
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public TerminalNode EnablesToken() { return getToken(CtaemsParser.EnablesToken, 0); }
		public TerminalNode DisablesToken() { return getToken(CtaemsParser.DisablesToken, 0); }
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode ConstraintsToken() { return getToken(CtaemsParser.ConstraintsToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public TerminalNode TaskGroupToken() { return getToken(CtaemsParser.TaskGroupToken, 0); }
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public LabelParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labelParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterLabelParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitLabelParse(this);
		}
	}

	public final LabelParseContext labelParse() throws RecognitionException {
		LabelParseContext _localctx = new LabelParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_labelParse);
		try {
			setState(72);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AgentToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(50);
				((LabelParseContext)_localctx).AgentToken = match(AgentToken);
				labelParseString = (((LabelParseContext)_localctx).AgentToken!=null?((LabelParseContext)_localctx).AgentToken.getText():null);
				    			if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				    			else{
				  					int endIndex = labelParseString.indexOf(')'); 
				  					labelParseString = labelParseString.substring(21,endIndex);}
				  				
				}
				break;
			case EnviToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(52);
				((LabelParseContext)_localctx).EnviToken = match(EnviToken);
				labelParseString = (((LabelParseContext)_localctx).EnviToken!=null?((LabelParseContext)_localctx).EnviToken.getText():null);
								if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
								else{
								labelParseString = labelParseString.substring(1);
								int startIndex = labelParseString.indexOf('(');
								int endIndex = labelParseString.indexOf(')');
								labelParseString = labelParseString.substring(startIndex,endIndex);
								startIndex = labelParseString.indexOf(' ')+1;
								labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case EnablesToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(54);
				((LabelParseContext)_localctx).EnablesToken = match(EnablesToken);
				labelParseString = (((LabelParseContext)_localctx).EnablesToken!=null?((LabelParseContext)_localctx).EnablesToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case DisablesToken:
				enterOuterAlt(_localctx, 4);
				{
				setState(56);
				((LabelParseContext)_localctx).DisablesToken = match(DisablesToken);
				labelParseString = (((LabelParseContext)_localctx).DisablesToken!=null?((LabelParseContext)_localctx).DisablesToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case FacilitatesToken:
				enterOuterAlt(_localctx, 5);
				{
				setState(58);
				((LabelParseContext)_localctx).FacilitatesToken = match(FacilitatesToken);
				labelParseString = (((LabelParseContext)_localctx).FacilitatesToken!=null?((LabelParseContext)_localctx).FacilitatesToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 6);
				{
				setState(60);
				((LabelParseContext)_localctx).HindersToken = match(HindersToken);
				labelParseString = (((LabelParseContext)_localctx).HindersToken!=null?((LabelParseContext)_localctx).HindersToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case ConstraintsToken:
				enterOuterAlt(_localctx, 7);
				{
				setState(62);
				((LabelParseContext)_localctx).ConstraintsToken = match(ConstraintsToken);
				labelParseString = (((LabelParseContext)_localctx).ConstraintsToken!=null?((LabelParseContext)_localctx).ConstraintsToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 8);
				{
				setState(64);
				((LabelParseContext)_localctx).SynchToken = match(SynchToken);
				labelParseString = (((LabelParseContext)_localctx).SynchToken!=null?((LabelParseContext)_localctx).SynchToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case TaskToken:
				enterOuterAlt(_localctx, 9);
				{
				setState(66);
				((LabelParseContext)_localctx).TaskToken = match(TaskToken);
				labelParseString = (((LabelParseContext)_localctx).TaskToken!=null?((LabelParseContext)_localctx).TaskToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case TaskGroupToken:
				enterOuterAlt(_localctx, 10);
				{
				setState(68);
				((LabelParseContext)_localctx).TaskGroupToken = match(TaskGroupToken);
				labelParseString = (((LabelParseContext)_localctx).TaskGroupToken!=null?((LabelParseContext)_localctx).TaskGroupToken.getText():null);
				  				  if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				  else{
				  				  labelParseString = labelParseString.substring(1);
				  				  int startIndex = labelParseString.indexOf('(');
				  				  int endIndex = labelParseString.indexOf(')');
				  				  labelParseString = labelParseString.substring(startIndex,endIndex);
				  				  startIndex = labelParseString.indexOf(' ')+1;
				  				  labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			case MethodToken:
				enterOuterAlt(_localctx, 11);
				{
				setState(70);
				((LabelParseContext)_localctx).MethodToken = match(MethodToken);
				labelParseString = (((LabelParseContext)_localctx).MethodToken!=null?((LabelParseContext)_localctx).MethodToken.getText():null);
				  				 if(labelParseString.contains("(label)")){labelParseString = "ERROR";}
				  				 else{
				  				 labelParseString = labelParseString.substring(1);
				  				 int startIndex = labelParseString.indexOf('(');
				  				 int endIndex = labelParseString.indexOf(')');
				  				 labelParseString = labelParseString.substring(startIndex,endIndex);
				  				 startIndex = labelParseString.indexOf(' ')+1;
				  				 labelParseString = labelParseString.substring(startIndex);}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AgentParseContext extends ParserRuleContext {
		public Token MethodToken;
		public Token EnviToken;
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public AgentParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_agentParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterAgentParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitAgentParse(this);
		}
	}

	public final AgentParseContext agentParse() throws RecognitionException {
		AgentParseContext _localctx = new AgentParseContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_agentParse);
		try {
			setState(78);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MethodToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(74);
				((AgentParseContext)_localctx).MethodToken = match(MethodToken);
				agentParseString = (((AgentParseContext)_localctx).MethodToken!=null?((AgentParseContext)_localctx).MethodToken.getText():null);
								if(agentParseString.contains("(agent)")){agentParseString = "ERROR";}
								else{
						  		 String omitString = "(agent";
								 int startIndex = agentParseString.indexOf(omitString);
								 agentParseString = agentParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = agentParseString.indexOf(')');
								 agentParseString = agentParseString.substring(startIndex,endIndex);}
				}
				break;
			case EnviToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(76);
				((AgentParseContext)_localctx).EnviToken = match(EnviToken);
				agentParseString = (((AgentParseContext)_localctx).EnviToken!=null?((AgentParseContext)_localctx).EnviToken.getText():null);
								if(agentParseString.contains("(agent)")){agentParseString = "ERROR";}
								else{
						  		 String omitString = "(agent";
								 int startIndex = agentParseString.indexOf(omitString);
								 agentParseString = agentParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = agentParseString.indexOf(')');
								 agentParseString = agentParseString.substring(startIndex,endIndex);}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParentsParseContext extends ParserRuleContext {
		public Token EnviToken;
		public Token MethodToken;
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public ParentsParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parentsParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterParentsParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitParentsParse(this);
		}
	}

	public final ParentsParseContext parentsParse() throws RecognitionException {
		ParentsParseContext _localctx = new ParentsParseContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_parentsParse);
		try {
			setState(84);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EnviToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(80);
				((ParentsParseContext)_localctx).EnviToken = match(EnviToken);
				parentsParseString = (((ParentsParseContext)_localctx).EnviToken!=null?((ParentsParseContext)_localctx).EnviToken.getText():null);
								String omitString = "(parents";
								int startIndex = parentsParseString.indexOf(omitString);
								parentsParseString = parentsParseString.substring(startIndex);
								startIndex = omitString.length()+1;		 
								int endIndex = parentsParseString.indexOf(')');
								parentsParseString = parentsParseString.substring(startIndex,endIndex);
								StringTokenizer tokenizer = new StringTokenizer(parentsParseString);
				    		  	int size = tokenizer.countTokens();
				              	parentsArray = new String[size];
				    	      	for(int i = 0;i < size;i++)
				        	  	parentsArray[i] = (String)tokenizer.nextElement();
				}
				break;
			case MethodToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(82);
				((ParentsParseContext)_localctx).MethodToken = match(MethodToken);
				parentsParseString = (((ParentsParseContext)_localctx).MethodToken!=null?((ParentsParseContext)_localctx).MethodToken.getText():null);
								String omitString = "(parents";
								int startIndex = parentsParseString.indexOf(omitString);
								parentsParseString = parentsParseString.substring(startIndex);
								startIndex = omitString.length()+1;		 
								int endIndex = parentsParseString.indexOf(')');
								parentsParseString = parentsParseString.substring(startIndex,endIndex);
								StringTokenizer tokenizer = new StringTokenizer(parentsParseString);
				    		  	int size = tokenizer.countTokens();
				              	parentsArray = new String[size];
				    	      	for(int i = 0;i < size;i++)
				        	  	parentsArray[i] = (String)tokenizer.nextElement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreferencesParseContext extends ParserRuleContext {
		public Token EnviToken;
		public Token MethodToken;
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public PreferencesParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preferencesParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterPreferencesParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitPreferencesParse(this);
		}
	}

	public final PreferencesParseContext preferencesParse() throws RecognitionException {
		PreferencesParseContext _localctx = new PreferencesParseContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_preferencesParse);
		try {
			setState(90);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EnviToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(86);
				((PreferencesParseContext)_localctx).EnviToken = match(EnviToken);
				preferencesParseString = (((PreferencesParseContext)_localctx).EnviToken!=null?((PreferencesParseContext)_localctx).EnviToken.getText():null);
								String omitString = "(preferences";
								int startIndex = preferencesParseString.indexOf(omitString);
								preferencesParseString = preferencesParseString.substring(startIndex);
								startIndex = omitString.length()+1;		 
								int endIndex = preferencesParseString.indexOf(')');
								preferencesParseString = preferencesParseString.substring(startIndex,endIndex);
								preferencesArray = preferencesParseString.split("\\s+");
				}
				break;
			case MethodToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(88);
				((PreferencesParseContext)_localctx).MethodToken = match(MethodToken);
				preferencesParseString = (((PreferencesParseContext)_localctx).MethodToken!=null?((PreferencesParseContext)_localctx).MethodToken.getText():null);
								String omitString = "(preferences";
								int startIndex = preferencesParseString.indexOf(omitString);
								preferencesParseString = preferencesParseString.substring(startIndex);
								startIndex = omitString.length()+1;		 
								int endIndex = preferencesParseString.indexOf(')');
								preferencesParseString = preferencesParseString.substring(startIndex,endIndex);
								preferencesArray = preferencesParseString.split("\\s+");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DistributionParseContext extends ParserRuleContext {
		public Token EnviToken;
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public DistributionParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_distributionParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterDistributionParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitDistributionParse(this);
		}
	}

	public final DistributionParseContext distributionParse() throws RecognitionException {
		DistributionParseContext _localctx = new DistributionParseContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_distributionParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			((DistributionParseContext)_localctx).EnviToken = match(EnviToken);
			String enviParseString = (((DistributionParseContext)_localctx).EnviToken!=null?((DistributionParseContext)_localctx).EnviToken.getText():null);
							 String omitString = "(distribution";				 
							 int startIndex = enviParseString.indexOf(omitString);
							 enviParseString = enviParseString.substring(startIndex);
							 startIndex = omitString.length()+1;		 
							 int endIndex = enviParseString.indexOf(')');
							 enviParseString = enviParseString.substring(startIndex,endIndex);
							 String[] enviStatesString = enviParseString.split("\\s+");
			    			 int size = enviStatesString.length / 2 ;
			                 enviDistributionArray = new double[size];
							 enviStates = new String[size];
			    			 for(int i = 0;i < size ;i = i + 1) {
							 	enviStates[i] = enviStatesString[i*2];
			        		 	enviDistributionArray[i] = Double.parseDouble(enviStatesString[(i*2)+1]);}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrequencyParseContext extends ParserRuleContext {
		public Token EnviToken;
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public FrequencyParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frequencyParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterFrequencyParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitFrequencyParse(this);
		}
	}

	public final FrequencyParseContext frequencyParse() throws RecognitionException {
		FrequencyParseContext _localctx = new FrequencyParseContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_frequencyParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			((FrequencyParseContext)_localctx).EnviToken = match(EnviToken);
			String enviParseString = (((FrequencyParseContext)_localctx).EnviToken!=null?((FrequencyParseContext)_localctx).EnviToken.getText():null);
							 String omitString = "(frequency";				 
							 int startIndex = enviParseString.indexOf(omitString);
							 enviParseString = enviParseString.substring(startIndex);
							 startIndex = omitString.length()+1;		 
							 int endIndex = enviParseString.indexOf(')');
							 enviParseString = enviParseString.substring(startIndex,endIndex);
							 String[] enviStatesString = enviParseString.split("\\s+");
			    			 int size = enviStatesString.length / 2 ;
			                 enviFrequencyArray = new double[size];
							 enviStates = new String[size];
			    			 for(int i = 0;i < size ;i = i + 1) {
							 	enviStates[i] = enviStatesString[i*2];
			        		 	enviFrequencyArray[i] = Double.parseDouble(enviStatesString[(i*2)+1]);}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DensityParseContext extends ParserRuleContext {
		public Token MethodToken;
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public DensityParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_densityParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterDensityParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitDensityParse(this);
		}
	}

	public final DensityParseContext densityParse() throws RecognitionException {
		DensityParseContext _localctx = new DensityParseContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_densityParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			((DensityParseContext)_localctx).MethodToken = match(MethodToken);
			densityParseString = (((DensityParseContext)_localctx).MethodToken!=null?((DensityParseContext)_localctx).MethodToken.getText():null);
							 String omitString = "(density";				 
							 int startIndex = densityParseString.indexOf(omitString);
							 densityParseString = densityParseString.substring(startIndex);
							 startIndex = omitString.length()+1;		 
							 int endIndex = densityParseString.indexOf(')');
							 densityParseString = densityParseString.substring(startIndex,endIndex);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualityDistributionParseContext extends ParserRuleContext {
		public Token MethodToken;
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public QualityDistributionParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualityDistributionParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterQualityDistributionParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitQualityDistributionParse(this);
		}
	}

	public final QualityDistributionParseContext qualityDistributionParse() throws RecognitionException {
		QualityDistributionParseContext _localctx = new QualityDistributionParseContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_qualityDistributionParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			((QualityDistributionParseContext)_localctx).MethodToken = match(MethodToken);
			String qualityParseString = (((QualityDistributionParseContext)_localctx).MethodToken!=null?((QualityDistributionParseContext)_localctx).MethodToken.getText():null);
							 String omitString = "(quality_distribution";				 
							 int startIndex = qualityParseString.indexOf(omitString);
							 qualityParseString = qualityParseString.substring(startIndex);
							 startIndex = omitString.length()+1;		 
							 int endIndex = qualityParseString.indexOf(')');
							 qualityParseString = qualityParseString.substring(startIndex,endIndex);
							 StringTokenizer tokenizer = new StringTokenizer(qualityParseString);
			    			 int size = tokenizer.countTokens();
			                 qualityDistributionArray = new double[size];
			    			 for(int i = 0;i < size;i++)
			        		 	qualityDistributionArray[i] = Double.parseDouble((String)tokenizer.nextElement());
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DurationDistributionParseContext extends ParserRuleContext {
		public Token MethodToken;
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public DurationDistributionParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_durationDistributionParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterDurationDistributionParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitDurationDistributionParse(this);
		}
	}

	public final DurationDistributionParseContext durationDistributionParse() throws RecognitionException {
		DurationDistributionParseContext _localctx = new DurationDistributionParseContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_durationDistributionParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			((DurationDistributionParseContext)_localctx).MethodToken = match(MethodToken);
			String durationParseString = (((DurationDistributionParseContext)_localctx).MethodToken!=null?((DurationDistributionParseContext)_localctx).MethodToken.getText():null);
							 String omitString = "(duration_distribution";				 
							 int startIndex = durationParseString.indexOf(omitString);
							 durationParseString = durationParseString.substring(startIndex);
							 startIndex = omitString.length()+1;		 
							 int endIndex = durationParseString.indexOf(')');
							 durationParseString = durationParseString.substring(startIndex,endIndex);
							 StringTokenizer tokenizer = new StringTokenizer(durationParseString);
			    			 int size = tokenizer.countTokens();
			                 durationDistributionArray = new double[size];
			    			 for(int i = 0;i < size;i++)
			        		 	durationDistributionArray[i] = Double.parseDouble((String)tokenizer.nextElement());
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VersionParseContext extends ParserRuleContext {
		public Token VersionToken;
		public TerminalNode VersionToken() { return getToken(CtaemsParser.VersionToken, 0); }
		public VersionParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_versionParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterVersionParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitVersionParse(this);
		}
	}

	public final VersionParseContext versionParse() throws RecognitionException {
		VersionParseContext _localctx = new VersionParseContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_versionParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			((VersionParseContext)_localctx).VersionToken = match(VersionToken);
			versionParseString = (((VersionParseContext)_localctx).VersionToken!=null?((VersionParseContext)_localctx).VersionToken.getText():null);
							 String omitString = "(spec_version";
							 int startIndex = omitString.length()+1;		 
							 int endIndex = versionParseString.indexOf(')');
							 versionParseString = versionParseString.substring(startIndex,endIndex);
							 if(versionParseString.charAt(0)=='"' && versionParseString.charAt(versionParseString.length()-1) =='"'){
							 	versionParseString = versionParseString.substring(1,versionParseString.length()-1);
							 }
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EarliestStartTimeParseContext extends ParserRuleContext {
		public Token TaskToken;
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public EarliestStartTimeParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_earliestStartTimeParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterEarliestStartTimeParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitEarliestStartTimeParse(this);
		}
	}

	public final EarliestStartTimeParseContext earliestStartTimeParse() throws RecognitionException {
		EarliestStartTimeParseContext _localctx = new EarliestStartTimeParseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_earliestStartTimeParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			((EarliestStartTimeParseContext)_localctx).TaskToken = match(TaskToken);
			earliestStartTimeParseString = (((EarliestStartTimeParseContext)_localctx).TaskToken!=null?((EarliestStartTimeParseContext)_localctx).TaskToken.getText():null);
								 String omitString = "(earliest_start_time";
								 if((((EarliestStartTimeParseContext)_localctx).TaskToken!=null?((EarliestStartTimeParseContext)_localctx).TaskToken.getText():null).contains(omitString)){				 
								 	int startIndex = earliestStartTimeParseString.indexOf(omitString);
								 	earliestStartTimeParseString = earliestStartTimeParseString.substring(startIndex);
								 	startIndex = omitString.length()+1;		 
								 	int endIndex = earliestStartTimeParseString.indexOf(')');
								 	earliestStartTimeParseString = earliestStartTimeParseString.substring(startIndex,endIndex);
								 	hasEST = true;}
								 else
								 	hasEST = false;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeadlineParseContext extends ParserRuleContext {
		public Token TaskToken;
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public DeadlineParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deadlineParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterDeadlineParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitDeadlineParse(this);
		}
	}

	public final DeadlineParseContext deadlineParse() throws RecognitionException {
		DeadlineParseContext _localctx = new DeadlineParseContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_deadlineParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			((DeadlineParseContext)_localctx).TaskToken = match(TaskToken);
			deadlineParseString = (((DeadlineParseContext)_localctx).TaskToken!=null?((DeadlineParseContext)_localctx).TaskToken.getText():null);
						  String omitString = "(deadline";
						  if((((DeadlineParseContext)_localctx).TaskToken!=null?((DeadlineParseContext)_localctx).TaskToken.getText():null).contains(omitString)){				 
				   	 	  	int startIndex = deadlineParseString.indexOf(omitString);
							deadlineParseString = deadlineParseString.substring(startIndex);
							startIndex = omitString.length()+1;		 
							int endIndex = deadlineParseString.indexOf(')');
							deadlineParseString = deadlineParseString.substring(startIndex,endIndex);
							hasDeadline = true;}
						  else
							hasDeadline = false;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubtaskParseContext extends ParserRuleContext {
		public Token TaskToken;
		public Token TaskGroupToken;
		public Token SpliceToken;
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public TerminalNode TaskGroupToken() { return getToken(CtaemsParser.TaskGroupToken, 0); }
		public TerminalNode SpliceToken() { return getToken(CtaemsParser.SpliceToken, 0); }
		public SubtaskParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subtaskParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterSubtaskParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitSubtaskParse(this);
		}
	}

	public final SubtaskParseContext subtaskParse() throws RecognitionException {
		SubtaskParseContext _localctx = new SubtaskParseContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_subtaskParse);
		try {
			setState(122);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TaskToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(116);
				((SubtaskParseContext)_localctx).TaskToken = match(TaskToken);
				String subtaskParseString = (((SubtaskParseContext)_localctx).TaskToken!=null?((SubtaskParseContext)_localctx).TaskToken.getText():null);
							  String omitString = "(subtasks";				 
					   	 	  int startIndex = subtaskParseString.indexOf(omitString);
							  subtaskParseString = subtaskParseString.substring(startIndex);
							  startIndex = omitString.length()+1;		 
							  int endIndex = subtaskParseString.indexOf(')');
							  subtaskParseString = subtaskParseString.substring(startIndex,endIndex);
							  StringTokenizer tokenizer = new StringTokenizer(subtaskParseString);
				    		  int size = tokenizer.countTokens();
				              subtaskArray = new String[size];
				    	      for(int i = 0;i < size;i++)
				        	  	 subtaskArray[i] = (String)tokenizer.nextElement();
				}
				break;
			case TaskGroupToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(118);
				((SubtaskParseContext)_localctx).TaskGroupToken = match(TaskGroupToken);
				String subtaskParseString = (((SubtaskParseContext)_localctx).TaskGroupToken!=null?((SubtaskParseContext)_localctx).TaskGroupToken.getText():null);
							  String omitString = "(subtasks";				 
					   	 	  int startIndex = subtaskParseString.indexOf(omitString);
							  subtaskParseString = subtaskParseString.substring(startIndex);
							  startIndex = omitString.length()+1;		 
							  int endIndex = subtaskParseString.indexOf(')');
							  subtaskParseString = subtaskParseString.substring(startIndex,endIndex);
							  StringTokenizer tokenizer = new StringTokenizer(subtaskParseString);
				    		  int size = tokenizer.countTokens();
				              subtaskArray = new String[size];
				    	      for(int i = 0;i < size;i++)
				        	  	 subtaskArray[i] = (String)tokenizer.nextElement();
				}
				break;
			case SpliceToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(120);
				((SubtaskParseContext)_localctx).SpliceToken = match(SpliceToken);
				String subtaskParseString = (((SubtaskParseContext)_localctx).SpliceToken!=null?((SubtaskParseContext)_localctx).SpliceToken.getText():null);
							  String omitString = "(subtasks";				 
					   	 	  int startIndex = subtaskParseString.indexOf(omitString);
							  subtaskParseString = subtaskParseString.substring(startIndex);
							  startIndex = omitString.length()+1;		 
							  int endIndex = subtaskParseString.indexOf(')');
							  subtaskParseString = subtaskParseString.substring(startIndex,endIndex);
							  StringTokenizer tokenizer = new StringTokenizer(subtaskParseString);
				    		  int size = tokenizer.countTokens();
				              subtaskArray = new String[size];
				    	      for(int i = 0;i < size;i++)
				        	  	 subtaskArray[i] = (String)tokenizer.nextElement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QafParseContext extends ParserRuleContext {
		public Token TaskToken;
		public Token TaskGroupToken;
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public TerminalNode TaskGroupToken() { return getToken(CtaemsParser.TaskGroupToken, 0); }
		public QafParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qafParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterQafParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitQafParse(this);
		}
	}

	public final QafParseContext qafParse() throws RecognitionException {
		QafParseContext _localctx = new QafParseContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_qafParse);
		try {
			setState(128);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TaskToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(124);
				((QafParseContext)_localctx).TaskToken = match(TaskToken);
				qafParseString = (((QafParseContext)_localctx).TaskToken!=null?((QafParseContext)_localctx).TaskToken.getText():null);
						  String omitString = "(qaf";				 
					   	  int startIndex = qafParseString.indexOf(omitString);
						  qafParseString = qafParseString.substring(startIndex);
						  startIndex = omitString.length()+1;		 
						  int endIndex = qafParseString.indexOf(')');
						  qafParseString = qafParseString.substring(startIndex,endIndex);
				}
				break;
			case TaskGroupToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(126);
				((QafParseContext)_localctx).TaskGroupToken = match(TaskGroupToken);
				qafParseString = (((QafParseContext)_localctx).TaskGroupToken!=null?((QafParseContext)_localctx).TaskGroupToken.getText():null);
						  String omitString = "(qaf";				 
					   	  int startIndex = qafParseString.indexOf(omitString);
						  qafParseString = qafParseString.substring(startIndex);
						  startIndex = omitString.length()+1;		 
						  int endIndex = qafParseString.indexOf(')');
						  qafParseString = qafParseString.substring(startIndex,endIndex);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PafParseContext extends ParserRuleContext {
		public Token TaskToken;
		public Token TaskGroupToken;
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public TerminalNode TaskGroupToken() { return getToken(CtaemsParser.TaskGroupToken, 0); }
		public PafParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pafParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterPafParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitPafParse(this);
		}
	}

	public final PafParseContext pafParse() throws RecognitionException {
		PafParseContext _localctx = new PafParseContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_pafParse);
		try {
			setState(134);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TaskToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(130);
				((PafParseContext)_localctx).TaskToken = match(TaskToken);
				pafParseString = (((PafParseContext)_localctx).TaskToken!=null?((PafParseContext)_localctx).TaskToken.getText():null);
						  String omitString = "(paf";				 
					   	  int startIndex = pafParseString.indexOf(omitString);
						  pafParseString = pafParseString.substring(startIndex);
						  startIndex = omitString.length()+1;		 
						  int endIndex = pafParseString.indexOf(')');
						  pafParseString = pafParseString.substring(startIndex,endIndex);
				}
				break;
			case TaskGroupToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(132);
				((PafParseContext)_localctx).TaskGroupToken = match(TaskGroupToken);
				pafParseString = (((PafParseContext)_localctx).TaskGroupToken!=null?((PafParseContext)_localctx).TaskGroupToken.getText():null);
						  String omitString = "(paf";				 
					   	  int startIndex = pafParseString.indexOf(omitString);
						  pafParseString = pafParseString.substring(startIndex);
						  startIndex = omitString.length()+1;		 
						  int endIndex = pafParseString.indexOf(')');
						  pafParseString = pafParseString.substring(startIndex,endIndex);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QnumParseContext extends ParserRuleContext {
		public Token TaskToken;
		public Token TaskGroupToken;
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public TerminalNode TaskGroupToken() { return getToken(CtaemsParser.TaskGroupToken, 0); }
		public QnumParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qnumParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterQnumParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitQnumParse(this);
		}
	}

	public final QnumParseContext qnumParse() throws RecognitionException {
		QnumParseContext _localctx = new QnumParseContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_qnumParse);
		try {
			setState(140);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TaskToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(136);
				((QnumParseContext)_localctx).TaskToken = match(TaskToken);
				qnumParseString = (((QnumParseContext)_localctx).TaskToken!=null?((QnumParseContext)_localctx).TaskToken.getText():null);
						  String omitString = "(qnum";				 
					   	  int startIndex = qnumParseString.indexOf(omitString);
						  qnumParseString = qnumParseString.substring(startIndex);
						  startIndex = omitString.length()+1;		 
						  int endIndex = qnumParseString.indexOf(')');
						  qnumParseString = qnumParseString.substring(startIndex,endIndex);
				}
				break;
			case TaskGroupToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(138);
				((QnumParseContext)_localctx).TaskGroupToken = match(TaskGroupToken);
				qnumParseString = (((QnumParseContext)_localctx).TaskGroupToken!=null?((QnumParseContext)_localctx).TaskGroupToken.getText():null);
						  String omitString = "(qnum";				 
					   	  int startIndex = qnumParseString.indexOf(omitString);
						  qnumParseString = qnumParseString.substring(startIndex);
						  startIndex = omitString.length()+1;		 
						  int endIndex = qnumParseString.indexOf(')');
						  qnumParseString = qnumParseString.substring(startIndex,endIndex);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FromParseContext extends ParserRuleContext {
		public Token FacilitatesToken;
		public Token HindersToken;
		public Token ConstraintsToken;
		public Token EnablesToken;
		public Token DisablesToken;
		public Token SynchToken;
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode ConstraintsToken() { return getToken(CtaemsParser.ConstraintsToken, 0); }
		public TerminalNode EnablesToken() { return getToken(CtaemsParser.EnablesToken, 0); }
		public TerminalNode DisablesToken() { return getToken(CtaemsParser.DisablesToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public FromParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fromParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterFromParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitFromParse(this);
		}
	}

	public final FromParseContext fromParse() throws RecognitionException {
		FromParseContext _localctx = new FromParseContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_fromParse);
		try {
			setState(154);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FacilitatesToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(142);
				((FromParseContext)_localctx).FacilitatesToken = match(FacilitatesToken);
				fromParseString = (((FromParseContext)_localctx).FacilitatesToken!=null?((FromParseContext)_localctx).FacilitatesToken.getText():null);
						  		 String omitString = "(from";
								 int startIndex = fromParseString.indexOf(omitString);
								 fromParseString = fromParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = fromParseString.indexOf(')');
								 fromParseString = fromParseString.substring(startIndex,endIndex);
				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(144);
				((FromParseContext)_localctx).HindersToken = match(HindersToken);
				fromParseString = (((FromParseContext)_localctx).HindersToken!=null?((FromParseContext)_localctx).HindersToken.getText():null);
						  		 String omitString = "(from";
								 int startIndex = fromParseString.indexOf(omitString);
								 fromParseString = fromParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = fromParseString.indexOf(')');
								 fromParseString = fromParseString.substring(startIndex,endIndex);
				}
				break;
			case ConstraintsToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(146);
				((FromParseContext)_localctx).ConstraintsToken = match(ConstraintsToken);
				fromParseString = (((FromParseContext)_localctx).ConstraintsToken!=null?((FromParseContext)_localctx).ConstraintsToken.getText():null);
						  		 String omitString = "(from";
								 int startIndex = fromParseString.indexOf(omitString);
								 fromParseString = fromParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = fromParseString.indexOf(')');
								 fromParseString = fromParseString.substring(startIndex,endIndex);
				}
				break;
			case EnablesToken:
				enterOuterAlt(_localctx, 4);
				{
				setState(148);
				((FromParseContext)_localctx).EnablesToken = match(EnablesToken);
				fromParseString = (((FromParseContext)_localctx).EnablesToken!=null?((FromParseContext)_localctx).EnablesToken.getText():null);
						  		 String omitString = "(from";
								 int startIndex = fromParseString.indexOf(omitString);
								 fromParseString = fromParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = fromParseString.indexOf(')');
								 fromParseString = fromParseString.substring(startIndex,endIndex);
				}
				break;
			case DisablesToken:
				enterOuterAlt(_localctx, 5);
				{
				setState(150);
				((FromParseContext)_localctx).DisablesToken = match(DisablesToken);
				fromParseString = (((FromParseContext)_localctx).DisablesToken!=null?((FromParseContext)_localctx).DisablesToken.getText():null);
						  		 String omitString = "(from";
								 int startIndex = fromParseString.indexOf(omitString);
								 fromParseString = fromParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = fromParseString.indexOf(')');
								 fromParseString = fromParseString.substring(startIndex,endIndex);
				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 6);
				{
				setState(152);
				((FromParseContext)_localctx).SynchToken = match(SynchToken);
				fromParseString = (((FromParseContext)_localctx).SynchToken!=null?((FromParseContext)_localctx).SynchToken.getText():null);
						  		 String omitString = "(from";
								 int startIndex = fromParseString.indexOf(omitString);
								 fromParseString = fromParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = fromParseString.indexOf(')');
								 fromParseString = fromParseString.substring(startIndex,endIndex);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ToParseContext extends ParserRuleContext {
		public Token FacilitatesToken;
		public Token HindersToken;
		public Token ConstraintsToken;
		public Token EnablesToken;
		public Token DisablesToken;
		public Token SynchToken;
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode ConstraintsToken() { return getToken(CtaemsParser.ConstraintsToken, 0); }
		public TerminalNode EnablesToken() { return getToken(CtaemsParser.EnablesToken, 0); }
		public TerminalNode DisablesToken() { return getToken(CtaemsParser.DisablesToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public ToParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_toParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterToParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitToParse(this);
		}
	}

	public final ToParseContext toParse() throws RecognitionException {
		ToParseContext _localctx = new ToParseContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_toParse);
		try {
			setState(168);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FacilitatesToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(156);
				((ToParseContext)_localctx).FacilitatesToken = match(FacilitatesToken);
				toParseString = (((ToParseContext)_localctx).FacilitatesToken!=null?((ToParseContext)_localctx).FacilitatesToken.getText():null);
						  		 String omitString = "(to";
								 int startIndex = toParseString.indexOf(omitString);
								 toParseString = toParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = toParseString.indexOf(')');
								 toParseString = toParseString.substring(startIndex,endIndex);
				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(158);
				((ToParseContext)_localctx).HindersToken = match(HindersToken);
				toParseString = (((ToParseContext)_localctx).HindersToken!=null?((ToParseContext)_localctx).HindersToken.getText():null);
						  		 String omitString = "(to";
								 int startIndex = toParseString.indexOf(omitString);
								 toParseString = toParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = toParseString.indexOf(')');
								 toParseString = toParseString.substring(startIndex,endIndex);
				}
				break;
			case ConstraintsToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(160);
				((ToParseContext)_localctx).ConstraintsToken = match(ConstraintsToken);
				toParseString = (((ToParseContext)_localctx).ConstraintsToken!=null?((ToParseContext)_localctx).ConstraintsToken.getText():null);
						  		 String omitString = "(to";
								 int startIndex = toParseString.indexOf(omitString);
								 toParseString = toParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = toParseString.indexOf(')');
								 toParseString = toParseString.substring(startIndex,endIndex);
				}
				break;
			case EnablesToken:
				enterOuterAlt(_localctx, 4);
				{
				setState(162);
				((ToParseContext)_localctx).EnablesToken = match(EnablesToken);
				toParseString = (((ToParseContext)_localctx).EnablesToken!=null?((ToParseContext)_localctx).EnablesToken.getText():null);
						  		 String omitString = "(to";
								 int startIndex = toParseString.indexOf(omitString);
								 toParseString = toParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = toParseString.indexOf(')');
								 toParseString = toParseString.substring(startIndex,endIndex);
				}
				break;
			case DisablesToken:
				enterOuterAlt(_localctx, 5);
				{
				setState(164);
				((ToParseContext)_localctx).DisablesToken = match(DisablesToken);
				toParseString = (((ToParseContext)_localctx).DisablesToken!=null?((ToParseContext)_localctx).DisablesToken.getText():null);
						  		 String omitString = "(to";
								 int startIndex = toParseString.indexOf(omitString);
								 toParseString = toParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = toParseString.indexOf(')');
								 toParseString = toParseString.substring(startIndex,endIndex);
				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 6);
				{
				setState(166);
				((ToParseContext)_localctx).SynchToken = match(SynchToken);
				toParseString = (((ToParseContext)_localctx).SynchToken!=null?((ToParseContext)_localctx).SynchToken.getText():null);
						  		 String omitString = "(to";
								 int startIndex = toParseString.indexOf(omitString);
								 toParseString = toParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = toParseString.indexOf(')');
								 toParseString = toParseString.substring(startIndex,endIndex);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WeightParseContext extends ParserRuleContext {
		public Token ConstraintsToken;
		public TerminalNode ConstraintsToken() { return getToken(CtaemsParser.ConstraintsToken, 0); }
		public WeightParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_weightParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterWeightParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitWeightParse(this);
		}
	}

	public final WeightParseContext weightParse() throws RecognitionException {
		WeightParseContext _localctx = new WeightParseContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_weightParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			((WeightParseContext)_localctx).ConstraintsToken = match(ConstraintsToken);
			weightString = (((WeightParseContext)_localctx).ConstraintsToken!=null?((WeightParseContext)_localctx).ConstraintsToken.getText():null);
					  		 String omitString = "(weight";
							 int startIndex = weightString.indexOf(omitString);
							 weightString = weightString.substring(startIndex);
							 startIndex = omitString.length()+1;		 
							 int endIndex = weightString.indexOf(')');
							 weightString = weightString.substring(startIndex,endIndex);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualityPowerParseContext extends ParserRuleContext {
		public Token FacilitatesToken;
		public Token HindersToken;
		public Token SynchToken;
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public QualityPowerParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualityPowerParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterQualityPowerParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitQualityPowerParse(this);
		}
	}

	public final QualityPowerParseContext qualityPowerParse() throws RecognitionException {
		QualityPowerParseContext _localctx = new QualityPowerParseContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_qualityPowerParse);
		try {
			setState(179);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FacilitatesToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(173);
				((QualityPowerParseContext)_localctx).FacilitatesToken = match(FacilitatesToken);
				String qpParseString = (((QualityPowerParseContext)_localctx).FacilitatesToken!=null?((QualityPowerParseContext)_localctx).FacilitatesToken.getText():null);
						  		 String omitString = "(quality_power";
								 int startIndex = qpParseString.indexOf(omitString);
								 qpParseString = qpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = qpParseString.indexOf(')');
								 qpParseString = qpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(qpParseString);
				    			 int size = tokenizer.countTokens();
				                 qualityPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	qualityPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(175);
				((QualityPowerParseContext)_localctx).HindersToken = match(HindersToken);
				String qpParseString = (((QualityPowerParseContext)_localctx).HindersToken!=null?((QualityPowerParseContext)_localctx).HindersToken.getText():null);
						  		 String omitString = "(quality_power";
								 int startIndex = qpParseString.indexOf(omitString);
								 qpParseString = qpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = qpParseString.indexOf(')');
								 qpParseString = qpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(qpParseString);
				    			 int size = tokenizer.countTokens();
				                 qualityPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	qualityPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(177);
				((QualityPowerParseContext)_localctx).SynchToken = match(SynchToken);
				String qpParseString = (((QualityPowerParseContext)_localctx).SynchToken!=null?((QualityPowerParseContext)_localctx).SynchToken.getText():null);
						  		 String omitString = "(quality_power";
								 int startIndex = qpParseString.indexOf(omitString);
								 qpParseString = qpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = qpParseString.indexOf(')');
								 qpParseString = qpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(qpParseString);
				    			 int size = tokenizer.countTokens();
				                 qualityPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	qualityPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PreferencePowerParseContext extends ParserRuleContext {
		public Token FacilitatesToken;
		public Token HindersToken;
		public Token SynchToken;
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public PreferencePowerParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_preferencePowerParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterPreferencePowerParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitPreferencePowerParse(this);
		}
	}

	public final PreferencePowerParseContext preferencePowerParse() throws RecognitionException {
		PreferencePowerParseContext _localctx = new PreferencePowerParseContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_preferencePowerParse);
		try {
			setState(187);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FacilitatesToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(181);
				((PreferencePowerParseContext)_localctx).FacilitatesToken = match(FacilitatesToken);
				String qpParseString = (((PreferencePowerParseContext)_localctx).FacilitatesToken!=null?((PreferencePowerParseContext)_localctx).FacilitatesToken.getText():null);
						  		 String omitString = "(pref_power";
								 int startIndex = qpParseString.indexOf(omitString);
								 qpParseString = qpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = qpParseString.indexOf(')');
								 qpParseString = qpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(qpParseString);
				    			 int size = tokenizer.countTokens();
				                 prefPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	prefPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(183);
				((PreferencePowerParseContext)_localctx).HindersToken = match(HindersToken);
				String qpParseString = (((PreferencePowerParseContext)_localctx).HindersToken!=null?((PreferencePowerParseContext)_localctx).HindersToken.getText():null);
						  		 String omitString = "(pref_power";
								 int startIndex = qpParseString.indexOf(omitString);
								 qpParseString = qpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = qpParseString.indexOf(')');
								 qpParseString = qpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(qpParseString);
				    			 int size = tokenizer.countTokens();
				                 prefPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	prefPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(185);
				((PreferencePowerParseContext)_localctx).SynchToken = match(SynchToken);
				String qpParseString = (((PreferencePowerParseContext)_localctx).SynchToken!=null?((PreferencePowerParseContext)_localctx).SynchToken.getText():null);
						  		 String omitString = "(pref_power";
								 int startIndex = qpParseString.indexOf(omitString);
								 qpParseString = qpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = qpParseString.indexOf(')');
								 qpParseString = qpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(qpParseString);
				    			 int size = tokenizer.countTokens();
				                 prefPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	prefPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DurationPowerParseContext extends ParserRuleContext {
		public Token FacilitatesToken;
		public Token HindersToken;
		public Token SynchToken;
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public DurationPowerParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_durationPowerParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterDurationPowerParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitDurationPowerParse(this);
		}
	}

	public final DurationPowerParseContext durationPowerParse() throws RecognitionException {
		DurationPowerParseContext _localctx = new DurationPowerParseContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_durationPowerParse);
		try {
			setState(195);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FacilitatesToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(189);
				((DurationPowerParseContext)_localctx).FacilitatesToken = match(FacilitatesToken);
				String dpParseString = (((DurationPowerParseContext)_localctx).FacilitatesToken!=null?((DurationPowerParseContext)_localctx).FacilitatesToken.getText():null);
						  		 String omitString = "(duration_power";
								 int startIndex = dpParseString.indexOf(omitString);
								 dpParseString = dpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = dpParseString.indexOf(')');
								 dpParseString = dpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(dpParseString);
				    			 int size = tokenizer.countTokens();
				                 durationPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	durationPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(191);
				((DurationPowerParseContext)_localctx).HindersToken = match(HindersToken);
				String dpParseString = (((DurationPowerParseContext)_localctx).HindersToken!=null?((DurationPowerParseContext)_localctx).HindersToken.getText():null);
						  		 String omitString = "(duration_power";
								 int startIndex = dpParseString.indexOf(omitString);
								 dpParseString = dpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = dpParseString.indexOf(')');
								 dpParseString = dpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(dpParseString);
				    			 int size = tokenizer.countTokens();
				                 durationPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	durationPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(193);
				((DurationPowerParseContext)_localctx).SynchToken = match(SynchToken);
				String dpParseString = (((DurationPowerParseContext)_localctx).SynchToken!=null?((DurationPowerParseContext)_localctx).SynchToken.getText():null);
						  		 String omitString = "(duration_power";
								 int startIndex = dpParseString.indexOf(omitString);
								 dpParseString = dpParseString.substring(startIndex);
								 startIndex = omitString.length()+1;		 
								 int endIndex = dpParseString.indexOf(')');
								 dpParseString = dpParseString.substring(startIndex,endIndex);
								 StringTokenizer tokenizer = new StringTokenizer(dpParseString);
				    			 int size = tokenizer.countTokens();
				                 durationPowerArray = new double[size];
				    			 for(int i = 0;i < size;i++)
				        		 	durationPowerArray[i] = Double.parseDouble((String)tokenizer.nextElement());
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpliceParseContext extends ParserRuleContext {
		public Token SpliceToken;
		public TerminalNode SpliceToken() { return getToken(CtaemsParser.SpliceToken, 0); }
		public SpliceParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_spliceParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterSpliceParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitSpliceParse(this);
		}
	}

	public final SpliceParseContext spliceParse() throws RecognitionException {
		SpliceParseContext _localctx = new SpliceParseContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_spliceParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197);
			((SpliceParseContext)_localctx).SpliceToken = match(SpliceToken);
			String spliceParseString = (((SpliceParseContext)_localctx).SpliceToken!=null?((SpliceParseContext)_localctx).SpliceToken.getText():null);
					  		String omitString = "(splice";
							int startIndex = spliceParseString.indexOf('(',3);	
							int endIndex = spliceParseString.indexOf(')');			
							spliceString = spliceParseString.substring(startIndex+1,endIndex);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeParseContext extends ParserRuleContext {
		public Token TimeToken;
		public TerminalNode TimeToken() { return getToken(CtaemsParser.TimeToken, 0); }
		public TimeParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeParse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterTimeParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitTimeParse(this);
		}
	}

	public final TimeParseContext timeParse() throws RecognitionException {
		TimeParseContext _localctx = new TimeParseContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_timeParse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			((TimeParseContext)_localctx).TimeToken = match(TimeToken);
			String timeParseString = (((TimeParseContext)_localctx).TimeToken!=null?((TimeParseContext)_localctx).TimeToken.getText():null);
						String omitString = "(time";
						int startIndex = timeParseString.indexOf(omitString);
						timeParseString = timeParseString.substring(startIndex);
				    	startIndex = omitString.length()+1;		 
						int endIndex = timeParseString.indexOf(')');
						timeParseString = timeParseString.substring(startIndex,endIndex);
						timeParseInt = Integer.parseInt(timeParseString);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParseContext extends ParserRuleContext {
		public String token;
		public TerminalNode VersionToken() { return getToken(CtaemsParser.VersionToken, 0); }
		public TerminalNode AgentToken() { return getToken(CtaemsParser.AgentToken, 0); }
		public TerminalNode TaskToken() { return getToken(CtaemsParser.TaskToken, 0); }
		public TerminalNode TaskGroupToken() { return getToken(CtaemsParser.TaskGroupToken, 0); }
		public TerminalNode MethodToken() { return getToken(CtaemsParser.MethodToken, 0); }
		public TerminalNode EnablesToken() { return getToken(CtaemsParser.EnablesToken, 0); }
		public TerminalNode FacilitatesToken() { return getToken(CtaemsParser.FacilitatesToken, 0); }
		public TerminalNode HindersToken() { return getToken(CtaemsParser.HindersToken, 0); }
		public TerminalNode DisablesToken() { return getToken(CtaemsParser.DisablesToken, 0); }
		public TerminalNode Spaces() { return getToken(CtaemsParser.Spaces, 0); }
		public TerminalNode SpliceToken() { return getToken(CtaemsParser.SpliceToken, 0); }
		public TerminalNode TimeToken() { return getToken(CtaemsParser.TimeToken, 0); }
		public TerminalNode EnviToken() { return getToken(CtaemsParser.EnviToken, 0); }
		public TerminalNode SynchToken() { return getToken(CtaemsParser.SynchToken, 0); }
		public TerminalNode ConstraintsToken() { return getToken(CtaemsParser.ConstraintsToken, 0); }
		public TerminalNode EOF() { return getToken(CtaemsParser.EOF, 0); }
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CtaemsListener ) ((CtaemsListener)listener).exitParse(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_parse);
		try {
			setState(235);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VersionToken:
				enterOuterAlt(_localctx, 1);
				{
				setState(203);
				match(VersionToken);

				}
				break;
			case AgentToken:
				enterOuterAlt(_localctx, 2);
				{
				setState(205);
				match(AgentToken);

				}
				break;
			case TaskToken:
				enterOuterAlt(_localctx, 3);
				{
				setState(207);
				match(TaskToken);

				}
				break;
			case TaskGroupToken:
				enterOuterAlt(_localctx, 4);
				{
				setState(209);
				match(TaskGroupToken);

				}
				break;
			case MethodToken:
				enterOuterAlt(_localctx, 5);
				{
				setState(211);
				match(MethodToken);

				}
				break;
			case EnablesToken:
				enterOuterAlt(_localctx, 6);
				{
				setState(213);
				match(EnablesToken);

				}
				break;
			case FacilitatesToken:
				enterOuterAlt(_localctx, 7);
				{
				setState(215);
				match(FacilitatesToken);

				}
				break;
			case HindersToken:
				enterOuterAlt(_localctx, 8);
				{
				setState(217);
				match(HindersToken);

				}
				break;
			case DisablesToken:
				enterOuterAlt(_localctx, 9);
				{
				setState(219);
				match(DisablesToken);

				}
				break;
			case Spaces:
				enterOuterAlt(_localctx, 10);
				{
				setState(221);
				match(Spaces);

				}
				break;
			case SpliceToken:
				enterOuterAlt(_localctx, 11);
				{
				setState(223);
				match(SpliceToken);

				}
				break;
			case TimeToken:
				enterOuterAlt(_localctx, 12);
				{
				setState(225);
				match(TimeToken);

				}
				break;
			case EnviToken:
				enterOuterAlt(_localctx, 13);
				{
				setState(227);
				match(EnviToken);

				}
				break;
			case SynchToken:
				enterOuterAlt(_localctx, 14);
				{
				setState(229);
				match(SynchToken);

				}
				break;
			case ConstraintsToken:
				enterOuterAlt(_localctx, 15);
				{
				setState(231);
				match(ConstraintsToken);

				}
				break;
			case EOF:
				enterOuterAlt(_localctx, 16);
				{
				setState(233);
				match(EOF);
				token = "done";
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\21\u00f0\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2K\n\2\3\3\3\3\3\3\3\3\5\3Q\n\3\3\4\3\4"+
		"\3\4\3\4\5\4W\n\4\3\5\3\5\3\5\3\5\5\5]\n\5\3\6\3\6\3\6\3\7\3\7\3\7\3\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3"+
		"\r\3\16\3\16\3\16\3\16\3\16\3\16\5\16}\n\16\3\17\3\17\3\17\3\17\5\17\u0083"+
		"\n\17\3\20\3\20\3\20\3\20\5\20\u0089\n\20\3\21\3\21\3\21\3\21\5\21\u008f"+
		"\n\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22"+
		"\u009d\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\5\23\u00ab\n\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u00b6"+
		"\n\25\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00be\n\26\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\5\27\u00c6\n\27\3\30\3\30\3\30\3\31\3\31\3\31\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\5\32\u00ee\n\32\3\32\2\2\33\2\4\6\b\n\f\16\20\22\24\26\30\32\34"+
		"\36 \"$&(*,.\60\62\2\2\u0107\2J\3\2\2\2\4P\3\2\2\2\6V\3\2\2\2\b\\\3\2"+
		"\2\2\n^\3\2\2\2\fa\3\2\2\2\16d\3\2\2\2\20g\3\2\2\2\22j\3\2\2\2\24m\3\2"+
		"\2\2\26p\3\2\2\2\30s\3\2\2\2\32|\3\2\2\2\34\u0082\3\2\2\2\36\u0088\3\2"+
		"\2\2 \u008e\3\2\2\2\"\u009c\3\2\2\2$\u00aa\3\2\2\2&\u00ac\3\2\2\2(\u00b5"+
		"\3\2\2\2*\u00bd\3\2\2\2,\u00c5\3\2\2\2.\u00c7\3\2\2\2\60\u00ca\3\2\2\2"+
		"\62\u00ed\3\2\2\2\64\65\7\3\2\2\65K\b\2\1\2\66\67\7\21\2\2\67K\b\2\1\2"+
		"89\7\b\2\29K\b\2\1\2:;\7\n\2\2;K\b\2\1\2<=\7\t\2\2=K\b\2\1\2>?\7\13\2"+
		"\2?K\b\2\1\2@A\7\r\2\2AK\b\2\1\2BC\7\f\2\2CK\b\2\1\2DE\7\6\2\2EK\b\2\1"+
		"\2FG\7\5\2\2GK\b\2\1\2HI\7\7\2\2IK\b\2\1\2J\64\3\2\2\2J\66\3\2\2\2J8\3"+
		"\2\2\2J:\3\2\2\2J<\3\2\2\2J>\3\2\2\2J@\3\2\2\2JB\3\2\2\2JD\3\2\2\2JF\3"+
		"\2\2\2JH\3\2\2\2K\3\3\2\2\2LM\7\7\2\2MQ\b\3\1\2NO\7\21\2\2OQ\b\3\1\2P"+
		"L\3\2\2\2PN\3\2\2\2Q\5\3\2\2\2RS\7\21\2\2SW\b\4\1\2TU\7\7\2\2UW\b\4\1"+
		"\2VR\3\2\2\2VT\3\2\2\2W\7\3\2\2\2XY\7\21\2\2Y]\b\5\1\2Z[\7\7\2\2[]\b\5"+
		"\1\2\\X\3\2\2\2\\Z\3\2\2\2]\t\3\2\2\2^_\7\21\2\2_`\b\6\1\2`\13\3\2\2\2"+
		"ab\7\21\2\2bc\b\7\1\2c\r\3\2\2\2de\7\7\2\2ef\b\b\1\2f\17\3\2\2\2gh\7\7"+
		"\2\2hi\b\t\1\2i\21\3\2\2\2jk\7\7\2\2kl\b\n\1\2l\23\3\2\2\2mn\7\4\2\2n"+
		"o\b\13\1\2o\25\3\2\2\2pq\7\6\2\2qr\b\f\1\2r\27\3\2\2\2st\7\6\2\2tu\b\r"+
		"\1\2u\31\3\2\2\2vw\7\6\2\2w}\b\16\1\2xy\7\5\2\2y}\b\16\1\2z{\7\17\2\2"+
		"{}\b\16\1\2|v\3\2\2\2|x\3\2\2\2|z\3\2\2\2}\33\3\2\2\2~\177\7\6\2\2\177"+
		"\u0083\b\17\1\2\u0080\u0081\7\5\2\2\u0081\u0083\b\17\1\2\u0082~\3\2\2"+
		"\2\u0082\u0080\3\2\2\2\u0083\35\3\2\2\2\u0084\u0085\7\6\2\2\u0085\u0089"+
		"\b\20\1\2\u0086\u0087\7\5\2\2\u0087\u0089\b\20\1\2\u0088\u0084\3\2\2\2"+
		"\u0088\u0086\3\2\2\2\u0089\37\3\2\2\2\u008a\u008b\7\6\2\2\u008b\u008f"+
		"\b\21\1\2\u008c\u008d\7\5\2\2\u008d\u008f\b\21\1\2\u008e\u008a\3\2\2\2"+
		"\u008e\u008c\3\2\2\2\u008f!\3\2\2\2\u0090\u0091\7\t\2\2\u0091\u009d\b"+
		"\22\1\2\u0092\u0093\7\13\2\2\u0093\u009d\b\22\1\2\u0094\u0095\7\r\2\2"+
		"\u0095\u009d\b\22\1\2\u0096\u0097\7\b\2\2\u0097\u009d\b\22\1\2\u0098\u0099"+
		"\7\n\2\2\u0099\u009d\b\22\1\2\u009a\u009b\7\f\2\2\u009b\u009d\b\22\1\2"+
		"\u009c\u0090\3\2\2\2\u009c\u0092\3\2\2\2\u009c\u0094\3\2\2\2\u009c\u0096"+
		"\3\2\2\2\u009c\u0098\3\2\2\2\u009c\u009a\3\2\2\2\u009d#\3\2\2\2\u009e"+
		"\u009f\7\t\2\2\u009f\u00ab\b\23\1\2\u00a0\u00a1\7\13\2\2\u00a1\u00ab\b"+
		"\23\1\2\u00a2\u00a3\7\r\2\2\u00a3\u00ab\b\23\1\2\u00a4\u00a5\7\b\2\2\u00a5"+
		"\u00ab\b\23\1\2\u00a6\u00a7\7\n\2\2\u00a7\u00ab\b\23\1\2\u00a8\u00a9\7"+
		"\f\2\2\u00a9\u00ab\b\23\1\2\u00aa\u009e\3\2\2\2\u00aa\u00a0\3\2\2\2\u00aa"+
		"\u00a2\3\2\2\2\u00aa\u00a4\3\2\2\2\u00aa\u00a6\3\2\2\2\u00aa\u00a8\3\2"+
		"\2\2\u00ab%\3\2\2\2\u00ac\u00ad\7\r\2\2\u00ad\u00ae\b\24\1\2\u00ae\'\3"+
		"\2\2\2\u00af\u00b0\7\t\2\2\u00b0\u00b6\b\25\1\2\u00b1\u00b2\7\13\2\2\u00b2"+
		"\u00b6\b\25\1\2\u00b3\u00b4\7\f\2\2\u00b4\u00b6\b\25\1\2\u00b5\u00af\3"+
		"\2\2\2\u00b5\u00b1\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b6)\3\2\2\2\u00b7\u00b8"+
		"\7\t\2\2\u00b8\u00be\b\26\1\2\u00b9\u00ba\7\13\2\2\u00ba\u00be\b\26\1"+
		"\2\u00bb\u00bc\7\f\2\2\u00bc\u00be\b\26\1\2\u00bd\u00b7\3\2\2\2\u00bd"+
		"\u00b9\3\2\2\2\u00bd\u00bb\3\2\2\2\u00be+\3\2\2\2\u00bf\u00c0\7\t\2\2"+
		"\u00c0\u00c6\b\27\1\2\u00c1\u00c2\7\13\2\2\u00c2\u00c6\b\27\1\2\u00c3"+
		"\u00c4\7\f\2\2\u00c4\u00c6\b\27\1\2\u00c5\u00bf\3\2\2\2\u00c5\u00c1\3"+
		"\2\2\2\u00c5\u00c3\3\2\2\2\u00c6-\3\2\2\2\u00c7\u00c8\7\17\2\2\u00c8\u00c9"+
		"\b\30\1\2\u00c9/\3\2\2\2\u00ca\u00cb\7\20\2\2\u00cb\u00cc\b\31\1\2\u00cc"+
		"\61\3\2\2\2\u00cd\u00ce\7\4\2\2\u00ce\u00ee\b\32\1\2\u00cf\u00d0\7\3\2"+
		"\2\u00d0\u00ee\b\32\1\2\u00d1\u00d2\7\6\2\2\u00d2\u00ee\b\32\1\2\u00d3"+
		"\u00d4\7\5\2\2\u00d4\u00ee\b\32\1\2\u00d5\u00d6\7\7\2\2\u00d6\u00ee\b"+
		"\32\1\2\u00d7\u00d8\7\b\2\2\u00d8\u00ee\b\32\1\2\u00d9\u00da\7\t\2\2\u00da"+
		"\u00ee\b\32\1\2\u00db\u00dc\7\13\2\2\u00dc\u00ee\b\32\1\2\u00dd\u00de"+
		"\7\n\2\2\u00de\u00ee\b\32\1\2\u00df\u00e0\7\16\2\2\u00e0\u00ee\b\32\1"+
		"\2\u00e1\u00e2\7\17\2\2\u00e2\u00ee\b\32\1\2\u00e3\u00e4\7\20\2\2\u00e4"+
		"\u00ee\b\32\1\2\u00e5\u00e6\7\21\2\2\u00e6\u00ee\b\32\1\2\u00e7\u00e8"+
		"\7\f\2\2\u00e8\u00ee\b\32\1\2\u00e9\u00ea\7\r\2\2\u00ea\u00ee\b\32\1\2"+
		"\u00eb\u00ec\7\2\2\3\u00ec\u00ee\b\32\1\2\u00ed\u00cd\3\2\2\2\u00ed\u00cf"+
		"\3\2\2\2\u00ed\u00d1\3\2\2\2\u00ed\u00d3\3\2\2\2\u00ed\u00d5\3\2\2\2\u00ed"+
		"\u00d7\3\2\2\2\u00ed\u00d9\3\2\2\2\u00ed\u00db\3\2\2\2\u00ed\u00dd\3\2"+
		"\2\2\u00ed\u00df\3\2\2\2\u00ed\u00e1\3\2\2\2\u00ed\u00e3\3\2\2\2\u00ed"+
		"\u00e5\3\2\2\2\u00ed\u00e7\3\2\2\2\u00ed\u00e9\3\2\2\2\u00ed\u00eb\3\2"+
		"\2\2\u00ee\63\3\2\2\2\20JPV\\|\u0082\u0088\u008e\u009c\u00aa\u00b5\u00bd"+
		"\u00c5\u00ed";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}