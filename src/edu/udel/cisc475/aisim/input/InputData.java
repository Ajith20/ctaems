package edu.udel.cisc475.aisim.input;

import java.util.ArrayList;
import java.util.HashSet;

import edu.udel.cisc475.aisim.tasktree.TaskTree;
import edu.udel.cisc475.aisim.tasktree.Constraint;
import edu.udel.cisc475.aisim.tasktree.EnvironmentVariable;

/**
 * This class holds the data that was parsed out of the input .ctaems file.
 * 
 * @author Ajith Vemuri
 *
 */
public class InputData {
	/**
	 * The TaskTree created from the input file.
	 */
	private TaskTree tree;

	/**
	 * The version of the cTAEMS file represented as a string.
	 */
	private String version;

	/**
	 * A HashSet of all of the agents.
	 */
	private HashSet<String> agents;
	/**
	 * A ArrayList of all environmental varibles (Each agent has a set of environmental variables)
	 */
	private ArrayList<EnvironmentVariable> enviVariables;

	private ArrayList<Constraint> constraints;

	public InputData(TaskTree tree, String version, HashSet<String> agents, 
						ArrayList<EnvironmentVariable> enviVariables, ArrayList<Constraint> cnrs) {
		this.tree = tree;
		this.agents = agents;
		this.version = version;
		this.enviVariables = enviVariables;
		this.constraints = cnrs;
	}

	public TaskTree getTree() {
		return tree;
	}

	public HashSet<String> getAgents() {
		return agents;
	}

	public String getVersion() {
		return version;
	}

	public ArrayList<EnvironmentVariable> getEnvironmentVariables() { 
		return enviVariables;
	}

	public ArrayList<Constraint> getConstraints() {
		return constraints;
	}
	
}
