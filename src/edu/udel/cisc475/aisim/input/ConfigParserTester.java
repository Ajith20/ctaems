package edu.udel.cisc475.aisim.input;

public class ConfigParserTester {


	public static void main(String[] args){
		ConfigurationParser configParser = new ConfigurationParser("/home/dave/eclipse_workspace/AIsim/trunk/runFiles/config.ini");
		try {
			configParser.parse();
		} catch (InvalidFileException e1) {
			System.err.println("Error" + (e1.getErrors().size() > 1 ? "s" : "") + " in config file");
			for(FileError fe : e1.getErrors()){
				System.err.println(fe);
			}
			System.exit(1);
		}
		//ConfigurationData configData = 
		configParser.getConfigurationData();
	}
	
	
}
