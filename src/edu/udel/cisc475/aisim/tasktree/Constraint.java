package edu.udel.cisc475.aisim.tasktree;

/**
 * This class represents an constriant between two methods in a task tree
 * 
 * @author Ajith Vemuri
 *
 */
public class Constraint {
	/**
	 * The node that activates the relationship.
	 */
	protected Node fromNode;

	/**
	 * The node that's affected by the relationship.
	 */
	protected Node toNode;

	/**
	 * The name of the relationship.
	 */
	protected String name;
	
	
	
	/**
	 * Whether or not this node has arrived in the problem yet.
	 */
	protected boolean arrived;
	
	/**
	 * The time at which this node will arrive in the problem.
	 */
	protected long arrivalTime;
	
	protected int weight; 

	/**
	 * Default constructor for a NodeRelationship.
	 * 
	 * @param fromNode
	 *            The node that activates the relationship.
	 * @param toNode
	 *            The node that's affected by the relationship.
	 * @param name
	 *            The name of the relationship.
	 */
	public Constraint(Node fromNode, Node toNode, String name, int weight) {
		this.fromNode = fromNode;
		this.toNode = toNode;
		this.name = name;
		this.weight = weight;
		this.arrived = true;
		this.arrivalTime = 0L;
	}

	

	public Node getFromNode() {
		return fromNode;
	}

	public Node getToNode() {
		return toNode;
	}

	public String getName() {
		return name;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public boolean hasArrived() {
		return arrived && toNode.hasArrived() && fromNode.hasArrived();
	}
	
	public void setArrived(boolean arrived) {
		this.arrived = arrived;
	}
	
	public long getArrivalTime() {
		return arrivalTime;
	}
	
	public void setArrivalTime(long arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	
}
