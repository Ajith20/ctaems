package edu.udel.cisc475.aisim.tasktree;
import java.util.ArrayList;

public class PreferenceOutcome {
	private String[] preferenceinstanstiations;
	private int preferenceValue;
	
	public PreferenceOutcome(String[] preferenceInstanstiations, int preferenceValue) {
		this.preferenceinstanstiations = preferenceInstanstiations;
		this.preferenceValue = preferenceValue;
	}
	
	public String[] getPreferenceInstanstiations() {
		return this.preferenceinstanstiations;
	}
	
	public int getPreferenceValue() {
		return this.preferenceValue;
	}
	public void setPreferenceValue(int value) {
		this.preferenceValue = value;
	}
	public void setPreferenceInstanstiations(String[] preferenceInstanstiations) {
		this.preferenceinstanstiations = preferenceInstanstiations;
	}
}
