package edu.udel.cisc475.aisim.tasktree;

import java.util.ArrayList;
//import org.apache.commons.collections4.map.MultiKeyMap;

/**
 * This class represents environment variables and preferences of agents over it.
 * Each agent is associated with a set of environmental variables and has preferences associated
 * with the instantiations.
 * 
 * @author Ajith Vemuri
 *
 */

public class EnvironmentVariable {
	
	private String name;
	private String associatedAgent;
	private ArrayList<PreferenceOutcome> preferences;
	private String[] preferenceParents;
	private String[] enviStates;
	private double[] enviDistribution;
	private double[] enviFrequency;

	/**
	 * 
	 * @param name
	 * 		Name of the envi variable
	 * @param associatedAgent
	 * 		Name of the agent associated with this variable 
	 * @param preferences
	 * 		Preferences of the agent 
	 * @param preferenceParents
	 * 		Parents of the envi variable, prefrences of the current envi variable are dependent on its parents
	 * @param enviStates
	 * 		State instantiations of the envi variable
	 * @param enviDistribution
	 * 		Distribution of states of instantiations
	 * @param enviFrequency
	 * 		Distribution of frequencies of instantiations (Number of times these states are visited)
	 */
	
	public EnvironmentVariable(String name, String associatedAgent, ArrayList<PreferenceOutcome> preferences, 
						String[] preferenceParents, String[] enviStates, double[] enviDistribution, double[] enviFrequency) {
		this.name = name;
		this.associatedAgent = associatedAgent;
		this.preferences = preferences;
		this.preferenceParents = preferenceParents;
		this.enviStates = enviStates;
		this.enviDistribution = enviDistribution;
		this.enviFrequency = enviFrequency;	
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setAssociatedAgent(String associatedAgent) {
		this.associatedAgent = associatedAgent;
	}

	public void setPreferenceParents(String[] preferenceParents) {
		this.preferenceParents = preferenceParents;
	}
	
	public void setPreferences(ArrayList<PreferenceOutcome> preferences) {
		this.preferences = preferences;
	}

	public void setEnviStates(String[] enviStates) {
		this.enviStates = enviStates;
	}

	public void setenviDistribution(double[] enviDistribution) {
		this.enviDistribution = enviDistribution;
	}

	public void setenviFrequency(double[] enviFrequency) {
		this.enviFrequency = enviFrequency;
	}

	public String[] getParents() {
		return this.preferenceParents;
	}

	public String getName() {
		return this.name;
	}
	public String getassociatedAgent() { 
		return this.associatedAgent;
	} 
	public String[] getenviStates() {
		return this.enviStates;
	}
	public double[] getenviDistribution() {
		return this.enviDistribution;
	}
	public double[] getenviFrequency() {
		return this.enviFrequency;
	}

	public ArrayList<PreferenceOutcome> getPreferences() {
		return this.preferences;
	}

}