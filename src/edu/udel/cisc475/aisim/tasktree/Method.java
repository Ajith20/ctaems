package edu.udel.cisc475.aisim.tasktree;

import java.util.ArrayList;
import java.util.stream.Stream;


/**
 * This class represents a Method in the task tree. Methods are the leaves of
 * the tree and are the actual "work" done by agents.
 * 
 * @author Ajith Vemuri
 *
 */
public class Method extends Node {
	/**
	 * The name of the agent that this method is assigned to.
	 */
	private String agent;

	/**
	 * The duration of the method in ticks.
	 */
	private long duration;

	/**
	 * Indicates whether or not the method has been started by an agent.
	 */
	private boolean isStarted;

	/**
	 * The start time of the method (in ticks).
	 */
	private Long startTime;
	
	/**
	 * 
	 * Parent variables that the method is conditionally preferentially dependent on
	 * 	 
	 */
	
	 private String[] preferenceParents;

	/**
	 * 
	 * @param prefereceOutcomes
	 */
	

	private ArrayList<ArrayList<PreferenceOutcome>> parentPreferences; 
	 
	private ArrayList<PreferenceOutcome> preferenceOutcomes;



	/**
	 * 
	 * @param preferenceContext
	 * 
	 * Preference context in which the method is set to be executed. The preference context
	 * is set by the scheduling agent
	 */
	 
	 private PreferenceOutcome preferenceContext;
	
	/**
	 * 
	 * @param preference
	 * 
	 * This is set after the method is executed, after the simulator sends a context getPreferenceValue sets this value
	 * 
	 */
	private int preference;
	/** 
	 * 
	 * The default constructor for a Method.
	 * 
	 * @param parent
	 *            The task that is the parent of this method in the task tree.
	 * @param name
	 *            The name of the method.
	 * @param agent
	 *            The name of the agent that this method is assigned to.
	 */
	public Method(Task parent, String name, String agent) {
		super(parent, name);
		this.agent = agent;
		this.duration = 0;
		this.isStarted = false;
		this.startTime = -1L;
		this.parentPreferences = new ArrayList<ArrayList<PreferenceOutcome>>();
		//this.preferenceParents = new ArrayList<EnvironmentVariable>();	
		//this.preferenceValues = new MultiKeyMap();
	}

	/**
	 * Enables this method. Called via an EnablesNodeRelationship.
	 */
	@Override
	public void enable() {
		if (!isStarted) {
			this.enabled = true;
		}
	}

	/**
	 * Disables this method. Called via a DisablesNodeRelationship.
	 */
	@Override
	public void disable() {
		if (!isStarted) {
			this.enabled = false;
		}
	}

	/**
	 * Facilitates this method. Called via a FacilitatesNodeRelationship.
	 */
	/*@Override
	public void facilitate(double qualityFactor, double durationFactor) {
		if (!isStarted) {
			this.quality *= (1 + qualityFactor);
			this.duration *= durationFactor;
		}
	}*/
	
	/**
	 * Change of facilitates to original TAEMS definition
	 * Should write code for preferenes
	 * Facilitation over preferences occur over the entire target method over all 
	 * contexts, so the entire preference table is updated  
	 *
	 */
	
	@Override
	public void facilitate(double qualityFactor, double durationFactor, double prefFactor, Node fromNode) {
		if (!isStarted) {
			Distribution qualityDist = fromNode.qualityDistribution;
			//fromNode.
			double qualityAchieved = fromNode.getQuality();
			ArrayList<Double> qualValues = qualityDist.getValues();
			double highest = 0;
			for (int i = 0; i < qualValues.size(); i++)
			{
				if(qualValues.get(i)> highest)
				{
					highest = qualValues.get(i);
				}
			}
			this.quality = this.quality * (1 + qualityFactor * (qualityAchieved/highest));
			this.duration = (long) (this.duration * (1 - durationFactor * (qualityAchieved/highest)));
			for (int i = 0; i < this.preferenceOutcomes.size(); i++) {
				int prefValue = preferenceOutcomes.get(i).getPreferenceValue();
				prefValue = (int) (prefValue * (1 + prefFactor));
				preferenceOutcomes.get(i).setPreferenceValue(prefValue);
			}
			
		}
	}

	/**
	 * Hinders this method. Called via a HindersNodeRelationship.
	 */
	/*@Override
	public void hinder(double qualityFactor, double durationFactor) {
		if (!isStarted) {
			this.quality *= qualityFactor;
			this.duration *= (1 + durationFactor);
		}
	}*/
	
	/*
	 * Change of Hinders to original TAEMS definiton
	 */
	@Override
	public void hinder(double qualityFactor, double durationFactor, double prefFactor, Node fromNode) {
		if (!isStarted) {
			Distribution qualityDist = fromNode.qualityDistribution;
			double qualityAchieved = fromNode.getQuality();
			ArrayList<Double> qualValues = qualityDist.getValues();
			double highest = 0;
			for (int i = 0; i < qualValues.size(); i++)
			{
				if(qualValues.get(i)> highest)
				{
					highest = qualValues.get(i);
				}
			}
			this.quality = this.quality * (1 - qualityFactor * (qualityAchieved/highest));
			this.duration = (long) (this.duration * (1 + durationFactor * (qualityAchieved/highest)));
			for (int i = 0; i < this.preferenceOutcomes.size(); i++) {
				int prefValue = preferenceOutcomes.get(i).getPreferenceValue();
				prefValue = (int) (prefValue * (1 - prefFactor));
				preferenceOutcomes.get(i).setPreferenceValue(prefValue);
			}
		}
	}
	
	@Override
	public void synchronize(double qualityFactor, double durationFactor, double prefFactor, Node fromNode) {
		if (!isStarted) {
			Distribution qualityDist = fromNode.qualityDistribution;
			double qualityAchieved = fromNode.getQuality();
			ArrayList<Double> qualValues = qualityDist.getValues();
			double highest = 0;
			for (int i = 0; i < qualValues.size(); i++)
			{
				if(qualValues.get(i)> highest)
				{
					highest = qualValues.get(i);
				}
			}
			this.quality = this.quality * (1 + qualityFactor * (qualityAchieved/highest));
			this.duration = (long) (this.duration * (1 - durationFactor * (qualityAchieved/highest)));
			for (int i = 0; i < this.preferenceOutcomes.size(); i++) {
				int prefValue = preferenceOutcomes.get(i).getPreferenceValue();
				prefValue = (int) (prefValue * (1 - prefFactor));
				preferenceOutcomes.get(i).setPreferenceValue(prefValue);
			}
		}
	}

	/**
	 * Gets the earliestStartTime and deadline from this method's parent. If the
	 * parent's times aren't set, the values default to the start of the
	 * simulation and the end of maximum tick value.
	 */
	@Override
	public void computeEarliestStartAndDeadline() {
		if (parent != null) {
			if (parent.getEarliestStartTime() != -1) {
				this.earliestStartTime = parent.getEarliestStartTime();
			} else {
				this.earliestStartTime = 0;
			}
			if (parent.getDeadline() != -1) {
				this.deadline = parent.getDeadline();
			} else {
				this.deadline = Long.MAX_VALUE;
			}
		}
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}
	
	public Distribution getQualityDistribution() {
		return qualityDistribution;
	}

	public void setQualityDistribution(Distribution d) {
		this.qualityDistribution = d;
	}
	
	public Distribution getDurationDistribution() {
		return durationDistribution;
	}

	public void setDurationDistribution(Distribution d) {
		this.durationDistribution = d;
	}

	public void setPreferenceParents(String[] preferenceParents) {
		this.preferenceParents = preferenceParents;
	}

	public String getPreferenceParents() {
		String parents = "";
		for (int i = 0; i < preferenceParents.length; i++) {
			if(i == preferenceParents.length - 1) {
				parents = parents + preferenceParents[i];
			}
			else {
			parents = parents + preferenceParents[i] + ",";
			}
		}
		return parents;
	}

	public void setPreferenceOutcomes(ArrayList<PreferenceOutcome> preferenceOutcomes)
	{
		this.preferenceOutcomes = preferenceOutcomes;
	}

	

	public String getPreferenceOutcomesAsString(ArrayList<PreferenceOutcome> preferenceOutcomes) {
		String outcomes = "";
		for (int i = 0; i  < preferenceOutcomes.size(); i++) {
			outcomes = outcomes +  "(";
			PreferenceOutcome prefOut = preferenceOutcomes.get(i);
			outcomes = outcomes + String.valueOf(prefOut.getPreferenceValue()); 
			outcomes = outcomes + " ";
			for (int j = 0; j < prefOut.getPreferenceInstanstiations().length; j++) {
				outcomes = outcomes + prefOut.getPreferenceInstanstiations()[j];
				if(j != prefOut.getPreferenceInstanstiations().length - 1) {
					outcomes = outcomes + " ";
				}
			}
			outcomes = outcomes + ")";
		}
		return outcomes;
	}

	public void setParentPreferences(ArrayList<PreferenceOutcome> parentPreferenceOutcomes) {
		this.parentPreferences.add(parentPreferenceOutcomes);
	}

	public ArrayList<ArrayList<PreferenceOutcome>> getParentPreferences() {
		return this.parentPreferences;
	}

	public String[] getParentPreferencesAsString(){
		String[] result = new String[this.parentPreferences.size()];
		for (int i = 0; i < this.parentPreferences.size(); i ++) {
			result[i] = this.getPreferenceOutcomesAsString(this.parentPreferences.get(i));
		}
		return result;
	}

	private char[] Integer(int preferenceValue) {
		return null;
	}

	public ArrayList<PreferenceOutcome> getPreferenceOutcomes() 
	{
		return this.preferenceOutcomes;
	}

	public void setPreferenceContext(PreferenceOutcome preferenceContext) {
		this.preferenceContext = preferenceContext;
	}

	public PreferenceOutcome getPreferenceContext() {
		return this.preferenceContext;
	}
	public int getpreferenceValue (String[] preferenceContext) {
		for (int i = 0; i < this.preferenceOutcomes.size(); i++) {
			String[] prefInstantiations = this.preferenceOutcomes.get(i).getPreferenceInstanstiations();
			prefInstantiations = Stream.of(prefInstantiations).sorted().toArray(String[]::new);
			preferenceContext = Stream.of(preferenceContext).sorted().toArray(String[]::new);
			if (prefInstantiations == preferenceContext) {
				this.preference = this.preferenceOutcomes.get(i).getPreferenceValue();
				return this.preferenceOutcomes.get(i).getPreferenceValue();
			}
		}
		//search for string[] in preference outcomes and return preference values
		return 0;
	}

	public int getPreference() { 
		return preference;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void start() {
		this.isStarted = true;
	}
	
	public void abort() {
		this.isStarted = false;
	}

	public boolean isStarted() {
		return isStarted;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long tick) {
		this.startTime = tick;
	}
}
